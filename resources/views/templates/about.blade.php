@extends('layouts.app')
@section('content')
<section class="about-section">
  <div class="container">
    <div class="company-info">
      <div class="company-info__inner">
        <div class="company-info__left">
          <div class="about-title">
            <h1 class="title about-title">О компании</h1>
            <div class="page-title__anchors-row">
              <div class="title link--arrow page-title__anchor"><a class="js-anchor about-section__title" href="#reviews">Отзывы
                  <div class="arrow-right title__arrow title__arrow--down about-section__arrow">
                    <div class="arrow-right__main"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
<defs>
<style>.cls-1{fill-rule:evenodd;}</style>
</defs>
<title>Untitled-1</title>
<g id="Trash" class="arrow-right__head arrow-right__part">
<g id="Logo-Copy-12">
<g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
<g id="Shape">
<path class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
<g id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part">
<g id="Logo-Copy-12-2" data-name="Logo-Copy-12"><g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
<g id="Shape-2" data-name="Shape"><path class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
</svg>
                      <div class="arrow-right__fill-wrap">
                        <div class="arrow-right__fill title__arrow--down-fill"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
<defs>
<style>.cls-1{fill-rule:evenodd;}</style>
</defs>
<title>Untitled-1</title>
<g id="Trash" class="arrow-right__head arrow-right__part">
<g id="Logo-Copy-12">
<g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
<g id="Shape">
<path class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
<g id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part">
<g id="Logo-Copy-12-2" data-name="Logo-Copy-12"><g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
<g id="Shape-2" data-name="Shape"><path class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
</svg>
                        </div>
                      </div>
                    </div>
                  </div></a></div>
              <div class="title link--arrow page-title__anchor"><a class="js-anchor about-section__title" href="#publications">Публикации
                  <div class="arrow-right title__arrow title__arrow--down about-section__arrow">
                    <div class="arrow-right__main"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
<defs>
<style>.cls-1{fill-rule:evenodd;}</style>
</defs>
<title>Untitled-1</title>
<g id="Trash" class="arrow-right__head arrow-right__part">
<g id="Logo-Copy-12">
<g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
<g id="Shape">
<path class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
<g id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part">
<g id="Logo-Copy-12-2" data-name="Logo-Copy-12"><g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
<g id="Shape-2" data-name="Shape"><path class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
</svg>
                      <div class="arrow-right__fill-wrap">
                        <div class="arrow-right__fill title__arrow--down-fill"><svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
<defs>
<style>.cls-1{fill-rule:evenodd;}</style>
</defs>
<title>Untitled-1</title>
<g id="Trash" class="arrow-right__head arrow-right__part">
<g id="Logo-Copy-12">
<g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
<g id="Shape">
<path class="cls-1" d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
<g id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part">
<g id="Logo-Copy-12-2" data-name="Logo-Copy-12"><g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
<g id="Shape-2" data-name="Shape"><path class="cls-1" d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z" transform="translate(-145.99 -484.4)"></path>
</g>
</g>
</g>
</g>
</svg>
                        </div>
                      </div>
                    </div>
                  </div></a></div>
              <div class="title link--arrow page-title__anchor"><a class="about-section__title" href="page-articles.html">Статьи</a></div>
            </div>
          </div>
          <div class="about-section__row">
            <div class="about-section__descr">
              <p>Hawking Bros. специализируется на аутсорсинге веб-разработки для бизнеса и digital-агентств. В число наших услуг входят frontend- и backend-разработка, QA, UX/UI-проектирование и аудит, веб-дизайн, техническая и дизайн-поддержка проектов. Отдельное направление —<span class="about-section__descr-red-text">разработка и поддержка корпоративных порталов.</span></p><br>
              <p>За 6 лет работы мы собрали сильную команду и накопили опыт в разработке для ecommerce, медиа, IT, инвестиций, НКО и других сфер бизнеса. В числе клиентов: Лаборатория Касперского, Сантехника-Онлайн, Мегаплан, Hansa и др.</p><br>
              <p>В качестве аутсорс-продакшена мы работаем с проектами различной сложности. На проект мы выделяем персонального менеджера, собираем команду и выстраиваем порядок коммуникации с клиентом. При необходимости помогаем заказчику понять, какие бизнес-цели должен закрывать проект, и исходя из полученных сведений, планируем работу: такой подход экономит время и бюджет. Также мы можем помочь с выстраиванием работы внутреннего IT-отдела.</p><br>
              <p><span>Как субподрядчик мы берем конкретный пул задач по проекту и реализуем его. Среди партнеров: MediaInstinct, Havas Digital, BBDO, AGIMA и другие ведущие рекламные агенства и digital-студии</span></p><br>
              <p>Ваш проект уникален, а поэтому мы не ограничиваемся одной методологией и комбинируем разные подходы в работе. Еще одно преимущество — масштабируемость. Если проект требует незапланированных ресурсов, то мы быстро подключим дополнительных специалистов из штата.</p><br>
              <p><span>Посмотрите наши &nbsp;</span><span class="about-section__descr-red-text">кейсы &nbsp;</span><span>и узнайте, в какие сроки и с какой командой мы запускали сайт высоконагруженного интернет-магазина, проекты для Лаборатории Касперского или сайт Александра Незлобина.</span></p>
            </div>
            @include('components.culture-info ')
          </div>
        </div>
        @include('components.company-info')
      </div>
    </div>
  </div>
  @include('components.reviews')
  @include('components.publication')
</section>
@include('components.ui.button-up')
@endsection 
