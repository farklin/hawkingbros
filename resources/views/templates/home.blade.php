@extends('layouts.app')

@section('modal')
    @include('components.cases-modal')
@endsection
@section('content') 
    @include('components.introduction')
    @include('components.partners')
    @include('components.services-main')
    @include('components.cases')
    @include('components.contacts')
@endsection 