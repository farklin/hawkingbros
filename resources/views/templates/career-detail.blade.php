@extends('layouts.app')
@section('content')
<section class="career-section career-detail">
    <div class="container">
      <div class="career-detail__breadcrumbs">
        <div class="career-detail__breadcrumbs-inner">
          <div class="career-detail__breadcrumbs-icon">
            <svg class="icon arrow-left">
              <use xlink:href="#arrow-left"></use>
            </svg>
          </div><a class="career-detail__breadcrumbs-text" href="career.html">Вернуться к списку вакансий</a>
        </div>
      </div>
      <div class="career-title">
        <h1 class="title career-title">Frontend-разработчик</h1>
      </div>
      <div class="jobs__inner">
        <div class="jobs__left">
          <div class="career-detail__row">
            <div class="career-section__descr">
              <p>Мы ищем разработчика, которому интересно реализовывать и развивать сложные проекты, интегрированные в распределенную инфраструктуру крупного eCommerce. Наши проекты – это работа с высокими нагрузками, кластерной архитектурой, и многоуровневыми интеграциями, общение с командами со стороны клиента и других IT-партнеров. Мы ищем того, кто будет смотреть на проект широко и принимать участие в принятии ключевых решений.</p>
            </div>
          </div>
          <div class="career-detail__info">
            <div class="job-basic-info">
              <div class="job-basic-info__item"><span class="job-basic-info__row">от <span>30 000 ₽</span></span><span class="job-basic-info__row">Заработная плата</span></div>
              <div class="job-basic-info__item"><span class="job-basic-info__row"><span>1-3</span> года</span><span class="job-basic-info__row">Требуемый опыт работы  </span></div>
            </div>
          </div>
          <div class="career-detail__job-row">
            <h2 class="title section-title career-detail__subtitle">Обязанности</h2>
            <div class="job-descr">
              <ul class="job-descr__list">
                <li class="job-descr__item">Верстка сайтов "с нуля"</li>
                <li class="job-descr__item">Доработка существующей верстки</li>
              </ul>
            </div>
          </div>
          <div class="career-detail__job-row">
            <h2 class="title section-title career-detail__subtitle">Требования</h2>
            <div class="job-descr">
              <ul class="job-descr__list">
                <li class="job-descr__item">Аккуратность и здоровый перфекционизм</li>
                <li class="job-descr__item">Уверенные знания HTML5, CSS3, препроцессоров, БЭМ, JavaScript и адаптивной верстки</li>
                <li class="job-descr__item">Опыт работы с системой контроля версий</li>
                <li class="job-descr__item">Знание особенностей верстки под IE10+ и последние версии всех современных браузеров</li>
                <li class="job-descr__item">Самостоятельность и инициатива в применении визуальных эффектов и анимации</li>
                <li class="job-descr__item">Внимательность, самоконтроль и ответственность </li>
              </ul>
            </div>
          </div>
          <div class="career-detail__job-row">
            <h2 class="title section-title career-detail__subtitle">Условия</h2>
            <div class="job-descr">
              <ul class="job-descr__list">
                <li class="job-descr__item">Работа в одном из лучших аутсорс-продакшнов России (5 место рейтинга Тэглайн)</li>
                <li class="job-descr__item">Профессиональная команда, которая всегда готова научить и подсказать</li>
                <li class="job-descr__item">Интересные проекты от крупных российских и мировых компаний и брендов</li>
                <li class="job-descr__item">Работа в комфортабельном офисе с отличным транспортным сообщением</li>
                <li class="job-descr__item">Премии по результатам работы</li>
                <li class="job-descr__item">Трудоустройство по ТК РФ</li>
                <li class="job-descr__item">Чай, кофе и печенье в неограниченном количестве</li>
                <li class="job-descr__item">Испытательный срок 1 месяц</li>
              </ul>
            </div>
          </div>
          <div class="career-detail__job-row">
            <h2 class="title section-title career-detail__subtitle career-detail__subtitle--contact">Заинтересовала вакансия?</h2>
            <div class="button career-button js-career-contact">Напишите нам</div>
          </div>
        </div>
        <div class="jobs__right">
          <div class="job__contacts js-contacts-sticky">
            <div class="career-detail__headhunter">
              <div class="headhunter">
                <div class="headhunter__logo"><img class="headhunter-logo--light" src="assets/images/content/career/logohhru.png" alt=""><img class="headhunter-logo--dark" src="assets/images/content/career/logohhru1.png" alt=""></div><a class="headhunter__link link--privacy" href="hh.ru">Вакансия на HeadHunter</a>
              </div>
            </div>
            <div class="career-detail__row">
              <div class="jobs__mail"><span class="title jobs__subtitle"> Не нашли подходящую вакансию?</span>
                <div class="jobs__mail-contact"><span class="jobs__mail-text">Напишите нам на </span><a class="link--privacy" href="#">hello@hawkingbrothers.com</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="career-contact-form-holder js-form-holder">
      <div class="contacts__form-wrap">
        <div class="contact-form__succes-message js-form-succes">
          <div class="contact-form__succes-inner"><span class="title">Форма успешно отправлена.</span></div>
        </div>
        <form class="contact-form js-career-form contact-form--career" novalidate="novalidate">
          <div class="contact-form__row contact-form__row--title"><span class="title contact-form__title">Вакансия Frontend-разработчик</span>
            <div class="contact-form__close js-close-contact-form"></div>
          </div>
          <div class="contact-form__row contact-form__row--name">
            <input class="contact-form__input contact-form__field js-input js-input-name" type="text" name="name" id="name" required="">
            <div class="contact-form__fill-line"></div>
            <label class="contact-form__label js-form-label" for="name">Как Вас зовут?</label>
          </div>
          <div class="contact-form__row">
            <label class="contact-form__label js-form-label" for="phone">Телефон</label>
            <input class="contact-form__input contact-form__field js-input js-phone customPhone" type="tel" name="phone" id="phone" required="" inputmode="text">
            <div class="contact-form__fill-line"></div>
          </div>
          <div class="contact-form__row">
            <label class="contact-form__label js-form-label" for="email">Электропочта</label>
            <input class="contact-form__input contact-form__field js-input customEmail" type="email" name="email" id="email" required="">
            <div class="contact-form__fill-line"></div>
          </div>
          <div class="contact-form__row contact-form__row--message">
            <label class="contact-form__label js-form-label" for="message">Расскажите о себе</label>
            <div class="scroll-wrapper contact-form__input contact-form__field contact-form__input--textarea contact-form__input--textarea_scroll js-input scroll-textarea" style="position: relative;"><div class="scroll-content" style="height: 65px; margin-bottom: 0px; margin-right: 0px; max-height: none;"><textarea class="contact-form__input contact-form__field contact-form__input--textarea contact-form__input--textarea_scroll js-input" name="message" id="message"></textarea></div><div class="scroll-element scroll-x" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar"></div></div></div><div class="scroll-element scroll-y" style=""><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar"></div></div></div></div>
            <div class="contact-form__fill-line"></div>
          </div>
          <div class="contact-form__row">
            <label class="upload-resume js-file-label">
              <input class="upload-resume__input js-file-input" type="file" name="resume" required=""><span class="js-file-error upload-resume__error">Пожалуйста, прикрепите Ваше резюме!</span><span class="upload-resume__icon">
                <svg class="icon plus">
                  <use xlink:href="#plus"></use>
                </svg></span><span class="js-file-name">Прикрепить резюме</span>
            </label>
          </div>
          <div class="contact-form__row contact-form__row--submit">
            <button class="contact-form__input career-form__button button" type="submit">Отправить</button>
            <div class="contact-form__terms"><span>Нажимая "Отправить" Вы соглашаетесь с </span><a class="link--privacy" href="sogl.pdf" target="_blank">Политикой обработки персональных данных</a></div>
          </div>
          <div class="form-error-container"></div>
          <input class="contact-form__person js-input-person" type="text" name="person">
        </form>
      </div>
    </div>
  </section>
  @include('components.ui.button-up')
@endsection  
