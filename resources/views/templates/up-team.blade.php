@extends('layouts.app')
@section('content')
    @include('components.introduction')
    @include('components.partners')
    @include('components.services')
    @include('components.cases')
    @include('components.contacts')
@endsection 
