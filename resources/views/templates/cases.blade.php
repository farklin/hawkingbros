@extends('layouts.app')
@section('modal')
    @include('components.cases-modal')
@endsection
@section('content')
    <section class="page-section cases cases-separate">
        <div class="container">
            <div class="cases__title-box">
                <h1 class="title cases-title">Проекты</h1>
            </div>
            <div class="cases__inner">
                <div class="cases__filter">
                    <form class="filter js-filter" action="/assets/data/works.json" method="GET">
                        @isset($tags)
                            <div class="filter__line">
                                @foreach ($tags as $tag)
                                    <label class="filter-item">
                                        <input class="filter-item__input js-filter-item__input" type="checkbox" name="tags[]"
                                            value="{{ $tag->id }}"><span class="filter-item__text">{{ $tag->title }}
                                            <svg class="icon cross">
                                                <use xlink:href="#cross"></use>
                                            </svg></span>
                                    </label>
                                @endforeach
                            </div>
                            @endif

                            @isset($services)
                                <div class="filter__line">
                                    @foreach ($services as $service)
                                        <label class="filter-item">
                                            <input class="filter-item__input js-filter-item__input" type="checkbox"
                                                name="services[]" value="{{ $service->id }}"><span
                                                class="
                                                                                                                                                                                                                                                            filter-item__text">{{ $service->title }}
                                                <svg class="icon cross">
                                                    <use xlink:href="#cross"></use>
                                                </svg></span>
                                        </label>
                                    @endforeach
                                </div>
                                @endif
                        </div>
                        </form>
                    </div>
                    @include('components.cases-list')
                </div>
                </div>
            </section>
            @include('components.ui.button-up')
        @endsection


        @section('scripts')
            <script>
                $('.filter-item__input').change(
                    function() {
                        console.log($('.js-filter').serialize());
                        // $.ajax({
                        //     method: "GET",
                        //     url: 'cases/filter',
                        //     data: $('.js-filter').serialize(),
                        //     success: function(data) {
                        //         console.log($(data).find('portfolio js-portfolio').html());
                        //     }
                        // });
                        $("#portfolio").load("/cases/filter #portfolio .portfolio__item", $('.js-filter')
                            .serialize());

                    }
                );

            </script>
        @endsection
