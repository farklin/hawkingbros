@extends('layouts.app')
@section('content')
<section class="page-section articles articles-separate">
    <div class="container">
      <div class="articles__title-box">
        <h1 class="title articles-title">Статьи</h1>
      </div>
      <div class="articles__inner">
        <div class="articles__filter">
          <form class="filter js-filter" action="/assets/data/articles.json" method="GET">
            <div class="filter__line">
              @if(isset($tags))
                @foreach($tags as $tag)
                <label class="filter-item">
                  <input class="filter-item__input js-filter-item__input" type="radio" name="type"><span class="filter-item__text">{{$tag['title']}}
                    <svg class="icon cross">
                      <use xlink:href="#cross"></use>
                    </svg></span>
                </label>
                @endforeach
              @endif
            </div>
          </form>
        </div>
        @if(isset($articles)) 
          @include('components.article-list', ['articles' => $articles])
        @endif
        <div class="publications__more-wrapper"><a class="publications__more-btn js-show-article">
            <svg class="icon plus">
              <use xlink:href="#plus"></use>
            </svg><span class="text">Показать еще </span></a></div>
      </div>
    </div>
  </section>
@endsection
