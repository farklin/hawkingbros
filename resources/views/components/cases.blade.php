<section class="page-section cases">
    <div class="container">
        <div class="cases__title-box">
            <h2 class="title section-title link--arrow"> <a href="/cases">Наши проекты
                    <div class="arrow-right title__arrow title__arrow--cases">
                        <div class="arrow-right__main"><svg id="Layer_1" data-name="Layer 1"
                                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
                                <defs>
                                    <style>
                                        .cls-1 {
                                            fill-rule: evenodd;
                                        }

                                    </style>
                                </defs>
                                <title>Untitled-1</title>
                                <g id="Trash" class="arrow-right__head arrow-right__part">
                                    <g id="Logo-Copy-12">
                                        <g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
                                            <g id="Shape">
                                                <path class="cls-1"
                                                    d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z"
                                                    transform="translate(-145.99 -484.4)"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                                <g id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part">
                                    <g id="Logo-Copy-12-2" data-name="Logo-Copy-12">
                                        <g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
                                            <g id="Shape-2" data-name="Shape">
                                                <path class="cls-1"
                                                    d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z"
                                                    transform="translate(-145.99 -484.4)"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <div class="arrow-right__fill-wrap">
                                <div class="arrow-right__fill"><svg id="Layer_1" data-name="Layer 1"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.39 19.29">
                                        <defs>
                                            <style>
                                                .cls-1 {
                                                    fill-rule: evenodd;
                                                }

                                            </style>
                                        </defs>
                                        <title>Untitled-1</title>
                                        <g id="Trash" class="arrow-right__head arrow-right__part">
                                            <g id="Logo-Copy-12">
                                                <g id="ic_arrow_right-copy" data-name="ic arrow right-copy">
                                                    <g id="Shape">
                                                        <path class="cls-1"
                                                            d="M169.73,487.3l4.52,4.94,1.71,1.82-1.71,1.83-4.45,4.9a1.77,1.77,0,0,0,0,2.43,1.58,1.58,0,0,0,2.22,0l.08-.08,8.23-9.11-8.3-9.15a1.6,1.6,0,0,0-2.22-.12.6.6,0,0,0-.08.08A1.77,1.77,0,0,0,169.73,487.3Z"
                                                            transform="translate(-145.99 -484.4)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                        <g id="Trash-2" data-name="Trash" class="arrow-right__axis arrow-right__part">
                                            <g id="Logo-Copy-12-2" data-name="Logo-Copy-12">
                                                <g id="ic_arrow_right-copy-2" data-name="ic arrow right-copy">
                                                    <g id="Shape-2" data-name="Shape">
                                                        <path class="cls-1"
                                                            d="M174.39,492.23c-4,0-24.17,0-26.58,0a1.83,1.83,0,0,0,0,3.65l26.58,0C176.68,495.86,176.68,492.23,174.39,492.23Z"
                                                            transform="translate(-145.99 -484.4)"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </a></h2>
        </div>
        <div class="cases__inner">
            <div class="portfolio">
                @isset($projects)
                    @foreach ($projects as $project)
                        @include('components/cases-item')
                    @endforeach
                    @endif
                </div>
            </div>
        </div>

    </section>
