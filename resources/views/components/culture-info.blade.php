<div class="culture-info company-info__points">
    <div class="culture-info__inner career-section__inner">
      <div class="culture-info__item">
        <div class="culture-info__item-inner"><span class="culture-info__text culture-info__text--lg">56</span><span class="culture-info__text">Штатных специалистов</span></div>
      </div>
      <div class="culture-info__item">
        <div class="culture-info__item-inner"><span class="culture-info__text culture-info__text--lg">400+</span><span class="culture-info__text">Реализованных партнерских проектов</span></div>
      </div>
      <div class="culture-info__item">
        <div class="culture-info__item-inner"><span class="culture-info__text culture-info__text--lg">2000+</span><span class="culture-info__text">Часов разработки отгружаем ежемесячно</span></div>
      </div>
    </div>
  </div>