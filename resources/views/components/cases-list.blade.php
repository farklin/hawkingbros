@isset($projects)
    <div class="portfolio js-portfolio" id="portfolio">
        @foreach ($projects as $project)
            @include('components.cases-item')
        @endforeach
    </div>
    @endif
