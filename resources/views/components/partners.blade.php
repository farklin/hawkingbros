@isset($parthners)
<section class="page-section partners">
    <div class="partners-slider">
        <div class="swiper-container js-partners-swiper swiper-container-initialized swiper-container-horizontal">
            <ul class="partners__inner swiper-wrapper" style="transform: translateX(-2690px);">
                @foreach($parthners as $partner)
                <li class="partners__item swiper-slide">
                    <a class="partner-logo" href="{{$partner->link}}" target="_blank">
                      <img src="{{asset($partner->getImageUrl())}}" alt=""> 
                    </a>
                </li>
                @endforeach 
            </ul>
            <div class="swiper-scrollbar">
                <div class="swiper-scrollbar-drag" style="width: 0px; transform: translate3d(0px, 0px, 0px);"></div>
            </div>
            <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
        </div>
    </div>
</section>
@endif