<div class="reviews-services">
    <div class="page-section reviews">
      <div class="container">
        <h2 class="title section-title">Отзывы</h2>
        <div class="reviews__inner">
          <div class="swiper-container js-reviews-slider swiper-container-initialized swiper-container-horizontal">
            <div class="reviews-slider swiper-wrapper" style="transform: translate3d(0px, 0px, 0px);">
              <div class="reviews-slider__item swiper-slide swiper-slide-active" data-id="0" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Евгений Сахаров 0</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>СТО</span></div><a class="reviews-slider__company" href="#"><span>Сантехника-Онлайн</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-san-teh.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза.</span></div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide swiper-slide-next" data-id="1" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Юлия Локшина 1</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>PR-менеджер</span></div><a class="reviews-slider__company" href="#"><span>Ales Capital </span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-ales.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. </span><br>Скорость — это сильная сторона команды.</div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide" data-id="2" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Евгений Сахаров 2</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>СТО</span></div><a class="reviews-slider__company" href="#"><span>Сантехника-Онлайн</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-san-teh.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза. </span></div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide" data-id="3" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Юлия Локшина 3</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>PR-менеджер</span></div><a class="reviews-slider__company" href="#"><span>Ales Capital</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-ales.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. </span><br>Скорость — это сильная сторона команды.</div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide" data-id="4" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Юлия Локшина 4</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>СТО</span></div><a class="reviews-slider__company" href="#"><span>Сантехника-Онлайн</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-san-teh.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза. </span></div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide" data-id="5" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Юлия Локшина 5</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>PR-менеджер</span></div><a class="reviews-slider__company" href="#"><span>Ales Capital</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-ales.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. </span><br>Скорость — это сильная сторона команды.</div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide" data-id="6" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Евгений Сахаров 6</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>СТО</span></div><a class="reviews-slider__company" href="#"><span>Сантехника-Онлайн</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-san-teh.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Hawking Bros, благодаря быстрому подключению дополнительных специалистов, всего лишь за месяц увеличила объем вырабатываемых часов в 2 раза. </span></div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
              <div class="reviews-slider__item swiper-slide" data-id="7" style="width: 737px; margin-right: 20px;">
                <div class="reviews-slider__company-wrapper">
                  <div class="reviews-slider__company-info"> 
                    <div class="reviews-slider__name"><span>Юлия Локшина 7</span></div>
                    <div class="reviews-slider__position-wrapper">
                      <div class="reviews-slider__position"><span>PR-менеджер</span></div><a class="reviews-slider__company" href="#"><span>Ales Capital</span></a>
                    </div>
                  </div>
                  <div class="reviews-popup__logo-wrapper"><img class="reviews-slider__company-logo" src="assets/images/content/partners/logo-ales.png"></div>
                </div>
                <div class="reviews-slider__text"><span>Работа над проектом шла быстро, все правки и пожелания ребята учитывали и быстро вносили. </span><br>Скорость — это сильная сторона команды.</div>
                <div class="reviews-slider__more-info js-show-popup-reviews"><span>Читать отзыв</span><span class="reviews-slider__arrow">
                    <svg class="icon arrow-right">
                      <use xlink:href="#arrow-right"></use>
                    </svg></span></div>
              </div>
            </div>
            <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-bullets-dynamic" style="width: 126px;"><span class="swiper-pagination-bullet swiper-pagination-bullet-active swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 1" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 2" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-main" tabindex="0" role="button" aria-label="Go to slide 3" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-next" tabindex="0" role="button" aria-label="Go to slide 4" style="left: 36px;"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active-next-next" tabindex="0" role="button" aria-label="Go to slide 5" style="left: 36px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6" style="left: 36px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 7" style="left: 36px;"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 8" style="left: 36px;"></span></div>
          <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
          <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"></div>
          <div class="swiper-button-prev swiper-button-disabled" tabindex="-1" role="button" aria-label="Previous slide" aria-disabled="true"></div>
        </div>
      </div>
    </div>
    <div class="reviews-popup">
      <div class="reviews-popup__slider">
        <div class="swiper-container js-reviews-popup-slider swiper-container-fade swiper-container-initialized swiper-container-horizontal"> 
          <div class="swiper-wrapper" style="transition-duration: 0ms;">
            <div class="reviews-popup__inner swiper-slide" data-id="0">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-san-teh.png">
                <div class="reviews-popup__name"><span>Евгений Сахаров 0</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>СТО</span></div><a class="reviews-popup__company" href="#"><span>Сантехника-Онлайн</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"><span>sam Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. 1111111</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="1">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-ales.png">
                <div class="reviews-popup__name"><span>Юлия Лошкина 1</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>PR-менеджер</span></div><a class="reviews-popup__company" href="#"><span>Ales Capital</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. </span><span>Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae </span><span>laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. </span><span>Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="2">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-san-teh.png">
                <div class="reviews-popup__name"><span>Евгений Сахаров 2</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>СТО</span></div><a class="reviews-popup__company" href="#"><span>Сантехника-Онлайн</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"> <span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="3">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-ales.png">
                <div class="reviews-popup__name"><span>Юлия Лошкина 3</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>PR-менеджер</span></div><a class="reviews-popup__company" href="#"><span>Ales Capital</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"> <span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis </span><span>voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis </span><span>voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="4">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-san-teh.png">
                <div class="reviews-popup__name"><span>Евгений Сахаров 4</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>СТО</span></div><a class="reviews-popup__company" href="#"><span>Сантехника-Онлайн</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps"> 
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="5">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-ales.png">
                <div class="reviews-popup__name"><span>Юлия Лошкина 5</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>PR-менеджер</span></div><a class="reviews-popup__company" href="#"><span>Ales Capital</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. </span><span>Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. </span><span>Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. </span><span>Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="6">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-san-teh.png">
                <div class="reviews-popup__name"><span>Евгений Сахаров 6</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>СТО</span></div><a class="reviews-popup__company" href="#"><span>Сантехника-Онлайн</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum? Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
            <div class="reviews-popup__inner swiper-slide" data-id="7">
              <div class="reviews-popup__info">
                <div class="reviews-popup__btn-close js-hide-popup-reviews"></div><img class="reviews-popup__company-logo" src="assets/images/content/partners/logo-ales.png">
                <div class="reviews-popup__name"><span>Юлия Лошкина 7</span></div>
                <div class="reviews-popup__position-wrapper">
                  <div class="reviews-popup__position"><span>PR-менеджер</span></div><a class="reviews-popup__company" href="#"><span>Ales Capital</span></a>
                </div>
              </div>
              <div class="reviews-popup__gradient"></div>
              <div class="reviews-popup__text-wrapper is-top is-bottom" style="top: 0px; margin-top: 0px;">
                <div class="reviews-popup__text ps">
                  <div class="reviews-popup__text-inner" style="padding-top: 0px;"><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus!lorem Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sint magni nemo rem earum dignissimos accusantium modi asperiores, suscipit natus corrupti esse eos illo dolor reiciendis, molestiae illum?</span><span>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Minima deserunt maxime autem est expedita. Eius quibusdam quae labore ut consequuntur odit. Ab recusandae laudantium, blanditiis consequatur ipsum hic omnis voluptatibus! 123</span></div>
                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination swiper-pagination-bullets swiper-pagination-bullets-dynamic"></div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
        <div class="swiper-button-next" tabindex="0" role="button" aria-label="Next slide" aria-disabled="false"></div>
        <div class="swiper-button-prev" tabindex="0" role="button" aria-label="Previous slide" aria-disabled="false"></div>
      </div>
    </div>
  </div>