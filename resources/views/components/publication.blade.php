<div class="container">
    <div class="page-section publications" id="publications">
      <h2 class="title section-title">Публикации</h2>
      <div class="publications__inner">
        <div class="publications__list">
          <div class="publication-item">
            <p class="publication-item__date">21 мая 2020</p>
            <p class="publication-item__title">Как региональной ИТ-компании начать выстраивать отношения с государством</p>
            <p class="publication-item__author">Егор Сизяков</p>
            <p class="publication-item__description">Когда речь заходит о GR, чаще всего в качестве примеров приводят зарубежные и столичные кейсы. Они не подходят региональным компаниям, а тем более МСБ…</p><a class="publication-item__link-wrapper"><span class="text">Читать на vc.ru</span>
              <svg class="icon link">
                <use xlink:href="#link"></use>
              </svg></a>
          </div>
          <div class="publication-item">
            <p class="publication-item__date">21 мая 2020</p>
            <p class="publication-item__title">Как региональной ИТ-компании начать выстраивать отношения с государством</p>
            <p class="publication-item__author">Егор Сизяков</p>
            <p class="publication-item__description">Когда речь заходит о GR, чаще всего в качестве примеров приводят зарубежные и столичные кейсы. Они не подходят региональным компаниям, а тем более МСБ…</p><a class="publication-item__link-wrapper"><span class="text">Читать на vc.ru</span>
              <svg class="icon link">
                <use xlink:href="#link"></use>
              </svg></a>
          </div>
          <div class="publication-item">
            <p class="publication-item__date">21 мая 2020</p>
            <p class="publication-item__title">Как региональной ИТ-компании начать выстраивать отношения с государством</p>
            <p class="publication-item__author">Егор Сизяков</p>
            <p class="publication-item__description">Когда речь заходит о GR, чаще всего в качестве примеров приводят зарубежные и столичные кейсы. Они не подходят региональным компаниям, а тем более МСБ…</p><a class="publication-item__link-wrapper"><span class="text">Читать на vc.ru</span>
              <svg class="icon link">
                <use xlink:href="#link"></use>
              </svg></a>
          </div>
        </div>
        <div class="publications__more-wrapper"><a class="publications__more-btn">
            <svg class="icon plus">
              <use xlink:href="#plus"></use>
            </svg><span class="text">Показать еще</span></a></div>
      </div>
    </div>
  </div>