<a class="articles__item @if($show_image) articles__item--img @endif" @if($show_image) style="background-image: url('assets/images/content/articles/articles-img.jpg'); background-color: rgba(0, 0, 0, 0.5)" @endif href="{{route('article', $article->url)}}">
      <div class="articles-card js-lazy">
        <div class="articles-card__inner">
          <div class="articles-card__top"><span class="articles-card__top-text">{{$article->tag->title}}</span>
            <p class="articles-card__top-title">{{$article->title}}</p>
          </div>
          <div class="articles-card__bottom">
            <p class="articles-card__bottom-text">{{$article->short_description}}</p>
            <div class="articles__btn"><span class="articles__btn-text">Читать статью</span><span class="articles__arrow">
                <svg class="icon arrow-right">
                  <use xlink:href="#arrow-right"></use>
                </svg></span></div>
          </div>
        </div>
      </div>
    </a>