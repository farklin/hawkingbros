<div class="page-section services" id="services-digital">
  <div class="container">
    <h2 class="title section-title">Как мы работаем</h2>
    <div class="services__inner">
      <div class="services__item">
        <div class="service-card">
          <div class="service-card__inner">
            <div class="service-card__top">
              <h3 class="title service-card__title">Аналитика</h3>
              <div class="service-card__icon service-card__icon--support">
                <svg class="icon icon-prototyping">
                  <use xlink:href="#icon-prototyping"></use>
                </svg>
              </div>
            </div>
            <div class="service-card__middle">
              <p class="service-card__descr">Проводим предпроектные исследования и глубинные интервью. Выявляем истинные потребности пользователей. Синтезируем полученные данные и готовим задания для проектирования. Осуществляем авторский надзор.</p>
            </div>
            <div class="service-card__bottom">
              <ul class="technologies">
                <li class="technologies__item">BRD</li>
                <li class="technologies__item">MTT</li>
                <li class="technologies__item">Interviews</li>
                <li class="technologies__item">CJM</li>
                <li class="technologies__item">Audits</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="services__item">
        <div class="service-card">
          <div class="service-card__inner">
            <div class="service-card__top">
              <h3 class="title service-card__title">UX / UI</h3>
              <div class="service-card__icon service-card__icon--ui">
                <svg class="icon icon-illustration">
                  <use xlink:href="#icon-illustration"></use>
                </svg>
              </div>
            </div>
            <div class="service-card__middle">
              <p class="service-card__descr">Проектируем интерфейсы основанные на данных и пишем Use-кейсы. Разрабатываем карты пути пользователя. Проводим usability-аудиты.</p>
            </div>
            <div class="service-card__bottom">
              <ul class="technologies">
                <li class="technologies__item">Sketch</li>
                <li class="technologies__item">Axure</li>
                <li class="technologies__item">Moqup</li>
                <li class="technologies__item">MindMup</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="services__item">
        <div class="service-card">
          <div class="service-card__inner">
            <div class="service-card__top">
              <h3 class="title service-card__title">Дизайн</h3>
              <div class="service-card__icon service-card__icon--wdesign">
                <svg class="icon icon-branding">
                  <use xlink:href="#icon-branding"></use>
                </svg>
              </div>
            </div>
            <div class="service-card__middle">
              <p class="service-card__descr">Готовим дизайн-концепции. Предоставляем услуги по веб-дизайну, а также работаем в коллаборациях с ведущими студиями страны. Выполняем задачи по дизайн-поддержке проектов.</p>
            </div>
            <div class="service-card__bottom">
              <ul class="technologies">
                <li class="technologies__item">Photoshop</li>
                <li class="technologies__item">Illustrator</li>
                <li class="technologies__item">Figma</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="services__item">
        <div class="service-card">
          <div class="service-card__inner">
            <div class="service-card__top">
              <h3 class="title service-card__title">Backend</h3>
              <div class="service-card__icon">
                <svg class="icon icon-development">
                  <use xlink:href="#icon-development"></use>
                </svg>
              </div>
            </div>
            <div class="service-card__middle">
              <p class="service-card__descr">Разрабатываем надежный бэкенд с использованием современных CMS и Framework. Пишем собственные API и проводим интеграции с любыми внешними системами. Проектируем базы данных.</p>
            </div>
            <div class="service-card__bottom">
              <ul class="technologies">
                <li class="technologies__item">PHP7</li>
                <li class="technologies__item">Laravel</li>
                <li class="technologies__item">Symfony</li>
                <li class="technologies__item">Bitrix</li>
                <li class="technologies__item">Elasticsearch</li>
                <li class="technologies__item">Sphinx</li>
                <li class="technologies__item">XML</li>
                <li class="technologies__item">MySQL</li>
                <li class="technologies__item">RabbitMQ</li>
                <li class="technologies__item">Apache</li>
                <li class="technologies__item">Kafka</li>
                <li class="technologies__item">PostgreSQL</li>
                <li class="technologies__item">Docker</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="services__item">
        <div class="service-card">
          <div class="service-card__inner">
            <div class="service-card__top">
              <h3 class="title service-card__title">Frontend</h3>
              <div class="service-card__icon">
                <svg class="icon icon-front">
                  <use xlink:href="#icon-front"></use>
                </svg>
              </div>
            </div>
            <div class="service-card__middle">
              <p class="service-card__descr">Работаем как со статичной версткой, так и современными JS Framework. Создаем SPA. Делаем сложные анимации с оптимальной скоростью загрузки. Адаптируем сайты под различные устройства и разрешения экранов.</p>
            </div>
            <div class="service-card__bottom">
              <ul class="technologies">
                <li class="technologies__item">HTML5 + CSS3</li>
                <li class="technologies__item">Sass</li>
                <li class="technologies__item">React</li>
                <li class="technologies__item">Vue</li>
                <li class="technologies__item">Redux</li>
                <li class="technologies__item">Webpack</li>
                <li class="technologies__item">Gulp</li>
                <li class="technologies__item">Canvas</li>
                <li class="technologies__item">WebGL</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="services__item">
        <div class="service-card">
          <div class="service-card__inner">
            <div class="service-card__top">
              <h3 class="title service-card__title">QA</h3>
              <div class="service-card__icon service-card__icon--qa">
                <svg class="icon icon-webdesign">
                  <use xlink:href="#icon-webdesign"></use>
                </svg>
              </div>
            </div>
            <div class="service-card__middle">
              <p class="service-card__descr">Тестируем разработанные системы. Улучшаем работу отделов Backend и Frontend. Держим баланс между здоровым перфекционизмом и интересами бизнеса клиента. Составляем прозрачные тест-кейсы и понятные баг-листы.</p>
            </div>
            <div class="service-card__bottom">
              <ul class="technologies">
                <li class="technologies__item">Functional</li>
                <li class="technologies__item">Performance</li>
                <li class="technologies__item">Regression</li>
                <li class="technologies__item">Smoke</li>
                <li class="technologies__item">Load</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>