<div class="articles__content js-articles">

    @foreach($articles as $article)
        @include('components.article-item', [
            'show_image' => false, 
            'article' => $article 
        ])
    @endforeach 

</div>