<section class="page-section contacts" id="contacts">
    <div class="container">
        <h2 class="title section-title">Контакты</h2>
        <div class="contacts__inner">
            <div class="contacts__map-holder js-map">
                <div class="contacts__map-overlay">
                    <div class="contact-info">
                        <div class="contact-info__row"><span class="title contact-info__title">email</span><a
                                class="contact-info__link"
                                href="mailto:hello@hawkingbrothers.com">hello@hawkingbros.com</a></div>
                        <div class="contact-info__row"><span class="title contact-info__title">телефон</span><a
                                class="contact-info__link contact-info__phone" href="tel:+79036454125">+7 903
                                645-41-25</a></div>
                        <div class="contact-info__row"><span class="title contact-info__title">соцсети</span><a
                                class="contact-info__link" href="https://www.facebook.com/hawkingbrothers/"
                                target="_blank">Facebook</a></div>
                        <div class="contact-info__row"><span class="title contact-info__title">адреса офисов</span><span
                                class="contact-info__text contact-info__city contact-info__text--red js-contact-info-radio-text">г.
                                Москва</span><span class="contact-info__text contact-info__text--small">Ленинская
                                Слобода, 19</span></div>
                        <div class="contact-info__row"><span
                                class="contact-info__text contact-info__city js-contact-info-radio-text">г.
                                Владимир</span><span class="contact-info__text contact-info__text--small">Б. Московская,
                                д. 11, 3 этаж</span></div>
                        <div class="contact-info__row">
                            <div class="button contact-info__button js-open-contact-form">Написать нам</div>
                        </div>
                    </div>
                </div>
                <div class="map contacts__map">
                    <!--.map__close.js-close-map+svg('cross')
            -->
                </div>
            </div>
            <div class="contacts__form-holder js-form-holder">
                <div class="contacts__form-wrap">
                    @if (session('form_message'))
                        <div class="contact-form__succes-message js-form-succes" style="display:block;opacity:1;">
                            <div class="contact-form__succes-inner"><span class="title">Форма успешно отправлена.</span>
                            </div>
                        </div>
                    @endif
                    <form class="" action="{{ route('application.store') }}" method='post'>
                        @csrf
                        <div class="contact-form__row"><span class="title contact-form__title">Свяжитесь с нами</span>
                            <div class="contact-form__close js-close-contact-form"></div>
                        </div>
                        <div class="contact-form__row contact-form__row--name">
                            <input class="contact-form__input contact-form__field js-input js-input-name" type="text"
                                name="name" id="name" required>
                            <div class="contact-form__fill-line"></div>
                            <label class="contact-form__label js-form-label" for="name">Как к вам обращаться?</label>
                        </div>
                        <div class="contact-form__row">
                            <label class="contact-form__label js-form-label" for="phone">Телефон</label>
                            <input class="contact-form__input contact-form__field js-input js-phone customPhone"
                                type="tel" name="phone" id="phone" required>
                            <div class="contact-form__fill-line"></div>
                        </div>
                        <div class="contact-form__row">
                            <label class="contact-form__label js-form-label" for="email">Электропочта</label>
                            <input class="contact-form__input contact-form__field js-input customEmail" type="email"
                                name="email" id="email" required>
                            <div class="contact-form__fill-line"></div>
                        </div>
                        <div class="contact-form__row contact-form__flags">
                            @foreach ($services as $service)
                                <input class="contact-form__checkbox" type="radio" id="{{ $service->getCode() }}"
                                    name="service_id" value={{ $service->id }} checked>
                                <label class="contact-form__checkbox-label"
                                    for="{{ $service->getCode() }}">{{ $service->title }}</label>
                            @endforeach
                        </div>
                        <div class="contact-form__row contact-form__row--message">
                            <label class="contact-form__label js-form-label" for="message">Опишите вашу задачу</label>
                            <textarea
                                class="contact-form__input contact-form__field contact-form__input--textarea contact-form__input--textarea_scroll js-input"
                                name="message" id="message"></textarea>
                            <div class="contact-form__fill-line"></div>
                        </div>
                        <div class="contact-form__row contact-form__row--button">
                            <button class="contact-form__input button" type="submit">Отправить</button>
                        </div>
                        <div class="contact-form__row contact-form__row--terms"><span
                                class="contact-form__terms">Нажимая "Отправить" Вы соглашаетесь с&nbsp;</span><a
                                class="link--privacy" href="sogl.pdf" target="_blank">Политикой обработки персональных
                                данных</a></div>
                        <div class="form-error-container"></div>
                        <input class="contact-form__person js-input-person" type="text" name="person">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
