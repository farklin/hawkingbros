<div class="company-info__right">
    <div class="about-sibtitle">
      <h2 class="title about-section__ratings">Рейтинги и награды</h2>
    </div>
    <div class="achievements about-section__achievements">
      <div class="achievements__inner">
        <div class="achievements__item"><a class="achievement" href="https://www.facebook.com/hawkingbrothers/photos/a.946521168763976/2503821363033941/?type=3&amp;theater" target="_blank">
            <div class="achievement__top">
              <div class="achievement__icon picture"><img class="achievement__icon-img" src="assets/images/content/about/p1.svg" alt=""><span class="achievement__icon-text">1</span></div>
              <div class="achievement__value"></div>
            </div>
            <div class="achievement__bottom"><span class="achievement__text">место в конкурсе Р1</span></div></a></div>
        <div class="achievements__item"><a class="achievement" href="https://tagline.ru/web-developers-integrators-rating/" target="_blank">
            <div class="achievement__top">
              <div class="achievement__icon picture"><img class="achievement__icon-img" src="assets/images/content/about/tagline-label_2.svg" alt=""><span class="achievement__icon-text">2</span></div>
              <div class="achievement__value"></div>
            </div>
            <div class="achievement__bottom"><span class="achievement__text">место в России среди аутсорс-продакшнов</span></div></a></div>
        <div class="achievements__item"><a class="achievement" href="https://ruward.ru/index-ruward/united-web-rating-2019/" target="_blank">
            <div class="achievement__top">
              <div class="achievement__icon picture"><img class="achievement__icon-img achievement__icon-img--light" src="assets/images/content/about/Bitmap.svg" alt=""><img class="achievement__icon-img achievement__icon-img--dark" src="assets/images/content/about/Bitmap1.svg" alt=""><span class="achievement__icon-text achievement__icon-text_small">ТОП-100</span></div>
              <div class="achievement__value"></div>
            </div>
            <div class="achievement__bottom"><span class="achievement__text">в Едином рейтинге веб‑студий 2019</span></div></a></div>
        <div class="achievements__item"><a class="achievement" href="https://tagline.ru/web-developers-integrators-rating/" target="_blank">
            <div class="achievement__top">
              <div class="achievement__icon picture"><img class="achievement__icon-img" src="assets/images/content/about/tagline-label_2.svg" alt=""><span class="achievement__icon-text">63</span></div>
              <div class="achievement__value"></div>
            </div>
            <div class="achievement__bottom"><span class="achievement__text">место в России среди веб-интеграторов</span></div></a></div>
      </div>
    </div>
  </div>