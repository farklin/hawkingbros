<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Авторизация</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
</head>

<body>
    <div class="container">
        <form action="{{ route('auth') }}" method="POST">
            @csrf
            <div class="row">
                <label for="email">Email</label>
                <input class="form-control m-2" id="email" type="text" name="email">
                <label for="password">Пароль</label>
                <input class="form-control m-2" id="password" type="password" name="password">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value=""  name="remember" id="remember" checked>
                    <label class="form-check-label" for="remember">
                        Запомнить меня
                    </label>
                </div>

            </div>
            <div class="row error">
                @if (Session::has('error'))
                    <div class="alert alert-info">{{ Session::get('error') }}</div>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Войти</button>
        </form>
    </div>
   
</body>

</html>
