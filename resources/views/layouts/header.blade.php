<header class="header js-header header--main">
    <div class="container container--wide">
        <div class="header__inner">
            <div class="header__logo"><a class="logo" href="/">
                    <div class="logo__pic">
                        <svg class="icon logo">
                            <use xlink:href="#logo"></use>
                        </svg>
                    </div>
                </a>
                <div class="breadcrumbs header__bredcrumbs">
                    <ul class="breadcrumbs__list">
                        <li><span class="breadcrumbs__link"></span></li>
                        <li><span class="breadcrumbs__link"></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="header menu-tools js-menu-tools">
    <div class="container container--wide">
        <div class="header__inner">
            <div class="header__tools"><a class="header__lang lang" href="#">RU</a>
                <div class="burger-wrap js-burger">
                    <div class="burger">
                        <div class="burger__bar burger__bar--1"></div>
                        <div class="burger__bar burger__bar--2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main-menu-holder">
    <div class="container">
        <div class="main-menu-box">
            <div class="js-nav-menu nav-menu">
                <div class="nav-menu__head">
                    <div class="nav-menu__head-inner">
                        <div class="mode-icon js-switch-theme nav-menu__mode">
                            <svg class="icon night-mode">
                                <use xlink:href="#night-mode"></use>
                            </svg>
                            <svg class="icon day-mode">
                                <use xlink:href="#day-mode"></use>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="nav-menu__wrap">
                    <div class="nav-menu__inner js-menu-inner">
                        <nav class="main-menu nav-menu__menu menu-top--animate">
                          @isset($rightMenu)
                            <ul class="main-menu__list">
                                @foreach($rightMenu->items as $item)
                                <li class="main-menu__item"><a class="main-menu__link link--red js-link-services"
                                        href="{{$item->link}}">{{$item->name}}</a>
                                    @foreach($item->children as $child)
                                    <a class="main-menu__sublink link--red" href="{{$child->link}}">{{$child->name}}</a>
                                    @endforeach    
                                </li>
                                @endforeach
                            </ul>
                          @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
