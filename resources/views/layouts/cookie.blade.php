<div class="cookie js-cookie">
    <div class="container">
      <div class="cookie__inner"><span class="cookie__text">Мы используем файлы cookie, чтобы улучшить работу сайта. Дальнейшее пребывание на сайте означает согласие с их применением. <a href="#">Узнать подробнее</a></span>
        <div class="cookie__button button js-cookie-button">Принять</div>
      </div>
    </div>
  </div>