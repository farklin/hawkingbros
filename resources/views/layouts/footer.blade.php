<footer class="footer">
    <div class="container">
        <div class="footer__inner">
            <div class="footer__row">
                <div class="footer__logo"><a class="logo" href="/">
                        <div class="logo__pic">
                            <svg class="icon logo">
                                <use xlink:href="#logo"></use>
                            </svg>
                        </div>
                    </a></div>
            </div>
            <div class="footer__row"><a class="footer__privacy link--privacy" href="sogl.pdf" target="_blank">Политика
                    конфиденциальности</a><span class="footer__copyright">© 2014 - 2019 Hawking Brothers</span></div>
        </div>
    </div>
</footer>

<script defer type="text/javascript">
    function isAnyPartOfElementInViewport(el) {

        var rect = el.getBoundingClientRect();
        // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
        var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
        var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

        // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
        var vertInView = (rect.top <= windowHeight + 1000) && ((rect.top + rect.height) >= 0);
        var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

        return (vertInView && horInView);
    }

</script>
<script defer type="text/javascript">
    window.addEventListener('scroll', function() {
        var ymap = document.querySelector('.contacts');
        if (isAnyPartOfElementInViewport(ymap) && mapLoaded === false) {
            var elem = document.createElement('script');
            elem.type = 'text/javascript';
            elem.src = '/js/ymaps-touch-scroll.js';
            document.getElementsByTagName('body')[0].appendChild(elem);

        } else return;
    });

</script>
<script defer type="text/javascript">
    var mapLoaded = false
    window.addEventListener('scroll', function() {
        var ymap = document.querySelector('.contacts');
        if (isAnyPartOfElementInViewport(ymap) && mapLoaded === false) {
            yandexMapCreate();
            mapLoaded = true;
        } else return;
    });



    function yandexMapCreate() {
        var elem = document.createElement('script');
        elem.type = 'text/javascript';
        elem.src =
            '//api-maps.yandex.ru/2.1/?apikey=1b577d37-f341-456a-8734-b6a76134da85&lang=ru_RU&onload=yandexMapInit';
        document.getElementsByTagName('body')[0].appendChild(elem);
    }

    function yandexMapInit() {
        var mapContainer = document.querySelector('.map');
        if (mapContainer === null) {
            return;
        }

        var yMap = new ymaps.Map(mapContainer, {
            center: [56.13768336653939, 40.374368568354896],
            zoom: 16,
            controls: ['zoomControl', 'routeButtonControl']
        });
        var myGeoObject = new ymaps.GeoObject({
            geometry: {
                type: "Point",
                coordinates: [56.13768336653939, 40.374368568354896]
            }
        }, {
            iconColor: '#eb5757'
        });
        yMap.geoObjects.add(myGeoObject);
        ymapsTouchScroll(yMap);
    }

</script><!-- Yandex.Metrika counter -->
<!--
  <script async type="text/javascript">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(26349456, "init", {
         clickmap:true,
         trackLinks:true,
         accurateTrackBounce:true
    });
  </script>
  
  <noscript><div><img src="https://mc.yandex.ru/watch/26349456" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  
  -->
<!-- /Yandex.Metrika counter -->
