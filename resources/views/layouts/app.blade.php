<!DOCTYPE html>
<html lang="ru">
    @include('layouts/head')
    <body class="loading">
        <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i&display=swap&subset=cyrillic-ext,latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="css/libs.min.css">
        <link rel="stylesheet" href="css/main.css">
      </head>
      <body class="loading">
        @include('layouts.preload', ['title' => $title])
        @include('layouts.header')
        @yield('modal')
        <div class="wrapper">
          <div class="main-content js-main-content index-content">
            @yield('content')
            @include('layouts.footer')
            @include('layouts.cookie')
          </div>
        </div>
        @include('layouts.scripts')
    </body>
</html> 
