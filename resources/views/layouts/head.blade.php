<head>    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Мы специализируемся на создании и поддержке веб-проектов для бизнеса, digital-студий и агентств">
    <meta name="keywords" content="веб-интегратор, техническая поддержка, аутсорс, аутсорсинг веб-разработки, digital-продакшн, субподряд, создание сайтов">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:type" content="website">
    <meta property="og:url" content="https://hawkingbrothers.com/">
    <meta property="og:title" content="Hawking Brothers">
    <meta property="twitter:title" content="Hawking Brothers">
    <meta property="og:description" content="Аутсорсинг веб-разработки для бизнеса и digital-агентств">
    <meta property="twitter:description" content="Аутсорсинг веб-разработки для бизнеса и digital-агентств">
    <meta property="og:keywords" content="веб-интегратор, техническая поддержка, аутсорс, аутсорсинг веб-разработки, digital-продакшн, субподряд, создание сайтов">
    <meta property="og:site_name" content="Hawking Brothers">
    <meta property="og:image" content="https://hawkingbrothers.com/assets/images/og/og-image.png">
    <meta property="og:image:widht" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="vk:image" content="https://hawkingbrothers.com/assets/images/og/og-image-vk.png">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:image" content="https://hawkingbrothers.com/assets/images/og/og-image.png">
    <link rel="shortcut icon" href="./assets/images/favicon.png" type="image/x-icon">
    <script defer src="/js/libs.min.js"></script>
    <script defer src="/js/common.js"></script>
    <script async type="text/javascript">
      //При переносе на бой убрать приписки .html
      setTimeout(() =>{
          const preloaderPageTitle = document.querySelector('.preloader__page-title')
          if (window.location.pathname === '/') {
              preloaderPageTitle.innerText = 'Главная'
              if (window.location.hash === '#services-main') {
                  preloaderPageTitle.innerText = 'Услуги'
              } else if (window.location.hash === '#contacts') {
                  if (window.location.href.match(/en/)) {
                      preloaderPageTitle.innerText = 'Contacts'
                  } else {
                      preloaderPageTitle.innerText = 'Контакты'
                  }
              }
          } else if (window.location.pathname === '/about.html') {
              preloaderPageTitle.innerText = 'О компании'
              if (window.location.hash === '#reviews') {
                  preloaderPageTitle.innerText = 'Отзывы'
              } else if (window.location.hash === '#publications') {
                  preloaderPageTitle.innerText = 'Публикации'
              }
          } else if (window.location.pathname === '/career.html') {
              preloaderPageTitle.innerText = 'Вакансии'
              if (window.location.hash === '#corporate-culture') {
                  preloaderPageTitle.innerText = 'Корпоративная культура'
              }
          }
          preloaderPageTitle.classList.add('preloader__page-title--show')
      }, 135)
    </script>
    <title>Hawking Brothers. Веб-интегратор. Аутсорсинг веб-разработки для бизнеса и digital-агентств.</title>
    <!--script(defer type="text/javascript").-->
    <!---->
    <!--    var globe = document.createElement('script');-->
    <!--    globe.type = 'text/javascript';-->
    <!--    globe.src = 'js/sphere-app.js';-->
    <!--    globe.defer = true;-->
    <!---->
    <!--    var isMobile = window.innerWidth < 768;-->
    <!--    if (!isMobile) document.getElementsByTagName('head')[0].appendChild(globe);-->
    <!---->
    <!--    window.addEventListener('resize', function(){-->
    <!--      if(isMobile && window.innerWidth >= 768) {-->
    <!--        isMobile = false;-->
    <!--        document.getElementsByTagName('head')[0].appendChild(globe);-->
    <!--      }-->
    <!--    })-->
    <!---->
    <script defer src="js/sphere-app.js"></script><link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto+Condensed:300,300i,400,400i,700,700i|Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i&display=swap&subset=cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.css">
</head>