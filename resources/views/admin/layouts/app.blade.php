<!DOCTYPE html>
<html lang="ru" xml:lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="ru">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('admin-theme/font_awesome/css/all.min.css') }}">
    <link href="{{ asset('admin-theme/styles/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/styles/shards-dashboards.1.1.0.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/styles/extras.1.1.0.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin-theme/styles/bootstrap/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.snow.css">
</head>
<link rel='stylesheet' href='https://sachinchoolur.github.io/lightslider/dist/css/lightslider.css'>
<script async defer src="https://buttons.github.io/buttons.js"></script>
</head>

<body>



    <div class="container-fluid">
        @include('admin/components/left-menu/template')

        <!-- End Main Sidebar -->
        <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
            @include('admin/components/header/template')
            @include('admin/components/general/error')
            <!-- / .main-navbar -->

            {{-- <div class="main-content-container container-fluid px-4">
                <!-- Page Header -->
                <div class="page-header row no-gutters py-4">
                    <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                        <span class="text-uppercase page-subtitle">Dashboard</span>
                        <h3 class="page-title"> 
                            @if (View::hasSection('h1'))
                                @yield('h1')
                            @else
                                Интернет магазин TeisBubble
                            @endif 
                    </h3>
                    </div>
                </div>
                <!-- End Page Header -->

                @if (Session::has('info'))
                <div class="alert alert-accent alert-dismissible fade show mb-0" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                    <i class="fa fa-info mx-2"></i>
                    <strong>{{Session::get('info')}}</strong></div>
                @endif
                    
                @if (View::hasSection('content'))
                        @yield('content')
                    @else

                <div class="row">

                    <!-- New Draft Component -->
                    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
                        <!-- Quick Post -->
                        <div class="card card-small h-100">
                            <div class="card-header border-bottom">
                                <h6 class="m-0">Создать новый товар</h6>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <form class="quick-post-form">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputEmail1"
                                           placeholder="Название товара">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control"
                                            placeholder="Описание товара"></textarea>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6 col-md-6 col-sm-12"> 
                                        <input type="number" class="form-control" id="price"
                                           placeholder="Цена товара">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12"> 
                                        <input type="number" class="form-control" id="oldprice"
                                           placeholder="Старая цена товара">
                                        </div>
                                    </div>
                                    <div class="form-group custom-file">
                                        <input type="file" class="form-control custom-file-input" id="file"
                                        placeholder="Старая цена товара">
                                        <label for="file" class="custom-file-label">Изображения товара</label>
                                    </div>

                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-accent">Создать</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- End Quick Post -->
                    </div>
                    <!-- End New Draft Component -->
                    <!-- Discussions Component -->
                    <div class="col-lg-5 col-md-12 col-sm-12 mb-4">
                        <div class="card card-small blog-comments">
                            <div class="card-header border-bottom">
                                <h6 class="m-0">Новые отзывы</h6>
                            </div>
                            <div class="card-body p-0">
                                @if (!empty($comments))
                                @foreach ($comments as $comment)
                                    <div class="blog-comments__item d-flex p-3">
                                        <div class="blog-comments__avatar mr-3">
                                            @if (isset($comment->product->image) > 0)
                                            <img src="{{ $comment->product->images[0]->thumbnail() }}"/>
                                            @endif
                                        </div>
                                        <div class="blog-comments__content col-9">
                                            <div class="blog-comments__meta text-muted">
                                                <a class="text-secondary" href="#">{{ $comment->name }} </a> к
                                                <a class="text-secondary" href="{{ route('product.show', $comment->product->seo->slug) }}">{{ $comment->product->title }} </a>
                                                <span class="text-muted">{{ $comment->created_at }}</span>
                                            </div>
                                            <div class="blog-comments__avatar mr-3">
                                                @foreach ($comment->images as $image)
                                                <img src="{{ $image->thumbnail}}"/>
                                                @endforeach
                                            </div>

                                            <p class="m-0 my-1 mb-2 text-muted">{{ $comment->description}} </p>
                                            <div class="blog-comments__actions">
                                                <div class="btn-group btn-group-sm row">
                                                    <a type="button" href="{{ route('admin.comment.editstatus', $comment->id)}}" class="btn btn-white">
                                                        <span class="text-success">
                                                            <i class="fa fa-check"> </i>
                                                        </span> Опубликовать </a>
                                                    <a type="button" href="{{ route('admin.comment.delete', $comment->id)}}" class="btn btn-white">
                                                        <span class="text-danger">
                                                            <i class="fa fa-trash"> </i>
                                                        </span> Удалить </a>
                                                    <a type="button" class="btn btn-white text-dark">
                                                        <span class="text-light">
                                                            <i class="fa fa-edit"> </i>
                                                        </span> Изменить </a>
                                                </div>
                                            </div>



                                            
                                        </div>
                                    </div>
                                @endforeach
                                @endif 
                            </div>
                            <div class="card-footer border-top">
                                <div class="row">
                                    <div class="col text-center view-report">
                                        <a type="submit" href="{{route('admin.comment.all')}}" class="btn btn-white">Смотреть все отзывы</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Discussions Component -->
                    <!-- Top Referrals Component -->
                    <div class="col-lg-3 col-md-12 col-sm-12 mb-4">
                        <div class="card card-small">
                            <div class="card-header border-bottom">
                                <h6 class="m-0">Топ</h6>
                            </div>
                            <div class="card-body p-0">
                                <ul class="list-group list-group-small list-group-flush">
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">GitHub</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">19,291</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">Stack Overflow</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">11,201</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">Hacker News</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">9,291</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">Reddit</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">8,281</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">The Next Web</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">7,128</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">Tech Crunch</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">6,218</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">YouTube</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">1,218</span>
                                    </li>
                                    <li class="list-group-item d-flex px-3">
                                        <span class="text-semibold text-fiord-blue">Adobe</span>
                                        <span class="ml-auto text-right text-semibold text-reagent-gray">827</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-footer border-top">
                                <div class="row">
                                    <div class="col">
                                        <select class="custom-select custom-select-sm">
                                            <option selected>Last Week</option>
                                            <option value="1">Today</option>
                                            <option value="2">Last Month</option>
                                            <option value="3">Last Year</option>
                                        </select>
                                    </div>
                                    <div class="col text-right view-report">
                                        <a href="#">Full report &rarr;</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Top Referrals Component -->
                    @endif
                </div>
            
                    
            
            </div> --}}

            <div class="main-content-container container-fluid p-0 mt-3">
                @yield('content')
            </div>
        </main>
    </div>

    <script src="{{ asset('admin-theme/scripts/jquery/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/popper.min.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/jquery/jquery-ui.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/boostrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/classy-nav.min.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/active.js') }}"></script>
    <script src="https://unpkg.com/shards-ui@latest/dist/js/shards.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
    <script src="{{ asset('admin-theme/scripts/extras.1.1.0.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sharrre/2.0.1/jquery.sharrre.min.js"></script>
    <script src="{{ asset('admin-theme/scripts/shards-dashboards.1.1.0.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/quill/1.3.6/quill.min.js"></script>


    @yield('scripts')

</body>
