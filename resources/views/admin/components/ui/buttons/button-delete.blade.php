<form action="{{ $delete_link }}" method="POST">
    @csrf
    @method("DELETE")
    <button href="" class="btn btn-danger btn" title='Удалить'><i class="fa fa-minus"></i></a>
</form>
