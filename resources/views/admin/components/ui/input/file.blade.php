<div class="custom-file d-flex ">
    <label for="{{ $name }}" class="form-control-lg custom-file-label">{{ $label }}</label>
    <input type="file" name="{{ $name }}" id="{{ $name }}" class="form-control-lg custom-file-input">
</div>
