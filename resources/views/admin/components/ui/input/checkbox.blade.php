<div class="custom-control custom-checkbox mb-1">
    <input type="checkbox" name="{{ $name }}" id="{{ $name }}" @if ($status) checked="checked" @endif
        class="custom-control-input">
    <label for="{{ $name }}" class="custom-control-label">{{ $label }}</label>
</div>
