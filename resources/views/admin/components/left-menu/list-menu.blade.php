<div class="nav-wrapper">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.parthner.index') }}">
                <i class="fa fa-handshake"></i>
                <span>Партнеры</span>
            </a>
            <a class="nav-link" href="{{ route('admin.project.index') }}">
                <i class="fa fa-newspaper"></i>
                <span>Проекты</span>
            </a>
            <a class="nav-link" href="{{ route('admin.service.index') }}">
                <i class="fa fa-edit"></i>
                <span>Услуги</span>
            </a>
            <a class="nav-link" href="{{ route('admin.article.index') }}">
                <i class="fa fa-book"></i>
                <span>Статьи</span>
            </a>
            <a class="nav-link" href="{{ route('admin.menu.index') }}">
                <i class="fa fa-bars"></i>
                <span>Меню</span>
            </a>
            <a class="nav-link" href="{{ route('admin.review.index') }}">
                <i class="	far fa-comment"></i>
                <span>Отзывы</span>
            </a>
            <a class="nav-link" href="{{ route('admin.application.index') }}">
                <i class="fa fa-envelope"></i>
                <span>Заявки</span>
            </a>
        </li>
    </ul>
</div>
