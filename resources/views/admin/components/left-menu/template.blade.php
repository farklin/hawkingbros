<div class="row">
    <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
       @include('admin/components/left-menu/logo')
       @include('admin/components/left-menu/list-menu')
    </aside>
</div>