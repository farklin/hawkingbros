<div class="row">
    <div class="col-md-2 mr-2">
        @include('admin/components/ui/buttons/button-edit', ['edit_link'=>route('admin.'.$path.'.edit',
        $object)])
    </div>
    <div class="col-md-2 mr-2">
        @include('admin/components/ui/buttons/button-delete', ['delete_link'=>route('admin.'.$path.'.destroy',
        $object)])
    </div>
</div>
