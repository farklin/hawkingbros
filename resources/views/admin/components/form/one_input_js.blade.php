<div class="row item-forms-js">
    <div class="col-md-10">
    <input type="text" placeholder="{{$placeholder}}" name="{{$name}}" id="teams" value="{{$value}}" class="form-control form-control-sm mb-3">
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-danger remove-item-form"><i class="fa fa-minus"></i></button>
    </div>
</div>