@extends('admin/layouts/app')

@section('content')

    @hasSection('create_link')
    <div class="col-12">
        <a href="@yield('create_link')" class="btn btn-primary btn"><i class="fa fa-plus"></i>Создать</a>
    </div>
    @endif
    
    <div class="col-12 mt-2">
        <div class="card card-small mb-4">
            <div class="card-header border-bottom">
                <h6 class="m-0">@yield('title')</h6>
            </div>
            <div class="card-body p-0 pb-3 text-center">

                <table class="table mb-0">
                    
                    <thead class="bg-light">
                        <tr>
                        @hasSection('thead')
                            @yield('thead')
                        @endif 
                        </tr>
                    </thead>
                    <tbody>
                        @hasSection('tbody')
                            @yield('tbody')
                            @else
                            Элементов не найдено 
                        @endif 
                    </tbody>
                </table>
            </div>
        </div>
    
    </div>
    @hasSection('pagination')
        @yield('pagination')
    @endif
@endsection
