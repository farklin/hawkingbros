@extends('admin/layouts/app')

@section('content')
    <form class="row mt-5" @hasSection('form_action') action="@yield('form_action')" @endif @hasSection('form_method') method="@yield('form_method')" @endif enctype="multipart/form-data">
        @csrf
        @hasSection('form_method_laravel')
            @php
                $method_laravel = $__env->yieldContent('form_method_laravel'); 
            @endphp 
            @method($method_laravel)
        @endif

        <div class="col-lg-9 col-md-12">
            <div class="card card-small mb-3">
                <div class="card-body">
                    @yield('body')
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-12">

            @hasSection('right')
                <div class='card card-small mb-3'>
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Статус</h6>
                    </div>

                    <div class='card-body p-0'>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item px-3 pb-2">
                                @yield('right')
                            </li>
                        </ul>
                    </div>
                </div>
            @endif

            <div class='card card-small mb-3'>
                <div class="card-header border-bottom">
                    <h6 class="m-0">Действие</h6>
                </div>
                <div class='card-body p-0'>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex px-3">
                            <button class="'btn btn-sm btn-outline-accent">Сохранить</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </form>
@endsection
