@extends('admin/components/general/table')

@section('title', 'Отзывы')

@section('create_link', route('admin.review.create'))

@section('thead')
@endsection

@section('tbody')
    @foreach ($reviews as $review)
        <tr>
            <td>
                {{ $review->name }}
            </td>
            <td>
                {{ $review->comment }}
            </td>
            <td>
                @if ($review->status)
                    <i class="fa fa-check"></i>
                @else
                    <i class="fa fa-close"></i>'
                @endif
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'review', 'object' => $review])
            </td>
        <tr>
    @endforeach
@endsection
