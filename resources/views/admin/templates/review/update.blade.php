@extends('admin/components/general/form')

@section('title', 'Редактирование отзыва')

{{-- Настройки формы --}}
@section('form_action', route('admin.review.update', $review))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/review/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $review->status,
    ])
@endsection
