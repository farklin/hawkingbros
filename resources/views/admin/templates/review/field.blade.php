<label for="review_name">Имя</label>
<input type="text" name="name" id="review_name" value="{{ $review->name ?? '' }}"
    class="form-control form-control-lg mb-3">

<label for="review_post">Должность</label>
<input type="text" name="post" id="review_post" value="{{ $review->post ?? '' }}"
    class="form-control form-control-lg mb-3">

<div class="form-group col-md-12 p-0">
    <label for="review_comment">Коментарий</label>
    <textarea class="form-control" name="comment" rows="5">{{ $review->comment ?? '' }}</textarea>
</div>

<label for="review_company">Компания</label>
<input type="text" name="company" id="review_company" value="{{ $review->company ?? '' }}"
    class="form-control form-control-lg mb-3">


@include('admin/components/ui/input/file', [
'name' => 'image',
'label' => 'Изображение'
]
)
