<label for="">Название</label>
<input type="text" name="title" id="title" value="{{$project->title ?? ''}}" class="form-control form-control-lg mb-3">

<div class="row">
    <div class="col-md-6">
        <label for="">Объём</label>
    <input type="text" name="volume" id="volume" value="{{$project->volume ?? ''}}" class="form-control form-control-lg mb-3">

    </div>
    <div class="col-md-6">
        <label for="">Срок проекта</label>
        <input type="text" name="dedline" id="dedline" value="{{$project->dedline ?? ''}}" class="form-control form-control-lg mb-3">
    </div>
</div>


<label for="">URL адрес</label>
<input type="text" name="link" id="link" value="{{$project->link ?? ''}}" class="form-control form-control-lg mb-3">

@include('admin/components/ui/input/file', [
    'name' => 'image', 
    'label' => 'Изображение'
    ]
)

@isset($services)
<div class="form-group mt-3"> 
    <label for="">Услуга</label>
    @php
        $service_id = $project->service_id ?? 0; 
    @endphp
    <select name='service_id' class='form-control'>
        @foreach ($services as $service)
            <option @if($service_id == $service->id) selected @endif value="{{ $service->id }}">{{ $service->title }}</option>
        @endforeach
    </select>
</div>
@endif

<div class="row">
<div class="col-md-6 my-4 p-3 border">
    <h5>Стэк технологий</h5>
    <div class="container-item-js">
    @if(isset($project->solution_stacks) and count($project->solution_stacks) !=0)
        @foreach ($project->solution_stacks as $solution_stack)
        @include('admin/components/form/one_input_js', ['value' => $solution_stack->title, 'name' => 'solution_stacks[]', 'placeholder' => 'Стек технологий'])
        @endforeach
        @else
        @include('admin/components/form/one_input_js', ['value' => '', 'name' => 'solution_stacks[]', 'placeholder' => 'Стек технологий'])
    @endif
    </div>

    <div class="col-md-12">
        <button type="button" class="btn btn-primary add-item-form"><i class="fa fa-plus"></i> Добавить </button>
    </div>
</div>

<div class="col-md-6 my-4 p-3 border">
    <h5>Тэги</h5>
    <div class="container-item-js">
    @if(isset($project->tags) and count($project->tags) !=0)
        @foreach ($project->tags as $tag)
            @include('admin/components/form/one_input_js', ['value' => $tag->title, 'name' => 'tags[]', 'placeholder' => 'Теги'])
        @endforeach
        @else
            @include('admin/components/form/one_input_js', ['value' => '', 'name' => 'tags[]', 'placeholder' => 'Теги'])
    @endif
    </div>

    <div class="col-md-12">
        <button type="button" class="btn btn-primary add-item-form"><i class="fa fa-plus"></i> Добавить </button>
    </div>
</div>

</div>
<div class="row">
    <div class="col-md-6 my-4 p-3 border">
        <h5>Команда проекта</h5>
        <div class="container-item-js">
        @if(isset($project->teams) and count($project->teams) !=0)
            @foreach ($project->teams as $team)
                @include('admin/components/form/one_input_js', ['value' => $team->name, 'name' => 'teams[]', 'placeholder' => 'Команда'])
            @endforeach
            @else
            @include('admin/components/form/one_input_js', ['value' => '', 'name' => 'teams[]', 'placeholder' => 'Команда'])
        @endif
        </div>
    
        <div class="col-md-12">
            <button type="button" class="btn btn-primary add-item-form"><i class="fa fa-plus"></i> Добавить </button>
        </div>
    
    </div>
</div>



@section('scripts')
    <script src="{{ asset('admin-theme/scripts/form/add-item-form.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/form/remove-item-form.js') }}"></script>
@endsection 

