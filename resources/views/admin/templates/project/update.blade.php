@extends('admin/components/general/form')

@section('title', 'Редактирование проекта')

{{-- Настройки формы --}}
@section('form_action', route('admin.project.update', $project))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/project/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $project->status,
    ])
@endsection
