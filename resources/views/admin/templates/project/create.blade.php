@extends('admin/components/general/form')

@section('title', 'Добавление проекта')

{{-- Настройки формы --}}
@section('form_action', route('admin.project.store'))
@section('form_method', 'POST')
@section('form_method_laravel', 'POST')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/project/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => true,
    ])
@endsection
