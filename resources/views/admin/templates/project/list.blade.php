@extends('admin/components/general/table')

@section('title', 'Проекты')

@section('create_link', route('admin.project.create'))

@section('thead')
@endsection

@section('tbody')
    @foreach ($projects as $project)
        <tr>
            <td>
                <img src="{{ asset($project->getImageUrl()) }}" height="100">
            </td>
            <td>
                <a href="{{ route('admin.project.edit', $project) }}"> {{ $project->title }} </a>
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'project', 'object' => $project])
            </td>
        <tr>
    @endforeach
@endsection
