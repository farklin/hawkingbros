@extends('admin/components/general/form')

@section('title', 'Редактирование')

{{-- Настройки формы --}}
@section('form_action', '')
@section('form_method', '')
@section('form_method_laravel', '')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/application/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'application_status',
    'label' => 'Опубликован',
    'status' => true,
    ])
@endsection
