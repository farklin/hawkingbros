@extends('admin/components/general/form')

@section('title', 'Добавление настройки')

{{-- Настройки формы --}}
@section('form_action', '')
@section('form_method', '')
@section('form_method_laravel', '')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/setting/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'setting_status',
    'label' => 'Опубликован',
    'status' => true,
    ])
@endsection
