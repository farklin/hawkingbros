<div class="row">
    <div class="col-md-6">
        <label for="setting_name">Название</label>
        <input type="text" name="setting_name" id="setting_name" value="" class="form-control form-control-lg mb-3">
    </div>
    <div class="col-md-6">
        <label for="setting_key">KEY</label>
        <input type="text" name="setting_key" id="setting_key" value="" class="form-control form-control-lg mb-3">
    </div>
</div>


<div class = "col-md-12">
    <h4>Значения</h4>
    <div class="row mt-1 setting-value">
        <div class="col-md-5"><input type="text" name="setting_value_title[]" placeholder="Название*" value="" class="form-control form-control-lg mb-3"></div>
        <div class="col-md-5"><input type="text" name="setting_value_value[]" placeholder="Описание" value="" class="form-control form-control-lg mb-3"></div>
        <div class="col-md-2">
            @include('admin/components/ui/input/checkbox',
            [
            'name'=>'setting_value_status',
            'label' => 'Вкл.',
            'status' => true,
            ])
        </div>
    </div>
    
    <button class="'btn btn-sm btn-outline-accent" type="button" id="add_setting_value">Добавить</button> 
</div> 
