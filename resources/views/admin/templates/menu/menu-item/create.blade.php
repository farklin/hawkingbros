@extends('admin/components/general/form')

@section('title', 'Добавление пункта меню')

{{-- Настройки формы --}}
@section('form_action', route('admin.menu.item.store', $menu))
@section('form_method', 'POST')
@section('form_method_laravel', 'POST')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/menu/menu-item/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => true,
    ])
@endsection
