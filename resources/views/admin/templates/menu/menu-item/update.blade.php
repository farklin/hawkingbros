@extends('admin/components/general/form')

@section('title', 'Редактирование пункта меню')

{{-- Настройки формы --}}
@section('form_action', route('admin.item.update',  $menuItem))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/menu/menu-item/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $menuItem->status,
    ])
@endsection

