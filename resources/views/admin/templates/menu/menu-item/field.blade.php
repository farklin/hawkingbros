<label for="name">Название</label>
<input type="text" name="name" id="name" value="{{ $menuItem->name ?? '' }}"
    class="form-control form-control-lg mb-3">


<label for="link">Ссылка</label>
<input type="text" name="link" id="link" value="{{ $menuItem->link ?? '' }}"
    class="form-control form-control-lg mb-3">

<label for="sorting">Сортировка</label>
<input type="number" name="sorting" id="sorting" value="{{ $menuItem->sorting ?? 0 }}"
    class="form-control form-control-lg mb-3">

<input type="hidden" name="menu_id" value="{{ $menu }}"
class="form-control form-control-lg mb-3">


<div class="form-group mt-3"> 
    <label for="">Родитель</label>
    @php
        $menu_item_id = $menuItem->menu_item_id ?? 0; 
    @endphp
    <select name='menu_item_id' class='form-control'>
        <option value="">Не выбран</option>
        @foreach ($menus as $item)
            <option @if($menu_item_id == $item->id) selected @endif value="{{ $item->id }}">{{ $item->name }}</option>
        @endforeach
    </select>
</div>

    