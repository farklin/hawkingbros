@extends('admin/components/general/form')

@section('title', 'Редактирование меню')

{{-- Настройки формы --}}
@section('form_action', route('admin.menu.update', $menu))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/menu/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $menu->status,
    ])
@endsection

