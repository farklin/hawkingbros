@extends('admin/layouts/app')

@section('content')

    @foreach($menu->items as $item)
        <div class="col-md-12">
            <span><a href="{{route('admin.item.edit', $item)}}"> {{$item->name}} </a></span>
            <div>
                @foreach($item->children as $children)
                <div class="order-md-1 col-md-11 ">
                    <span><a href="{{route('admin.item.edit', $children)}}"> {{$children->name}} </a></span>
                </div>
                @endforeach
            </div>
        </div>
    @endforeach
    <a href="{{route('admin.menu.item.create', $menu)}}" class="btn btn-primary btn"><i class="fa fa-plus"></i>Создать пункт меню</a>
@endsection

