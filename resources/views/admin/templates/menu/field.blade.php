<label for="name">Название</label>
<input type="text" name="name" id="name" value="{{ $menu->name ?? '' }}"
    class="form-control form-control-lg mb-3">


<label for="code">Код</label>
<input type="text" name="code" id="code" value="{{ $menu->code ?? '' }}"
    class="form-control form-control-lg mb-3">
