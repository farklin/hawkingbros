@extends('admin/components/general/table')

@section('title', 'Меню')

@section('create_link', route('admin.menu.create'))

@section('thead')
@endsection

@section('tbody')
    @foreach ($menus as $menu)
        <tr>
            <td>
                <a href="{{ route('admin.menu.show', $menu) }}"> {{ $menu->name }} </a>
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'menu', 'object' => $menu])
            </td>
        <tr>
    @endforeach
@endsection

