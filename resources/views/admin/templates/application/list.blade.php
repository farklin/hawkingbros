@extends('admin/components/general/table')

@section('title', 'Заявки')

@section('create_link', '')

@section('thead')
@endsection

@section('tbody')
    @foreach ($applications as $application)
        <tr>
            <td>
                <a href="{{ route('admin.application.edit', $application->id) }}"> {{ $application->name }} </a>
            </td>
            <td>
                {{ $application->created_at }}
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'application', 'object' => $application])
            </td>
        </tr>
    @endforeach
@endsection

@section('pagination')
    {{ $applications->links('admin/components/general/pagination') }}
@endsection
