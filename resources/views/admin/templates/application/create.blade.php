@extends('admin/components/general/form')

@section('title', 'Добавление')

    {{-- Настройки формы --}}
@section('form_action', route('admin.application.store'))
@section('form_method', 'POST')
@section('form_method_laravel', 'POST')
    {{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/application/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'application_status',
    'label' => 'Обработан',
    'status' => true,
    ])
@endsection
