@extends('admin/components/general/form')

@section('title', 'Редактирование')

    {{-- Настройки формы --}}
@section('form_action', route('admin.application.update', $application))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
    {{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/application/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'application_status',
    'label' => 'Обработан',
    'status' => $application->status,
    ])
@endsection
