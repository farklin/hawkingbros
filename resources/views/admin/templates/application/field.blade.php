<label for="">Имя</label>
<input type="text" name="name" id="name" value="{{ $application->name ?? '' }}"
    class="form-control form-control-lg mb-3">

<label for="">Номер телефона</label>
<input type="text" name="phone" id="phone" value="{{ $application->phone ?? '' }}"
    class="form-control form-control-lg mb-3">

<label for="">Email</label>
<input type="email" name="email" id="email" value="{{ $application->email ?? '' }}"
    class="form-control form-control-lg mb-3">

<div class="form-group col-md-12 p-0">
    <label for="message">Задача</label>
    <textarea class="form-control" name="message" rows="5"> {{ $application->message ?? '' }} </textarea>
</div>


@isset($services)
    <div class="form-group mt-3">
        <label for="">Услуга</label>
        @php
            $service_id = $application->service_id ?? 0;
        @endphp
        <select name='service_id' class='form-control'>
            @foreach ($services as $service)
                <option @if ($service_id == $service->id) selected @endif
                    value="{{ $service->id }}">{{ $service->title }}</option>
            @endforeach
        </select>
    </div>
    @endif
