@extends('admin/components/general/form')

@section('title', 'Редактирование услуги')

{{-- Настройки формы --}}
@section('form_action', route('admin.service.update', $service))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/service/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $service->status,
    ])
@endsection
