<label for="name">Название</label>
<input type="text" name="title" id="title" value="{{ $service->title ?? '' }}" class="form-control form-control-lg mb-3">

<div class="form-group col-md-12 p-0">
    <label for="description">Описание</label>
    <textarea class="form-control" name="description" rows="10"> {{ $service->description ?? '' }} </textarea>
</div>


<div class="col-md-12 my-4 p-3 border">
    <h5>Решения</h5>
    <div class="container-item-js">
    @if(isset($service->solutions) and count($service->solutions) !=0)
        @foreach ($service->solutions as $solution)
        @include('admin/components/form/one_input_js', ['value' => $solution->value, 'name' => 'solutions[]', 'placeholder' => 'Решения'])
        @endforeach
        @else
            @include('admin/components/form/one_input_js', ['value' => '', 'name' => 'solutions[]', 'placeholder' => 'Решения'])
    @endif
    </div>

    <div class="col-md-12">
        <button type="button" class="btn btn-primary add-item-form"><i class="fa fa-plus"></i> Добавить </button>
    </div>
</div>


@include('admin/components/ui/input/file', [
'name' => 'image',
'label' => 'Изображение'
]
)


@section('scripts')
    <script src="{{ asset('admin-theme/scripts/form/add-item-form.js') }}"></script>
    <script src="{{ asset('admin-theme/scripts/form/remove-item-form.js') }}"></script>
@endsection 