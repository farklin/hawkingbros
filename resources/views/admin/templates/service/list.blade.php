@extends('admin/components/general/table')

@section('title', 'Услуги')

@section('create_link', route('admin.service.create'))

@section('thead')
@endsection

@section('tbody')
    @foreach ($services as $service)
        <tr>
            <td>
                <img src="{{ asset($service->getImageUrl()) }}" height="100">
            </td>
            <td>
                <a href="{{ route('admin.service.edit', $service) }}"> {{ $service->title }} </a>
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'service', 'object' => $service])
            </td>
        <tr>
    @endforeach
@endsection
