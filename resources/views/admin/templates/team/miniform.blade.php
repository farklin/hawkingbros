<div class="row item-forms-js">
    <div class="col-md-11">
        <input type="text" placeholder="Команда проекта" name="teams[]" id="teams" value="{{$value}}" class="form-control form-control-lg mb-3">
    </div>
    <div class="col-md-1">
        <button type="button" class="btn btn-danger remove-item-form"><i class="fa fa-minus"></i></button>
    </div>
</div>