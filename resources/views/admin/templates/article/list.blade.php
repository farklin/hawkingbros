@extends('admin/components/general/table')

@section('title', 'Статьи')

@section('create_link', route('admin.article.create'))

@section('thead')
@endsection

@section('tbody')
    @foreach ($articles as $article)
        <tr>
            <td>
                <a href="{{ route('admin.article.edit', $article) }}"> {{ $article->title }} </a>
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'article', 'object' => $article])
            </td>
        <tr>
    @endforeach
@endsection
