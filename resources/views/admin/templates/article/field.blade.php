<label for="">Название</label>
<input type="text" name="title" id="title" value="{{ $article->title ?? '' }}"
    class="form-control form-control-lg mb-3">

<div class="form-group col-md-12 p-0">
    <label for="review_comment">Короткое описание</label>
    <textarea class="form-control" name="short_description"
        rows="5">{{ $article->short_description ?? '' }}</textarea>
</div>

<div class="form-group col-md-12 p-0">
    <label for="review_comment">Описание</label>
    <textarea class="form-control" name="content" rows="5">{{ $article->content ?? '' }}</textarea>
</div>


<label for="url">URL адрес</label>
<input type="text" name="url" id="url" value="{{ $article->url ?? '' }}" class="form-control form-control-lg mb-3">

@include('admin/components/ui/input/file', [
'name' => 'image',
'label' => 'Изображение'
]
)
