@extends('admin/components/general/form')

@section('title', 'Редактирование статьи')

{{-- Настройки формы --}}
@section('form_action', route('admin.article.update', $article))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/article/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $article->status,
    ])
@endsection
