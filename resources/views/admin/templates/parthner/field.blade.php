<label for="title">Название</label>
<input type="text" name="title" id="title" value="{{ $parthner->title ?? '' }}"
    class="form-control form-control-lg mb-3">

<label for="link">URL адрес</label>
<input type="text" name="link" id="link" value="{{ $parthner->link ?? '' }}" class="form-control form-control-lg mb-3">


@include('admin/components/ui/input/file', [
'name' => 'image',
'label' => 'Изображение',
]
)


@isset($parthner->image)
    <div class="mt-2">
        <img src="{{ asset($parthner->image->path) }}" alt="">
    </div>
    @endif
