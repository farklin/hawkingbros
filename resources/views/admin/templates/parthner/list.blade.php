@extends('admin/components/general/table')

@section('title', 'Партнеры')

@section('create_link', route('admin.parthner.create'))

@section('thead')
@endsection

@section('tbody')
    @foreach ($parthners as $parthner)
        <tr>
            <td>
                <img src="{{ asset($parthner->getImageUrl()) }}" height="100">
            </td>
            <td>
                <a href="{{ route('admin.parthner.edit', $parthner) }}"> {{ $parthner->title }} </a>
            </td>
            <td>
                @include('admin/components/form/controller', ['path'=> 'parthner', 'object' => $parthner])
            </td>
        <tr>
    @endforeach
@endsection

@section('pagination')
    {{ $parthners->links('admin/components/general/pagination') }}
@endsection
