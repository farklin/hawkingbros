@extends('admin/components/general/form')

@section('title', 'Редактирование партнера')

{{-- Настройки формы --}}
@section('form_action', route('admin.parthner.update', $parthner))
@section('form_method', 'POST')
@section('form_method_laravel', 'PATCH')
{{-- Конец настройки формы --}}

@section('body')
    @include('admin/templates/parthners/field')
@endsection

@section('right')
    @include('admin/components/ui/input/checkbox',
    [
    'name'=>'status',
    'label' => 'Опубликован',
    'status' => $parthner->status,
    ])
@endsection
