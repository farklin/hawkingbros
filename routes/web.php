<?php

use Illuminate\Support\Facades\Route;
use App\Models\Service\Project;
use App\Models\Service\Parthner;
use App\Models\Service\Service;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Главная страница
Route::get('/', MainController::class)->name('home') ;

/**
 * Заглушки
 */ 
Route::get('/article/{url}', function ($url) {
    $article = App\Models\Article::where('url', $url)->first();
    $title = $article->title;
    return view('templates.article', compact('title', 'article'));
})->name('article');;

Route::get('/articles', function () {
    $title = 'Статьи';
    $articles = App\Models\Article::all();
    $tags = App\Models\Tag::all();
    return view('templates.articles', compact('title', 'articles', 'tags'));
});

Route::get('/about', function () {
    $title = 'О компании';
    return view('templates.about', compact('title'));
});

Route::get('/career-detail', function () {
    $title = 'Frontend-разработчик';
    return view('templates.career-detail', compact('title'));
});

Route::get('/career', function () {
    $title = 'Вакансии';
    return view('templates.career-detail', compact('title'));
});

Route::get('/digital', function () {
    $title = 'Digital-разработка и усиление IT-команд';
    return view('templates.digital', compact('title'));
});

Route::get('/up-team', function () {
    $title = 'Усиление IT-команд';
    return view('templates.up-team', compact('title'));
});

/**
 * @ЗаглушкиАксессуары для ванной комнаты
 */

// Проекты 
Route::get('/cases', [App\Http\Controllers\Service\ProjectController::class, 'index'])->name('project.index');
Route::get('/cases/filter', [App\Http\Controllers\Service\ProjectController::class, 'filter'])->name('project.filter');

//Заявки
Route::post('/application', [App\Http\Controllers\Application\ApplicationController::class, 'store'])->name('application.store');


Route::get('/lesson18', [App\Http\Controllers\Lesson\Lesson18Controller::class, 'index'])->name('lesson18');

/**
 * Форма создания поста
 */
Route::get('/post/create', [App\Http\Controllers\PostController::class, 'create'])->name('post-create');
Route::post('/post/store', [App\Http\Controllers\PostController::class, 'store'])->name('post-store');

/**
 * создайте именованный роут для личного кабинета вида ‘/profile/{id}’, 
 */
Route::get('/login', [App\Http\Controllers\AuthController::class, 'login'])->name('login');
Route::get('/message', [App\Http\Controllers\AuthController::class, 'message'])->name('message');
Route::get('/register', [App\Http\Controllers\AuthController::class, 'register'])->name('register');
Route::post('/registration', [App\Http\Controllers\AuthController::class, 'registration'])->name('registration');
Route::post('/auth', [App\Http\Controllers\AuthController::class, 'auth'])->name('auth');
Route::get('/profiel/{user}', [App\Http\Controllers\AuthController::class, 'profiel'])->where('user', '[0-9]{1,10}')->name('profiel')->middleware('idProfielUserValid');

Route::get('/file', [App\Http\Controllers\FileControlller::class, 'getFile'])->name('getFile');
Route::get('/exist-file', [App\Http\Controllers\FileControlller::class, 'existFile'])->name('existFile');
Route::get('/download-file', [App\Http\Controllers\FileControlller::class, 'downloadFile'])->name('downLoadFile');