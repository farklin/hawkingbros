$( document ).ready(function() {
    $('.cases .js-filter-item__input').change(function () {
        var formCases = document.querySelector('.cases .js-filter');
        var data = new FormData(formCases);
        $.ajax({
            method: formCases.method,
            url: formCases.action,
            data: data,
            processData: false,
            contentType: false,
            dataType: 'json',
            complete: function (data) {
                if (data.responseJSON.status === 'success') {
                    $('.js-portfolio').html(data.responseJSON.items);
                }
            }
        })
    })
});

