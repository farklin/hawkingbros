document.addEventListener("DOMContentLoaded", function() {
    const corpPortalsCanvas = document.querySelector('.corp-portals-canvas');

    const ctx = corpPortalsCanvas.getContext('2d');
    const ctxWidth = ctx.canvas.width;
    const ctxHeight = ctx.canvas.height;

    const opts = {
        countParticles: 1000,

        defaultSpeed: 0.5,
        addedSpeed: 0,

        defaultRadius: 1,
        addedRadius: 0.5
    }

    const particles = [];

    class Particle {
        constructor() {
            this.position = {
                x: Math.random() * ctxWidth,
                y: Math.random() * ctxHeight
            };

            this.radius = opts.defaultRadius + Math.random() * opts.addedRadius;
            this.speed = opts.defaultSpeed + Math.random() * opts.addedSpeed;
            this.directionAngle = Math.floor(Math.random() * 360);
            this.direction = {
                x: Math.cos(this.directionAngle) * this.speed,
                y: Math.sin(this.directionAngle) * this.speed
            };
            this.opacity = Math.random();
            this.inSquare = this.isPointInTriangle(this.position.x, this.position.y);
        }

        draw() {
            ctx.beginPath();
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.fillStyle = `rgba(235, 87, 87, ${this.opacity})`;
            ctx.fill();
            ctx.closePath();
        }

        update() {
            this.checkBorder();

            this.position.x += this.direction.x;
            this.position.y += this.direction.y;
        }

        squareTriangle(aAx, aAy, aBx, aBy, aCx, aCy) {
            return Math.abs(aBx * aCy - aCx * aBy - aAx * aCy + aCx * aAy + aAx * aBy - aBx * aAy);
        }

        isPointInTriangle(x, y) {
            const sumSquarePart = this.squareTriangle(x, y, 360, 0, 520, 520) + this.squareTriangle(x, y, 520, 520, 0, 375) + this.squareTriangle(x, y, 0, 375, 360, 0);
            const sumSquareFull = this.squareTriangle(360, 0, 520, 520, 0, 375);

            return Math.abs(sumSquareFull - sumSquarePart) > 0.01
        }

        isPointInCircle(x, y, cx, cy, r) {
            return (x - cx) * (x - cx) + (y - cy) * (y - cy) >= r * r;
        }

        checkBorder() {
            if (this.isPointInTriangle(this.position.x, this.position.y)) {
                this.direction.x *= -1;
                this.direction.y *= -1;
            }

            // if (this.isPointInCircle(this.position.x, this.position.y, ctxWidth / 2, ctxHeight / 2, ctxWidth / 2)) {
            //     this.direction.x *= -1;
            //     this.direction.y *= -1;
            // }
        }
    }

    class Triangle {
        constructor(x11, y11, x12, y12, x13, y13) {
            this.x11 = x11;
            this.y11 = y11;
            this.x12 = x12;
            this.y12 = y12;
            this.x13 = x13;
            this.y13 = y13;
        }

        draw(isMask) {
            ctx.beginPath();
            ctx.moveTo(this.x11, this.y11);
            ctx.lineTo(this.x12, this.y12);
            ctx.lineTo(this.x13, this.y13);
            ctx.closePath();

            isMask ? ctx.clip() : ctx.stroke();
        }
    }

    class Sphere {
        constructor(x, y, radius) {
            this.position = {
                x: x ? x : ctxWidth / 2,
                y: y ? y : ctxHeight / 2
            },
            this.radius = radius ? radius : ctxWidth / 2
        }

        draw(isMask) {
            ctx.beginPath();
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.closePath();

            isMask ? ctx.clip() : ctx.stroke();
        }
    }

    function setup() {
        for (let i = 0; i < opts.countParticles; i++) {
            const particle = new Particle();

            /* TODO отрисовка внутри контура */
            if (!particle.inSquare) {
                particles.push(particle);
            }
        }

        new Triangle(360, 0, 520, 520, 0, 375).draw(true); // отрисовка контура, но без лагов
        // new Sphere().draw(true);

        window.requestAnimationFrame(loop);
    }

    function loop() {
        ctx.clearRect(0, 0, ctxWidth, ctxHeight);
        // const triangle = new Triangle(360, 0, 520, 520, 0, 375).draw(); //для отрисовки контура (лагает)
        // new Sphere().draw();

        window.requestAnimationFrame(loop);

        for (let i = 0; i < particles.length; i++) {
            particles[i].update();
            particles[i].draw();
        }
    }

    setup();
});


