function getScrollbarWidth() {
  return scrollBarWidth = window.innerWidth - $(window).width();
}

$(function() {

  var menuOpen = false;

  $('.js-burger').on('click', function() {
    
    navMenuToggle();
    
  })
  var sUsrAg = navigator.userAgent;

   if (sUsrAg.indexOf("Safari") > -1) {
    $('.nav-menu__menu').removeClass('menu-top--animate-delay')
  } else {
    $('.nav-menu__menu').addClass('menu-top--animate-delay')
  }
  $('.js-menu-anchor').each(function () {
    $(this).on('click', function(event) {
        var re =/#([\w+.-]+)/;
        var link = $(this).attr('href').match(re)[0];

        var scrolled = window.pageYOffset;
        var offsetTop = $(link).offset().top;

        if(scrolled > offsetTop) {
          offsetTop -= $('.header').innerHeight();
        }
      $('html, body').animate({scrollTop:offsetTop},600)
      closeMenu()
    });
  })
  var $menu = $('.js-nav-menu');

  function navMenuToggle() {

      var scrollbarWidth = getScrollbarWidth();

      if (!menuOpen) {
        $menu.addClass('is-active');
        $('.js-burger').addClass('is-active');
        $('.js-menu-tools').css({'padding-right' : scrollBarWidth});
        $('.js-header').css({'padding-right' : scrollBarWidth});
        $('body').css({'overflow' : 'hidden', 'padding-right' : scrollBarWidth});
        $('.nav-menu__menu').css({'padding-right' : scrollBarWidth});
        setTimeout(function(){
          $('.js-main-content').addClass('bg--blur');
          $('.js-header').addClass('is-blur');
          $('.js-cookie').css({
            'position': 'absolute',
            'bottom': (document.documentElement.offsetHeight - window.pageYOffset - window.innerHeight) + 'px'
          });
        },50);
        menuOpen = true;
      } else {
        closeMenu();
      }
  }

  $('.js-menu-inner').each(function(index, element) {
    var menupPs = new PerfectScrollbar(element, {
      wheelSpeed: 1,
      minScrollbarLength: 10,
      maxScrollbarLength : 70,
      suppressScrollX: true
    });
  
    menupPs.update();
    $(window).on('resize', function() {
      menupPs.update();
    });

    element.scrollTop += 1;
    element.scrollTop -= 1;
  });
  $('.js-main-content, .js-header').on('click', function() {
    closeMenu();
  })

  function closeMenu() {
    $menu.removeClass('is-active');
    $('.js-burger').removeClass('is-active');
    $('.js-header').css({'padding-right' : 0});
    $('.js-menu-tools').css({'padding-right' : 0});

    if($('.reviews-popup').hasClass('is-show')){
      $('body').attr('style' , 'overflow: hidden');
    }else{
      $('body').css({'overflow' : 'auto', 'padding-right' : 0});
    }
    
    $('.js-main-content').removeClass('bg--blur');
    $('.js-header').removeClass('is-blur');
    $('.js-cookie').css({
      'position': '',
      'bottom': ''
    });
    $('.nav-menu__menu').css({'padding-right' : ''});

    menuOpen = false;

  }

  $('.js-switch-theme').on('click', function() {
    $('html').toggleClass('is-dark');
    var currentMode =  $('html').attr('class');
    document.cookie = 'currentTheme=' + currentMode + ' max-age=2592000; path=/;';
  })

  function isDarkThemeSelected() {
    return document.cookie.match(/currentTheme=is-dark/i) != null
  }

  function setThemeFromCookie() {
    var root = $('html');

    if (isDarkThemeSelected()) {
      root.addClass('is-dark')
    } else {
      root.removeClass('is-dark')
    }

  }

  setThemeFromCookie()

  $('.js-link-contacts').on('click', function() {
    var $targetElement = $('#contacts');
    if ($targetElement.length) {
      closeMenu();
      event.preventDefault();
      var re =/#([\w+.-]+)/;
      var link = $(this).attr('href').match(re)[0];

      var scrolled = window.pageYOffset;
      var offsetTop = $(link).offset().top;

      if(scrolled > offsetTop) {
        offsetTop -= $('.header').innerHeight();
      }
      $('html, body').animate({scrollTop:offsetTop},600)
    }
  })
  $('.js-link-services').on('click', function() {
    var $targetElement = $('#services');
    if ($targetElement.length) {
      closeMenu();
      event.preventDefault();
      var re =/#([\w+.-]+)/;
      var link = $(this).attr('href').match(re)[0];
      
      var scrolled = window.pageYOffset;
      var offsetTop = $(link).offset().top;

      if(scrolled > offsetTop) {
        offsetTop -= $('.header').innerHeight();
      }

      $('html, body').animate({scrollTop:offsetTop},600)
    }
  })

  if(location.hash === '#services' || location.hash === '#contacts') {
    $(window).trigger('scroll', true);
  }
})