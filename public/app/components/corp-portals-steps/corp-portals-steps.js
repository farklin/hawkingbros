$(function() {
    var timeline = $('.corp-portals-steps__timeline');

    if (timeline.length) {
        var listSteps = $('.corp-portals-steps__list');
        var itemsSteps = $('.corp-portals-steps__item');

        function calcHeightTimeline() {
            var heightListSteps = listSteps.outerHeight();
            var halfHeightFirstStep = $(itemsSteps[0]).outerHeight() / 2;
            var halfHeightLastStep = $(itemsSteps[itemsSteps.length - 1]).outerHeight() / 2;
    
            $(timeline).css({
                top: halfHeightFirstStep,
                height: heightListSteps - (halfHeightFirstStep + halfHeightLastStep),
            });
        }

        function animateItems() {
            var scroll = $(window).scrollTop() + $(window).height();
            var isAnimatedItem = 0;
    
            $(itemsSteps).each(function() {
                isAnimatedItem = 0;
                var heightItem = $(this).outerHeight();
    
                if (scroll - heightItem - heightItem / 2 > $(this).offset().top && isAnimatedItem == 0) {
                    $(this).addClass('fade-in');
                    isAnimatedItem = 1;
                }
            });
        }

        var timelineFill = $('.corp-portals-steps__timeline-fill');
        var circlesSteps = $('.corp-portals-steps__circle');

        function animateTimeline() {
            var heightListSteps = $(listSteps).outerHeight();
            var controlPoint = $(window).scrollTop() + $(window).height() / 2;
            var topTimelineFill = timelineFill.offset().top;
            var heightTimelineFill = controlPoint - topTimelineFill;

            if ($(window).scrollTop() + $(window).height() > topTimelineFill && heightTimelineFill < heightListSteps) {
                $(timelineFill).css({height: heightTimelineFill});

                $(circlesSteps).each(function() {
                    if ($(this).offset().top <= controlPoint) {
                        $(this).addClass('is-active');
                    } else {
                        $(this).removeClass('is-active');
                    }
                });
            }
        }
        
        calcHeightTimeline();
        animateItems();
        animateTimeline();

        $(window).on('resize', function() {
            calcHeightTimeline();
            animateItems();
            animateTimeline();
        });

        $(window).on('scroll' , function() {
            animateItems();
            animateTimeline();
        })
    }
});