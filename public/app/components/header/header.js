$(function () {

    // IE detection

    if (!!window.MSInputMethodContext && !!document.documentMode) {
        $('body').addClass('browser-ie');
    }


    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    var idEdge = !isIE && !!window.StyleMedia

    if (!isIE && !!window.StyleMedia) {
        $('body').addClass('browser-edge');
    }
    // Android detection

    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1;

    if (isAndroid) {
        $('body').addClass('is-android');
    }

    //Touch detection
    if (!("ontouchstart" in document.documentElement)) {
        $('body').addClass('no-touch');
    }

    // Header scroll

    var prevScroll = window.pageYOffset;
    var $header = $('.header');

    if (prevScroll > 0) {
        $header.addClass('is-fixed');
        addShadow();
        $(this).trigger('scroll');
    }
    if (window.location.hash.match(/#\w/) && prevScroll > 0) {
        $header.addClass('is-hide')
    }
    function headerScroll(event, isAnchor) {
        var currentScroll = window.pageYOffset;
        var delta = currentScroll - prevScroll;
        if (delta < 0) {
            var bdr = window.innerWidth >= 768 ? 0 : 4
            if (currentScroll <= bdr) {
                $header.css('transition', 'none').removeClass('is-fixed is-hide is-show');
                removeShadow();
                setTimeout(function () {
                    $header.css('transition', '');
                }, 0);
            }
            if ($header.is('.is-fixed')) {
                $header.removeClass('is-hide').addClass('is-show');
                addShadow();
            }
        } else if (delta > 0 || isAnchor) {
            if ($header.is('.is-fixed')) {
                $header.removeClass('is-show').addClass('is-hide');
            } else {
                if (currentScroll > $header.outerHeight()) {
                    $header.css('transition', 'none').addClass('is-fixed is-hide');
                    setTimeout(function () {
                        $header.css('transition', '');
                    }, 0);
                }
            }
        }

        prevScroll = currentScroll;
    }

    $(window).on('scroll', headerScroll);

    function addShadow() {
        $('.js-header').addClass('header--shadow')
    }

    function removeShadow() {
        $('.js-header').removeClass('header--shadow')
    }

    var carousel = $('.triangle__carousel')[0];
    var angle = 0;
    var last = 1;
    var intervalAnimation = 5 * 1000;
    var triangleText = $('.triangle__text');
    var startAnimateElement = 0;

    function rotateCarousel(now) {
        if (!last || now - last >= intervalAnimation) {
            last = now;

            angle += -120;
            carousel.style.transform = 'translateZ(-40px) rotateX(' + angle + 'deg)';
        }
        requestAnimationFrame(rotateCarousel);
    }

    function rotateCarouselIE() {
        setInterval(function () {
            $(triangleText[startAnimateElement]).animate({"opacity": "0"}, 1000);
            $(triangleText[startAnimateElement + 1]).animate({"opacity": "1"}, 1000);

            if (startAnimateElement == 2) {
                startAnimateElement = 0;
                $(triangleText[startAnimateElement]).animate({"opacity": "1"}, 1000);
            } else {
                startAnimateElement++;
            }
        }, intervalAnimation)
    }

    !!carousel && !isIE && window.requestAnimationFrame(rotateCarousel);
    !!carousel && isIE && rotateCarouselIE();
})

// lazy image load

window.addEventListener('DOMContentLoaded', function () {
    $('.js-lazy').lazy({
        threshold: 1200
    });
})