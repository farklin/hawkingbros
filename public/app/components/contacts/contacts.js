$(function () {

  // select2 init

  $('.js-contact-select').select2({
    dropdownPosition: 'below',
    dropdownCssClass: 'no-search',
    placeholder: {
      id: '',
      text: ''
    },
    dropdownParent: $('.js-contact-select-row'),
    closeOnSelect: false,
    multiple: true
  });

  $('.js-contact-select').on('select2:open', function (e) {
    var evt = "scroll.select2";
    $(e.target).parents().off(evt);
    $(window).off(evt);
  });


  // Contact form labels effects

  $('.js-input').on('focus', function () {
    $(this).siblings('.js-form-label').addClass('is-up');
  })
  if($('.contact-form__input--textarea').value){
    $(this).closest('.contact-form__row--message').children('.js-form-label').addClass('is-up');
  }
  $('.contact-form__input--textarea').on('focus', function(){
    $(this).closest('.contact-form__row--message').children('.js-form-label').addClass('is-up');
    $(this).closest('.contact-form__row--message').children('.contact-form__fill-line').css('width', '100%');
  })
  $('.contact-form__input--textarea').on('focusout', function(){
    $(this).closest('.contact-form__row--message').children('.contact-form__fill-line').css('width', '0');
  })

  $('.js-input').on('blur', function () {
    if (!$(this).val().length) {
      $(this).siblings('.js-form-label').removeClass('is-up');
      if($(this).hasClass('contact-form__input--textarea')){
        $(this).closest('.contact-form__row--message').children('.js-form-label').removeClass('is-up');
        
      }
    }
  })

  $('.js-contact-select').on('select2:opening select2:change', function () {
    $(this).siblings('.js-form-label').addClass('is-up');
    $selectContainer.find('.contact-form__fill-line').removeClass('select-error');
  })

  $('.js-contact-select').on('select2:close', function (e) {
    var $selectedOptions = $('.select2-selection__choice');

    if (!$selectedOptions.length) {
      $(this).siblings('.js-form-label').removeClass('is-up');

    }

  })

  var ps; // Perfect Scrollbar container

  $('.js-contact-select').on('select2:open', function (e) {

    var $selectResulsts = $('.select2-results__options');

    setTimeout(function () {
      ps = new PerfectScrollbar($selectResulsts[0], {
        wheelSpeed: 1,
        minScrollbarLength: 10,
        maxScrollbarLength: 70,
        suppressScrollX: true
      });
    }, 10)

  })

  $('.js-contact-select').on('select2:closing', function (e) {
    ps.destroy();
    ps = null;
  })

  // Contact form validation

  $('html[lang="ru"] .js-phone').inputmask({
    'mask': '+7 (999) 999-99-99',
    showMaskOnHover: false,
    onBeforePaste: function (pastedValue, opts) {
      var newValue = pastedValue.replace(/[^0-9]/ig, '');
      newValue = newValue.substring(newValue.length - 10);
      return newValue;
    },
  });

  $('html[lang="en"] .js-phone').on('input', function() {
    var value = $(this).val();
    value = value.replace(/[^0-9\+\-\(\)\s)]/ig, '');
    $(this).val(value);
  });

  var $contactForm = $('.js-contact-form'),
    $errorContainer = $contactForm.find('.form-error-container'),
    $selectContainer = $('.js-contact-select-row'),
    $hiddenField = $contactForm.find('.js-input-person');

  $('form').on('formReset', function () {
    $(this)[0].reset();
    $(this).find('.contact-form__fill-line, input').removeClass('error select-error');
    $(this).find('.js-file-name').html('Прикрепить резюме');
    $(this).find('.js-contact-select').val('');
    $(this).find('.select2-selection__choice').remove();
    $(this).find('.js-form-label').removeClass('is-up');
  })

  $('form').on('formSubmit', function () {
    var $formSucces = $(this).siblings('.js-form-succes');

    $(this).trigger('formReset');
    $formSucces.css({ 'display': 'block' });
    $('.contact-form__input').blur();
    $formSucces.stop().animate({
      opacity: 1,
    }, 200)
    setTimeout(function () {
      $formSucces.stop().animate({
        opacity: 0,
      }, 200)
      $formSucces.hide(200);
    }, 3000)
  })

  jQuery.validator.addMethod("customEmail", function (value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
  }, '');

  jQuery.validator.addMethod("customPhone", function (value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional(element) || (document.documentElement.lang !== 'en' ? 10 === $(element).inputmask("unmaskedvalue").length : true)
  }, '');

  $contactForm.validate({
    ignore: [],
    errorLabelContainer: $errorContainer,
    submitHandler: function () {
      if ($hiddenField.val() === '') {
        var data = new FormData();
        $contactForm.find('.js-input').filter('input, select, textarea').each(function () {
          if($(this).is('input[type="radio"]') && !$(this).prop('checked')) return;
          var value = $(this).val();
          data.append(this.name, value);
        });

        $.ajax({
          url: "/sendfeedback",
          type: "POST",
          data: data,
          processData: false,
          contentType: false,
          success: function () {
            $contactForm.trigger('formSubmit');
            $contactForm.validate().resetForm()
          }
        });

      }

      return false;
    },
    invalidHandler: function (event, validator) {

      var $selectedOptions = $('.select2-selection__choice');
      if (!$selectedOptions.length) {
        $selectContainer.children('.contact-form__fill-line').addClass('select-error');
      }
      setTimeout(function () {
        $('.error').siblings('.contact-form__fill-line').addClass('error');
      }, 100)
    },
    onkeyup: function (element) {
      $(element).valid();
    },

  })

  $('.js-input-name').on('input', function () {
    var value = $(this).val();
    value = value.replace(/[^a-zA-Zа-яА-Я\s\-]/ig, '');
    $(this).val(value);
  });

  $('.js-open-map').on('click', function () {
    $('.js-map').addClass('is-active');
  })

  $('.js-close-map').on('click', function () {
    $('.js-map').removeClass('is-active');
  })

  $('.js-open-contact-form').on('click', function () {
    $('.js-form-holder').addClass('is-active');
    $('html, body').css({ 'overflow': 'hidden' });
  })

  $('.js-close-contact-form').on('click', function () {
    $('.js-form-holder').removeClass('is-active');
    $('html, body').css('overflow', '');
    $contactForm.trigger('formReset')
  })


})

$(document).ready(function(){
  $('.contact-form__input--textarea_scroll').scrollbar({
    "duration": "1000",
    "autoUpdate": true
  });
});


$( document ).ready(function() {
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  if (isIE) {


    var borderBottom = "<span class='link--privacy_border-b'></span>";
    var linkB = $('.link--privacy');

    $('.link--privacy').append(borderBottom);
    $('.js-contact-form .contact-form__terms ').css('display', 'inline-block')
    $('.js-contact-form .link--privacy').css('display', 'inline-block');
  }
$('.js-contact-info-radio-text').click(function () {
  $('.js-contact-info-radio-text').removeClass('contact-info__text--red')
  $(this).addClass('contact-info__text--red')
})
});