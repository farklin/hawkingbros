$(function() {

  var $careerForm = $('.js-career-form'), 
      $errorContainer = $('.form-error-container'),
      $fileInput = $('.js-file-input'),
      $fileError = $('.js-file-error'),
      $hiddenField = $('.js-input-person');
      
	
  $('form').on('formSend', function() {
	  
	  
  })

  $careerForm.validate({
    errorLabelContainer: $errorContainer,
    submitHandler: function() {
      
      if ($hiddenField.val() === '') {
		$careerForm.trigger('formSend');
        //$careerForm.trigger('formSubmit');
      } else return;
     
    },
    invalidHandler: function(event, validator) {

      setTimeout(function(){
        $('.error').siblings('.contact-form__fill-line').addClass('error');
      },100)
      
    },
    onkeyup: function(element) {
      $(element).valid(); 
    },

  }) 

  $fileInput.on('change', function(e) {
    var $fileLabel = $('.js-file-name'),
        labelVal = $fileLabel.html(),
        fileName = '';
        error = false;
    $(this).removeClass('error');

    $('.js-file-error').css('display', 'none');
    var format = ['doc', 'docx' ,'pdf','jpg','png','xls','xlsx','txt'];
    var parts = this.value.split('.');
    parts = parts.pop();

    for(var i = 0; i < format.length; i++){
      if(parts ==  format[i]){
        error = false;
        break;
      }
      else{
        error = true;
      }
    }
    if(error == true){
      this.value = "";
      $fileLabel.html('Прикрепить резюме');
      $('.js-file-error').css('display', 'block');
    }
    else{
    if(this.files && this.files.length > 1)
			fileName = (this.getAttribute( 'data-multiple-caption' ) || '').replace('{count}', this.files.length);
		else if( e.target.value )
			fileName = e.target.value.split('\\').pop();
		if(fileName)
			$fileLabel.html(fileName);
		else
			$fileLabel.html(labelVal);
    }
  })

  $('.js-career-contact').on('click', function() {
    $('.js-form-holder').addClass('is-active');
      $('html, body').css({'overflow': 'hidden'});
  })

  // DO NOT DELETE!

  // Stick block with HH link
  $('.js-contacts-sticky').each(function() {
    // $('.js-form-holder').addClass('is-active');
    $(this).stick_in_parent();
  });

  $(window).on('resize', function() {
    $(document.body).trigger("sticky_kit:recalc");
  })
  
})

