$(document).ready(function () {
    let button = $('.js-show-article');

    function handleBtn(page, pagesCount) {
        
        if (page >= pagesCount) {
            button.parent().hide();
        } else if (button.parent().is(':hidden')) {
            button.parent().show();
        }
    }
    const articles = $('body').find('.js-articles');


    $('body').on('click', '.js-show-article', function () {

        if (!articles.hasClass("loading")) {

            let page = articles.attr('data-page') ? parseInt(articles.attr('data-page')) : 1;
            let countPages = articles.attr('data-pages') ? parseInt(articles.attr('data-pages')) : 1;
            let url = articles.data('action');
            let data = {
                page: page + 1
            };

            let categoryId = getCategoryID();

            if (categoryId) {
                data.category = categoryId;
            }

            if (page < countPages) {

                articles.addClass("loading");
                $.ajax({
                    url: url,
                    data: data,
                    type: "GET"
                }).done(function (html) {
                    if ($(html).find('.js-articles')) {
                        articles.append($(html).html());
                        articles.attr('data-page', $(html).data('page'));
                        handleBtn($(html).data('page'), $(html).data('pages'));
                    }
                }).always(function () {
                    articles.removeClass('loading');
                });
            }
        }
    });

    document.addEventListener('click', (e) => {
        let radioBtn = e.target.closest('.articles .js-filter-item__input');
        let removeFilterRadioBtn = document.querySelectorAll('.articles .js-filter-item__input');
        if (!!radioBtn) {
            removeFilterRadioBtn.forEach(el => {
                if (el.classList.contains('checked') && el !== radioBtn) {
                    el.classList.remove('checked');
                }
            })
            if (radioBtn.checked && radioBtn.classList.contains('checked')) {
                radioBtn.classList.remove('checked');
                radioBtn.checked = false
            } else {
                radioBtn.classList.add('checked');
            }

            const articles = $('body').find('.js-articles');
            if (articles) {
                let url = $('.articles .js-filter').data('action');
                $.ajax({
                    url: url,
                    data: {
                        page: 1,
                        category: getCategoryID()
                    },
                    type: "GET"
                }).done(function (html) {
                    if ($(html).find('.js-articles')) {
                        articles.html($(html).html());
                        articles.attr('data-page', 1);
                        handleBtn(1, $(html).data('pages'));
                    }
                })


            }
        }


    })

    function getCategoryID() {
        return $('.js-filter-item__input.checked').val();
    }

});