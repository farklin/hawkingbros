$(function() {
  
  $(".js-school-swiper").each(function (index, elem) {

    var schoolSwiperOption = {
      speed: 400,
      spaceBetween: 10,
      slidesPerView: 1,
      scrollbar: {
        el: $(elem).parent().next(".js-school-scrollbar"),
      },
    }
  
    var schoolSwiperActive = false;
    var schoolSwiper;
  
    if ($(window).innerWidth() < 768) {
      schoolSwiper = new Swiper(elem, schoolSwiperOption);
      schoolSwiperActive = true;
    }
  
    $(window).on('resize', function() {
      
      if ($(this).innerWidth() >767 && schoolSwiperActive) {
        schoolSwiper.destroy(1, 1);
        schoolSwiperActive = false;
      } else if($(this).innerWidth() < 768 && schoolSwiperActive === false) {
        schoolSwiper = new Swiper(elem, schoolSwiperOption);
        schoolSwiperActive = true;
      } 
  
    })

  })

  


})

