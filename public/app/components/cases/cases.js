$(function() {
  
  if ($(window).innerWidth() < 768) {
    $(document).on('touchstart', '.js-case-card', function() {
      $(this).addClass('touch-down');
    })
    $(document).on('touchend', function() {
      $('.js-case-card').removeClass('touch-down');
    })

  }

//   popup

  var   $projectPopup = $('.js-project-popup'),
        $closePopup = $('.js-close-popup'),
        $body = $('body'),
        $menuTools = $('.js-menu-tools'),
        $header = $('.js-header'),
        $mainContent = $('.js-main-content')

  var ps

  function openPopup(id) {
    var scrollbarWidth = getScrollbarWidth(),
        $projectPopup = $('[data-popup='+id),
        $projectPopupInner = $projectPopup.find('.popup__inner')

    $projectPopup.addClass('is-active')
    $body.css({'overflow' : 'hidden', 'padding-right' : scrollBarWidth})
    $menuTools.css({'padding-right' : scrollBarWidth})
    $header.css({'padding-right' : scrollBarWidth})
    $mainContent.addClass('bg--blur');
    $header.addClass('is-blur');
    $menuTools.addClass('is-blur');
    var $popupContent = $projectPopup.find('.project-details')
    ps = new PerfectScrollbar($projectPopupInner[0], {
      wheelSpeed: 1,
        minScrollbarLength: 10,
        maxScrollbarLength: 70,
        suppressScrollX: true
    })
  }

  function closePopup() {
    $projectPopup.removeClass('is-active')
    $body.css({'overflow' : 'auto', 'padding-right' : '0'})
    $menuTools.css({'padding-right' : '0'})
    $header.css({'padding-right' : '0'})
    $mainContent.removeClass('bg--blur');
    $header.removeClass('is-blur');
    $menuTools.removeClass('is-blur');
    ps.destroy()
    ps = null
  }

  $(document).on('click', '.js-open-project', function(e) {
    var href = $(this).attr('href')
    e.preventDefault()
    e.stopPropagation()
    openPopup(href)
  })

  $projectPopup.find('.popup__inner').on('click', function() {    
    closePopup()
  })

  $closePopup.on('click', function() {
    closePopup()
  })

  $projectPopup.find('.popup__window').on('click', function(e) {
    e.stopPropagation()
  })


})