document.addEventListener("DOMContentLoaded", function() {
    const corpPortalsCanvas = document.querySelector('.corp-portals-canvas');

    const ctx = corpPortalsCanvas.getContext('2d');
    const ctxWidth = ctx.canvas.width;
    const ctxHeight = ctx.canvas.height;

    const opts = {
        countParticles: 1000,

        defaultSpeed: 0.5,
        addedSpeed: 0,

        defaultRadius: 1,
        addedRadius: 0.5
    }

    const particles = [];

    class Particle {
        constructor() {
            this.position = {
                x: Math.random() * ctxWidth,
                y: Math.random() * ctxHeight
            };

            this.radius = opts.defaultRadius + Math.random() * opts.addedRadius;
            this.speed = opts.defaultSpeed + Math.random() * opts.addedSpeed;
            this.directionAngle = Math.floor(Math.random() * 360);
            this.direction = {
                x: Math.cos(this.directionAngle) * this.speed,
                y: Math.sin(this.directionAngle) * this.speed
            };
            this.opacity = Math.random();
            this.inSquare = this.isPointInTriangle(this.position.x, this.position.y);
        }

        draw() {
            ctx.beginPath();
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.fillStyle = `rgba(235, 87, 87, ${this.opacity})`;
            ctx.fill();
            ctx.closePath();
        }

        update() {
            this.checkBorder();

            this.position.x += this.direction.x;
            this.position.y += this.direction.y;
        }

        squareTriangle(aAx, aAy, aBx, aBy, aCx, aCy) {
            return Math.abs(aBx * aCy - aCx * aBy - aAx * aCy + aCx * aAy + aAx * aBy - aBx * aAy);
        }

        isPointInTriangle(x, y) {
            const sumSquarePart = this.squareTriangle(x, y, 360, 0, 520, 520) + this.squareTriangle(x, y, 520, 520, 0, 375) + this.squareTriangle(x, y, 0, 375, 360, 0);
            const sumSquareFull = this.squareTriangle(360, 0, 520, 520, 0, 375);

            return Math.abs(sumSquareFull - sumSquarePart) > 0.01
        }

        isPointInCircle(x, y, cx, cy, r) {
            return (x - cx) * (x - cx) + (y - cy) * (y - cy) >= r * r;
        }

        checkBorder() {
            if (this.isPointInTriangle(this.position.x, this.position.y)) {
                this.direction.x *= -1;
                this.direction.y *= -1;
            }

            // if (this.isPointInCircle(this.position.x, this.position.y, ctxWidth / 2, ctxHeight / 2, ctxWidth / 2)) {
            //     this.direction.x *= -1;
            //     this.direction.y *= -1;
            // }
        }
    }

    class Triangle {
        constructor(x11, y11, x12, y12, x13, y13) {
            this.x11 = x11;
            this.y11 = y11;
            this.x12 = x12;
            this.y12 = y12;
            this.x13 = x13;
            this.y13 = y13;
        }

        draw(isMask) {
            ctx.beginPath();
            ctx.moveTo(this.x11, this.y11);
            ctx.lineTo(this.x12, this.y12);
            ctx.lineTo(this.x13, this.y13);
            ctx.closePath();

            isMask ? ctx.clip() : ctx.stroke();
        }
    }

    class Sphere {
        constructor(x, y, radius) {
            this.position = {
                x: x ? x : ctxWidth / 2,
                y: y ? y : ctxHeight / 2
            },
            this.radius = radius ? radius : ctxWidth / 2
        }

        draw(isMask) {
            ctx.beginPath();
            ctx.arc(this.position.x, this.position.y, this.radius, 0, 2 * Math.PI);
            ctx.closePath();

            isMask ? ctx.clip() : ctx.stroke();
        }
    }

    function setup() {
        for (let i = 0; i < opts.countParticles; i++) {
            const particle = new Particle();

            /* TODO отрисовка внутри контура */
            if (!particle.inSquare) {
                particles.push(particle);
            }
        }

        new Triangle(360, 0, 520, 520, 0, 375).draw(true); // отрисовка контура, но без лагов
        // new Sphere().draw(true);

        window.requestAnimationFrame(loop);
    }

    function loop() {
        ctx.clearRect(0, 0, ctxWidth, ctxHeight);
        // const triangle = new Triangle(360, 0, 520, 520, 0, 375).draw(); //для отрисовки контура (лагает)
        // new Sphere().draw();

        window.requestAnimationFrame(loop);

        for (let i = 0; i < particles.length; i++) {
            particles[i].update();
            particles[i].draw();
        }
    }

    setup();
});



//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb3JwLXBvcnRhbHMtY2FudmFzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoXCJET01Db250ZW50TG9hZGVkXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgY29uc3QgY29ycFBvcnRhbHNDYW52YXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY29ycC1wb3J0YWxzLWNhbnZhcycpO1xyXG5cclxuICAgIGNvbnN0IGN0eCA9IGNvcnBQb3J0YWxzQ2FudmFzLmdldENvbnRleHQoJzJkJyk7XHJcbiAgICBjb25zdCBjdHhXaWR0aCA9IGN0eC5jYW52YXMud2lkdGg7XHJcbiAgICBjb25zdCBjdHhIZWlnaHQgPSBjdHguY2FudmFzLmhlaWdodDtcclxuXHJcbiAgICBjb25zdCBvcHRzID0ge1xyXG4gICAgICAgIGNvdW50UGFydGljbGVzOiAxMDAwLFxyXG5cclxuICAgICAgICBkZWZhdWx0U3BlZWQ6IDAuNSxcclxuICAgICAgICBhZGRlZFNwZWVkOiAwLFxyXG5cclxuICAgICAgICBkZWZhdWx0UmFkaXVzOiAxLFxyXG4gICAgICAgIGFkZGVkUmFkaXVzOiAwLjVcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBwYXJ0aWNsZXMgPSBbXTtcclxuXHJcbiAgICBjbGFzcyBQYXJ0aWNsZSB7XHJcbiAgICAgICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgICAgIHRoaXMucG9zaXRpb24gPSB7XHJcbiAgICAgICAgICAgICAgICB4OiBNYXRoLnJhbmRvbSgpICogY3R4V2lkdGgsXHJcbiAgICAgICAgICAgICAgICB5OiBNYXRoLnJhbmRvbSgpICogY3R4SGVpZ2h0XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB0aGlzLnJhZGl1cyA9IG9wdHMuZGVmYXVsdFJhZGl1cyArIE1hdGgucmFuZG9tKCkgKiBvcHRzLmFkZGVkUmFkaXVzO1xyXG4gICAgICAgICAgICB0aGlzLnNwZWVkID0gb3B0cy5kZWZhdWx0U3BlZWQgKyBNYXRoLnJhbmRvbSgpICogb3B0cy5hZGRlZFNwZWVkO1xyXG4gICAgICAgICAgICB0aGlzLmRpcmVjdGlvbkFuZ2xlID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogMzYwKTtcclxuICAgICAgICAgICAgdGhpcy5kaXJlY3Rpb24gPSB7XHJcbiAgICAgICAgICAgICAgICB4OiBNYXRoLmNvcyh0aGlzLmRpcmVjdGlvbkFuZ2xlKSAqIHRoaXMuc3BlZWQsXHJcbiAgICAgICAgICAgICAgICB5OiBNYXRoLnNpbih0aGlzLmRpcmVjdGlvbkFuZ2xlKSAqIHRoaXMuc3BlZWRcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5vcGFjaXR5ID0gTWF0aC5yYW5kb20oKTtcclxuICAgICAgICAgICAgdGhpcy5pblNxdWFyZSA9IHRoaXMuaXNQb2ludEluVHJpYW5nbGUodGhpcy5wb3NpdGlvbi54LCB0aGlzLnBvc2l0aW9uLnkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZHJhdygpIHtcclxuICAgICAgICAgICAgY3R4LmJlZ2luUGF0aCgpO1xyXG4gICAgICAgICAgICBjdHguYXJjKHRoaXMucG9zaXRpb24ueCwgdGhpcy5wb3NpdGlvbi55LCB0aGlzLnJhZGl1cywgMCwgMiAqIE1hdGguUEkpO1xyXG4gICAgICAgICAgICBjdHguZmlsbFN0eWxlID0gYHJnYmEoMjM1LCA4NywgODcsICR7dGhpcy5vcGFjaXR5fSlgO1xyXG4gICAgICAgICAgICBjdHguZmlsbCgpO1xyXG4gICAgICAgICAgICBjdHguY2xvc2VQYXRoKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB1cGRhdGUoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2hlY2tCb3JkZXIoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucG9zaXRpb24ueCArPSB0aGlzLmRpcmVjdGlvbi54O1xyXG4gICAgICAgICAgICB0aGlzLnBvc2l0aW9uLnkgKz0gdGhpcy5kaXJlY3Rpb24ueTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNxdWFyZVRyaWFuZ2xlKGFBeCwgYUF5LCBhQngsIGFCeSwgYUN4LCBhQ3kpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE1hdGguYWJzKGFCeCAqIGFDeSAtIGFDeCAqIGFCeSAtIGFBeCAqIGFDeSArIGFDeCAqIGFBeSArIGFBeCAqIGFCeSAtIGFCeCAqIGFBeSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpc1BvaW50SW5UcmlhbmdsZSh4LCB5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN1bVNxdWFyZVBhcnQgPSB0aGlzLnNxdWFyZVRyaWFuZ2xlKHgsIHksIDM2MCwgMCwgNTIwLCA1MjApICsgdGhpcy5zcXVhcmVUcmlhbmdsZSh4LCB5LCA1MjAsIDUyMCwgMCwgMzc1KSArIHRoaXMuc3F1YXJlVHJpYW5nbGUoeCwgeSwgMCwgMzc1LCAzNjAsIDApO1xyXG4gICAgICAgICAgICBjb25zdCBzdW1TcXVhcmVGdWxsID0gdGhpcy5zcXVhcmVUcmlhbmdsZSgzNjAsIDAsIDUyMCwgNTIwLCAwLCAzNzUpO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIE1hdGguYWJzKHN1bVNxdWFyZUZ1bGwgLSBzdW1TcXVhcmVQYXJ0KSA+IDAuMDFcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlzUG9pbnRJbkNpcmNsZSh4LCB5LCBjeCwgY3ksIHIpIHtcclxuICAgICAgICAgICAgcmV0dXJuICh4IC0gY3gpICogKHggLSBjeCkgKyAoeSAtIGN5KSAqICh5IC0gY3kpID49IHIgKiByO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2hlY2tCb3JkZXIoKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzUG9pbnRJblRyaWFuZ2xlKHRoaXMucG9zaXRpb24ueCwgdGhpcy5wb3NpdGlvbi55KSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kaXJlY3Rpb24ueCAqPSAtMTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGlyZWN0aW9uLnkgKj0gLTE7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIGlmICh0aGlzLmlzUG9pbnRJbkNpcmNsZSh0aGlzLnBvc2l0aW9uLngsIHRoaXMucG9zaXRpb24ueSwgY3R4V2lkdGggLyAyLCBjdHhIZWlnaHQgLyAyLCBjdHhXaWR0aCAvIDIpKSB7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLmRpcmVjdGlvbi54ICo9IC0xO1xyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5kaXJlY3Rpb24ueSAqPSAtMTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjbGFzcyBUcmlhbmdsZSB7XHJcbiAgICAgICAgY29uc3RydWN0b3IoeDExLCB5MTEsIHgxMiwgeTEyLCB4MTMsIHkxMykge1xyXG4gICAgICAgICAgICB0aGlzLngxMSA9IHgxMTtcclxuICAgICAgICAgICAgdGhpcy55MTEgPSB5MTE7XHJcbiAgICAgICAgICAgIHRoaXMueDEyID0geDEyO1xyXG4gICAgICAgICAgICB0aGlzLnkxMiA9IHkxMjtcclxuICAgICAgICAgICAgdGhpcy54MTMgPSB4MTM7XHJcbiAgICAgICAgICAgIHRoaXMueTEzID0geTEzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZHJhdyhpc01hc2spIHtcclxuICAgICAgICAgICAgY3R4LmJlZ2luUGF0aCgpO1xyXG4gICAgICAgICAgICBjdHgubW92ZVRvKHRoaXMueDExLCB0aGlzLnkxMSk7XHJcbiAgICAgICAgICAgIGN0eC5saW5lVG8odGhpcy54MTIsIHRoaXMueTEyKTtcclxuICAgICAgICAgICAgY3R4LmxpbmVUbyh0aGlzLngxMywgdGhpcy55MTMpO1xyXG4gICAgICAgICAgICBjdHguY2xvc2VQYXRoKCk7XHJcblxyXG4gICAgICAgICAgICBpc01hc2sgPyBjdHguY2xpcCgpIDogY3R4LnN0cm9rZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjbGFzcyBTcGhlcmUge1xyXG4gICAgICAgIGNvbnN0cnVjdG9yKHgsIHksIHJhZGl1cykge1xyXG4gICAgICAgICAgICB0aGlzLnBvc2l0aW9uID0ge1xyXG4gICAgICAgICAgICAgICAgeDogeCA/IHggOiBjdHhXaWR0aCAvIDIsXHJcbiAgICAgICAgICAgICAgICB5OiB5ID8geSA6IGN0eEhlaWdodCAvIDJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGhpcy5yYWRpdXMgPSByYWRpdXMgPyByYWRpdXMgOiBjdHhXaWR0aCAvIDJcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGRyYXcoaXNNYXNrKSB7XHJcbiAgICAgICAgICAgIGN0eC5iZWdpblBhdGgoKTtcclxuICAgICAgICAgICAgY3R4LmFyYyh0aGlzLnBvc2l0aW9uLngsIHRoaXMucG9zaXRpb24ueSwgdGhpcy5yYWRpdXMsIDAsIDIgKiBNYXRoLlBJKTtcclxuICAgICAgICAgICAgY3R4LmNsb3NlUGF0aCgpO1xyXG5cclxuICAgICAgICAgICAgaXNNYXNrID8gY3R4LmNsaXAoKSA6IGN0eC5zdHJva2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gc2V0dXAoKSB7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBvcHRzLmNvdW50UGFydGljbGVzOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3QgcGFydGljbGUgPSBuZXcgUGFydGljbGUoKTtcclxuXHJcbiAgICAgICAgICAgIC8qIFRPRE8g0L7RgtGA0LjRgdC+0LLQutCwINCy0L3Rg9GC0YDQuCDQutC+0L3RgtGD0YDQsCAqL1xyXG4gICAgICAgICAgICBpZiAoIXBhcnRpY2xlLmluU3F1YXJlKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJ0aWNsZXMucHVzaChwYXJ0aWNsZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG5ldyBUcmlhbmdsZSgzNjAsIDAsIDUyMCwgNTIwLCAwLCAzNzUpLmRyYXcodHJ1ZSk7IC8vINC+0YLRgNC40YHQvtCy0LrQsCDQutC+0L3RgtGD0YDQsCwg0L3QviDQsdC10Lcg0LvQsNCz0L7QslxyXG4gICAgICAgIC8vIG5ldyBTcGhlcmUoKS5kcmF3KHRydWUpO1xyXG5cclxuICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKGxvb3ApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIGxvb3AoKSB7XHJcbiAgICAgICAgY3R4LmNsZWFyUmVjdCgwLCAwLCBjdHhXaWR0aCwgY3R4SGVpZ2h0KTtcclxuICAgICAgICAvLyBjb25zdCB0cmlhbmdsZSA9IG5ldyBUcmlhbmdsZSgzNjAsIDAsIDUyMCwgNTIwLCAwLCAzNzUpLmRyYXcoKTsgLy/QtNC70Y8g0L7RgtGA0LjRgdC+0LLQutC4INC60L7QvdGC0YPRgNCwICjQu9Cw0LPQsNC10YIpXHJcbiAgICAgICAgLy8gbmV3IFNwaGVyZSgpLmRyYXcoKTtcclxuXHJcbiAgICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShsb29wKTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJ0aWNsZXMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgcGFydGljbGVzW2ldLnVwZGF0ZSgpO1xyXG4gICAgICAgICAgICBwYXJ0aWNsZXNbaV0uZHJhdygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXR1cCgpO1xyXG59KTtcclxuXHJcblxyXG4iXSwiZmlsZSI6ImNvcnAtcG9ydGFscy1jYW52YXMuanMifQ==
