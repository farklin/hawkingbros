window.onload = function () {
    closePreloadre();
};

function closePreloadre() {
    setTimeout(function () {
        var $preloader = $('.js-preloader');
        $preloader.removeClass('is-active');
        $('body').removeClass('loading');
    }, 1000)
}

const preloaderPageTitle = document.querySelector('.preloader__page-title')


$(function () {

    // IE detection

    if (!!window.MSInputMethodContext && !!document.documentMode) {
        $('body').addClass('browser-ie');
    }


    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/false || !!document.documentMode;

    var idEdge = !isIE && !!window.StyleMedia

    if (!isIE && !!window.StyleMedia) {
        $('body').addClass('browser-edge');
    }
    // Android detection

    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1;

    if (isAndroid) {
        $('body').addClass('is-android');
    }

    //Touch detection
    if (!("ontouchstart" in document.documentElement)) {
        $('body').addClass('no-touch');
    }

    // Header scroll

    var prevScroll = window.pageYOffset;
    var $header = $('.header');

    if (prevScroll > 0) {
        $header.addClass('is-fixed');
        addShadow();
        $(this).trigger('scroll');
    }
    if (window.location.hash.match(/#\w/) && prevScroll > 0) {
        $header.addClass('is-hide')
    }
    function headerScroll(event, isAnchor) {
        var currentScroll = window.pageYOffset;
        var delta = currentScroll - prevScroll;
        if (delta < 0) {
            var bdr = window.innerWidth >= 768 ? 0 : 4
            if (currentScroll <= bdr) {
                $header.css('transition', 'none').removeClass('is-fixed is-hide is-show');
                removeShadow();
                setTimeout(function () {
                    $header.css('transition', '');
                }, 0);
            }
            if ($header.is('.is-fixed')) {
                $header.removeClass('is-hide').addClass('is-show');
                addShadow();
            }
        } else if (delta > 0 || isAnchor) {
            if ($header.is('.is-fixed')) {
                $header.removeClass('is-show').addClass('is-hide');
            } else {
                if (currentScroll > $header.outerHeight()) {
                    $header.css('transition', 'none').addClass('is-fixed is-hide');
                    setTimeout(function () {
                        $header.css('transition', '');
                    }, 0);
                }
            }
        }

        prevScroll = currentScroll;
    }

    $(window).on('scroll', headerScroll);

    function addShadow() {
        $('.js-header').addClass('header--shadow')
    }

    function removeShadow() {
        $('.js-header').removeClass('header--shadow')
    }

    var carousel = $('.triangle__carousel')[0];
    var angle = 0;
    var last = 1;
    var intervalAnimation = 5 * 1000;
    var triangleText = $('.triangle__text');
    var startAnimateElement = 0;

    function rotateCarousel(now) {
        if (!last || now - last >= intervalAnimation) {
            last = now;

            angle += -120;
            carousel.style.transform = 'translateZ(-40px) rotateX(' + angle + 'deg)';
        }
        requestAnimationFrame(rotateCarousel);
    }

    function rotateCarouselIE() {
        setInterval(function () {
            $(triangleText[startAnimateElement]).animate({"opacity": "0"}, 1000);
            $(triangleText[startAnimateElement + 1]).animate({"opacity": "1"}, 1000);

            if (startAnimateElement == 2) {
                startAnimateElement = 0;
                $(triangleText[startAnimateElement]).animate({"opacity": "1"}, 1000);
            } else {
                startAnimateElement++;
            }
        }, intervalAnimation)
    }

    !!carousel && !isIE && window.requestAnimationFrame(rotateCarousel);
    !!carousel && isIE && rotateCarouselIE();
})

// lazy image load

window.addEventListener('DOMContentLoaded', function () {
    $('.js-lazy').lazy({
        threshold: 1200
    });
})

$(function() {
    $('.js-anchor').on('click', function(event) {
        event.preventDefault();

        var link = $(this).attr('href');
        var $targetElement = $(link);

        if ($targetElement.length) {
            var scrolled = window.pageYOffset;
            var offsetTop = $(link).offset().top;

            if(scrolled > offsetTop) {
                offsetTop -= $('.header').innerHeight();
            }

            $('html, body').animate({scrollTop:offsetTop},600)
        }
    });
});
$(function() {
  
  if ($(window).innerWidth() < 768) {
    $(document).on('touchstart', '.js-case-card', function() {
      $(this).addClass('touch-down');
    })
    $(document).on('touchend', function() {
      $('.js-case-card').removeClass('touch-down');
    })

  }

//   popup

  var   $projectPopup = $('.js-project-popup'),
        $closePopup = $('.js-close-popup'),
        $body = $('body'),
        $menuTools = $('.js-menu-tools'),
        $header = $('.js-header'),
        $mainContent = $('.js-main-content')

  var ps

  function openPopup(id) {
    var scrollbarWidth = getScrollbarWidth(),
        $projectPopup = $('[data-popup='+id),
        $projectPopupInner = $projectPopup.find('.popup__inner')

    $projectPopup.addClass('is-active')
    $body.css({'overflow' : 'hidden', 'padding-right' : scrollBarWidth})
    $menuTools.css({'padding-right' : scrollBarWidth})
    $header.css({'padding-right' : scrollBarWidth})
    $mainContent.addClass('bg--blur');
    $header.addClass('is-blur');
    $menuTools.addClass('is-blur');
    var $popupContent = $projectPopup.find('.project-details')
    ps = new PerfectScrollbar($projectPopupInner[0], {
      wheelSpeed: 1,
        minScrollbarLength: 10,
        maxScrollbarLength: 70,
        suppressScrollX: true
    })
  }

  function closePopup() {
    $projectPopup.removeClass('is-active')
    $body.css({'overflow' : 'auto', 'padding-right' : '0'})
    $menuTools.css({'padding-right' : '0'})
    $header.css({'padding-right' : '0'})
    $mainContent.removeClass('bg--blur');
    $header.removeClass('is-blur');
    $menuTools.removeClass('is-blur');
    ps.destroy()
    ps = null
  }

  $(document).on('click', '.js-open-project', function(e) {
    var href = $(this).attr('href')
    e.preventDefault()
    e.stopPropagation()
    openPopup(href)
  })

  $projectPopup.find('.popup__inner').on('click', function() {    
    closePopup()
  })

  $closePopup.on('click', function() {
    closePopup()
  })

  $projectPopup.find('.popup__window').on('click', function(e) {
    e.stopPropagation()
  })


})
$(document).ready(function () {
    let button = $('.js-show-article');

    function handleBtn(page, pagesCount) {
        
        if (page >= pagesCount) {
            button.parent().hide();
        } else if (button.parent().is(':hidden')) {
            button.parent().show();
        }
    }
    const articles = $('body').find('.js-articles');


    $('body').on('click', '.js-show-article', function () {

        if (!articles.hasClass("loading")) {

            let page = articles.attr('data-page') ? parseInt(articles.attr('data-page')) : 1;
            let countPages = articles.attr('data-pages') ? parseInt(articles.attr('data-pages')) : 1;
            let url = articles.data('action');
            let data = {
                page: page + 1
            };

            let categoryId = getCategoryID();

            if (categoryId) {
                data.category = categoryId;
            }

            if (page < countPages) {

                articles.addClass("loading");
                $.ajax({
                    url: url,
                    data: data,
                    type: "GET"
                }).done(function (html) {
                    if ($(html).find('.js-articles')) {
                        articles.append($(html).html());
                        articles.attr('data-page', $(html).data('page'));
                        handleBtn($(html).data('page'), $(html).data('pages'));
                    }
                }).always(function () {
                    articles.removeClass('loading');
                });
            }
        }
    });

    document.addEventListener('click', (e) => {
        let radioBtn = e.target.closest('.articles .js-filter-item__input');
        let removeFilterRadioBtn = document.querySelectorAll('.articles .js-filter-item__input');
        if (!!radioBtn) {
            removeFilterRadioBtn.forEach(el => {
                if (el.classList.contains('checked') && el !== radioBtn) {
                    el.classList.remove('checked');
                }
            })
            if (radioBtn.checked && radioBtn.classList.contains('checked')) {
                radioBtn.classList.remove('checked');
                radioBtn.checked = false
            } else {
                radioBtn.classList.add('checked');
            }

            const articles = $('body').find('.js-articles');
            if (articles) {
                let url = $('.articles .js-filter').data('action');
                $.ajax({
                    url: url,
                    data: {
                        page: 1,
                        category: getCategoryID()
                    },
                    type: "GET"
                }).done(function (html) {
                    if ($(html).find('.js-articles')) {
                        articles.html($(html).html());
                        articles.attr('data-page', 1);
                        handleBtn(1, $(html).data('pages'));
                    }
                })


            }
        }


    })

    function getCategoryID() {
        return $('.js-filter-item__input.checked').val();
    }

});
var initSwiper = function(swiperSelector) {
  var partnersSwiperOption = {
    updateOnWindowResize: true,
    speed: 400,
    slidesPerView: 'auto',
    slidesPerColumnFill: 'row',
    scrollbar: {
      el: '.swiper-scrollbar',
    },
    allowTouchMove: false,
  }
  if (window.innerWidth < 768) {
    partnersSwiperOption.loopedSlides = 3;
    partnersSwiperOption.allowTouchMove = true;
  }

  return new Swiper('.js-partners-swiper', partnersSwiperOption);
}

$(function() {
  var swiperSelector = '.js-partners-swiper';
  var innerSwiperSelector = '.partners__inner.swiper-wrapper';
  var partnersSwiper = initSwiper(swiperSelector);
  
  var swiperWidth;
  
  if (!$(swiperSelector).length) return;

  function getSwiperWidth() {
    if(swiperWidth) {
      return swiperWidth;
    } else {
      swiperWidth = 0;
      for(key = 0; key < partnersSwiper.slidesSizesGrid.length; key++) {
        swiperWidth += partnersSwiper.slidesSizesGrid[key];
      }
      //document.querySelector(innerSwiperSelector).style.transform = "translateX(" + (-parseInt((swiperWidth - window.innerWidth) / 2)) + "px)";
    }

    return swiperWidth;
  }

  function swiperMouseTrigger(event) {
    var windowWidth = window.innerWidth - 20;
    var centerX = parseInt(window.innerWidth/2);
  
    var swiperWrapper = document.querySelector(innerSwiperSelector);
    var swiperWidth = getSwiperWidth();
  
    var mouseX = event.clientX;
    var diffX = mouseX-centerX;
  
    var marginsOuterOfWindow = swiperWidth-windowWidth;
    var marginLeft = marginsOuterOfWindow/2 + diffX/windowWidth*marginsOuterOfWindow;
    swiperWrapper.style.transform = "translateX(" + (-parseInt(marginLeft)) + "px)";
  }
  
  $(window).on('resize', function() {
    if (window.innerWidth > 767) {
      partnersSwiper.params.loopedSlides = 7;
      partnersSwiper.allowTouchMove = false;

      partnersSwiper.update();
      swiperWidth = false;

      document.addEventListener('mousemove', swiperMouseTrigger);
    } else {
      document.removeEventListener('mousemove', swiperMouseTrigger);
      partnersSwiper.params.loopedSlides = 3;
      partnersSwiper.allowTouchMove = true;

      partnersSwiper.update();
      swiperWidth = false;

      document.querySelector(innerSwiperSelector).style.transform = "translateX(-510px)";
    }
  })

  if(window.innerWidth > 767) {
    document.addEventListener('mousemove', swiperMouseTrigger);
  } else {
    document.querySelector(innerSwiperSelector).style.transform = "translateX(-510px)";
  }

  getSwiperWidth();
})
$(function () {

  // select2 init

  $('.js-contact-select').select2({
    dropdownPosition: 'below',
    dropdownCssClass: 'no-search',
    placeholder: {
      id: '',
      text: ''
    },
    dropdownParent: $('.js-contact-select-row'),
    closeOnSelect: false,
    multiple: true
  });

  $('.js-contact-select').on('select2:open', function (e) {
    var evt = "scroll.select2";
    $(e.target).parents().off(evt);
    $(window).off(evt);
  });


  // Contact form labels effects

  $('.js-input').on('focus', function () {
    $(this).siblings('.js-form-label').addClass('is-up');
  })
  if($('.contact-form__input--textarea').value){
    $(this).closest('.contact-form__row--message').children('.js-form-label').addClass('is-up');
  }
  $('.contact-form__input--textarea').on('focus', function(){
    $(this).closest('.contact-form__row--message').children('.js-form-label').addClass('is-up');
    $(this).closest('.contact-form__row--message').children('.contact-form__fill-line').css('width', '100%');
  })
  $('.contact-form__input--textarea').on('focusout', function(){
    $(this).closest('.contact-form__row--message').children('.contact-form__fill-line').css('width', '0');
  })

  $('.js-input').on('blur', function () {
    if (!$(this).val().length) {
      $(this).siblings('.js-form-label').removeClass('is-up');
      if($(this).hasClass('contact-form__input--textarea')){
        $(this).closest('.contact-form__row--message').children('.js-form-label').removeClass('is-up');
        
      }
    }
  })

  $('.js-contact-select').on('select2:opening select2:change', function () {
    $(this).siblings('.js-form-label').addClass('is-up');
    $selectContainer.find('.contact-form__fill-line').removeClass('select-error');
  })

  $('.js-contact-select').on('select2:close', function (e) {
    var $selectedOptions = $('.select2-selection__choice');

    if (!$selectedOptions.length) {
      $(this).siblings('.js-form-label').removeClass('is-up');

    }

  })

  var ps; // Perfect Scrollbar container

  $('.js-contact-select').on('select2:open', function (e) {

    var $selectResulsts = $('.select2-results__options');

    setTimeout(function () {
      ps = new PerfectScrollbar($selectResulsts[0], {
        wheelSpeed: 1,
        minScrollbarLength: 10,
        maxScrollbarLength: 70,
        suppressScrollX: true
      });
    }, 10)

  })

  $('.js-contact-select').on('select2:closing', function (e) {
    ps.destroy();
    ps = null;
  })

  // Contact form validation

  $('html[lang="ru"] .js-phone').inputmask({
    'mask': '+7 (999) 999-99-99',
    showMaskOnHover: false,
    onBeforePaste: function (pastedValue, opts) {
      var newValue = pastedValue.replace(/[^0-9]/ig, '');
      newValue = newValue.substring(newValue.length - 10);
      return newValue;
    },
  });

  $('html[lang="en"] .js-phone').on('input', function() {
    var value = $(this).val();
    value = value.replace(/[^0-9\+\-\(\)\s)]/ig, '');
    $(this).val(value);
  });

  var $contactForm = $('.js-contact-form'),
    $errorContainer = $contactForm.find('.form-error-container'),
    $selectContainer = $('.js-contact-select-row'),
    $hiddenField = $contactForm.find('.js-input-person');

  $('form').on('formReset', function () {
    $(this)[0].reset();
    $(this).find('.contact-form__fill-line, input').removeClass('error select-error');
    $(this).find('.js-file-name').html('Прикрепить резюме');
    $(this).find('.js-contact-select').val('');
    $(this).find('.select2-selection__choice').remove();
    $(this).find('.js-form-label').removeClass('is-up');
  })

  $('form').on('formSubmit', function () {
    var $formSucces = $(this).siblings('.js-form-succes');

    $(this).trigger('formReset');
    $formSucces.css({ 'display': 'block' });
    $('.contact-form__input').blur();
    $formSucces.stop().animate({
      opacity: 1,
    }, 200)
    setTimeout(function () {
      $formSucces.stop().animate({
        opacity: 0,
      }, 200)
      $formSucces.hide(200);
    }, 3000)
  })

  jQuery.validator.addMethod("customEmail", function (value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional(element) || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
  }, '');

  jQuery.validator.addMethod("customPhone", function (value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional(element) || (document.documentElement.lang !== 'en' ? 10 === $(element).inputmask("unmaskedvalue").length : true)
  }, '');

  $contactForm.validate({
    ignore: [],
    errorLabelContainer: $errorContainer,
    submitHandler: function () {
      if ($hiddenField.val() === '') {
        var data = new FormData();
        $contactForm.find('.js-input').filter('input, select, textarea').each(function () {
          if($(this).is('input[type="radio"]') && !$(this).prop('checked')) return;
          var value = $(this).val();
          data.append(this.name, value);
        });

        $.ajax({
          url: "/sendfeedback",
          type: "POST",
          data: data,
          processData: false,
          contentType: false,
          success: function () {
            $contactForm.trigger('formSubmit');
            $contactForm.validate().resetForm()
          }
        });

      }

      return false;
    },
    invalidHandler: function (event, validator) {

      var $selectedOptions = $('.select2-selection__choice');
      if (!$selectedOptions.length) {
        $selectContainer.children('.contact-form__fill-line').addClass('select-error');
      }
      setTimeout(function () {
        $('.error').siblings('.contact-form__fill-line').addClass('error');
      }, 100)
    },
    onkeyup: function (element) {
      $(element).valid();
    },

  })

  $('.js-input-name').on('input', function () {
    var value = $(this).val();
    value = value.replace(/[^a-zA-Zа-яА-Я\s\-]/ig, '');
    $(this).val(value);
  });

  $('.js-open-map').on('click', function () {
    $('.js-map').addClass('is-active');
  })

  $('.js-close-map').on('click', function () {
    $('.js-map').removeClass('is-active');
  })

  $('.js-open-contact-form').on('click', function () {
    $('.js-form-holder').addClass('is-active');
    $('html, body').css({ 'overflow': 'hidden' });
  })

  $('.js-close-contact-form').on('click', function () {
    $('.js-form-holder').removeClass('is-active');
    $('html, body').css('overflow', '');
    $contactForm.trigger('formReset')
  })


})

$(document).ready(function(){
  $('.contact-form__input--textarea_scroll').scrollbar({
    "duration": "1000",
    "autoUpdate": true
  });
});


$( document ).ready(function() {
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  if (isIE) {


    var borderBottom = "<span class='link--privacy_border-b'></span>";
    var linkB = $('.link--privacy');

    $('.link--privacy').append(borderBottom);
    $('.js-contact-form .contact-form__terms ').css('display', 'inline-block')
    $('.js-contact-form .link--privacy').css('display', 'inline-block');
  }
$('.js-contact-info-radio-text').click(function () {
  $('.js-contact-info-radio-text').removeClass('contact-info__text--red')
  $(this).addClass('contact-info__text--red')
})
});
$(function() {

  $('.js-up-button').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop: 0}, 700);
  });

})

$(function() {

  var $careerForm = $('.js-career-form'), 
      $errorContainer = $('.form-error-container'),
      $fileInput = $('.js-file-input'),
      $fileError = $('.js-file-error'),
      $hiddenField = $('.js-input-person');
      
	
  $('form').on('formSend', function() {
	  
	  
  })

  $careerForm.validate({
    errorLabelContainer: $errorContainer,
    submitHandler: function() {
      
      if ($hiddenField.val() === '') {
		$careerForm.trigger('formSend');
        //$careerForm.trigger('formSubmit');
      } else return;
     
    },
    invalidHandler: function(event, validator) {

      setTimeout(function(){
        $('.error').siblings('.contact-form__fill-line').addClass('error');
      },100)
      
    },
    onkeyup: function(element) {
      $(element).valid(); 
    },

  }) 

  $fileInput.on('change', function(e) {
    var $fileLabel = $('.js-file-name'),
        labelVal = $fileLabel.html(),
        fileName = '';
        error = false;
    $(this).removeClass('error');

    $('.js-file-error').css('display', 'none');
    var format = ['doc', 'docx' ,'pdf','jpg','png','xls','xlsx','txt'];
    var parts = this.value.split('.');
    parts = parts.pop();

    for(var i = 0; i < format.length; i++){
      if(parts ==  format[i]){
        error = false;
        break;
      }
      else{
        error = true;
      }
    }
    if(error == true){
      this.value = "";
      $fileLabel.html('Прикрепить резюме');
      $('.js-file-error').css('display', 'block');
    }
    else{
    if(this.files && this.files.length > 1)
			fileName = (this.getAttribute( 'data-multiple-caption' ) || '').replace('{count}', this.files.length);
		else if( e.target.value )
			fileName = e.target.value.split('\\').pop();
		if(fileName)
			$fileLabel.html(fileName);
		else
			$fileLabel.html(labelVal);
    }
  })

  $('.js-career-contact').on('click', function() {
    $('.js-form-holder').addClass('is-active');
      $('html, body').css({'overflow': 'hidden'});
  })

  // DO NOT DELETE!

  // Stick block with HH link
  $('.js-contacts-sticky').each(function() {
    // $('.js-form-holder').addClass('is-active');
    $(this).stick_in_parent();
  });

  $(window).on('resize', function() {
    $(document.body).trigger("sticky_kit:recalc");
  })
  
})


$(function() {
  
  $(".js-school-swiper").each(function (index, elem) {

    var schoolSwiperOption = {
      speed: 400,
      spaceBetween: 10,
      slidesPerView: 1,
      scrollbar: {
        el: $(elem).parent().next(".js-school-scrollbar"),
      },
    }
  
    var schoolSwiperActive = false;
    var schoolSwiper;
  
    if ($(window).innerWidth() < 768) {
      schoolSwiper = new Swiper(elem, schoolSwiperOption);
      schoolSwiperActive = true;
    }
  
    $(window).on('resize', function() {
      
      if ($(this).innerWidth() >767 && schoolSwiperActive) {
        schoolSwiper.destroy(1, 1);
        schoolSwiperActive = false;
      } else if($(this).innerWidth() < 768 && schoolSwiperActive === false) {
        schoolSwiper = new Swiper(elem, schoolSwiperOption);
        schoolSwiperActive = true;
      } 
  
    })

  })

  


})


$(function() {

  if ($('.js-life-swiper').length) {
    var lifeSwiperOption = {
      speed: 400,
      spaceBetween: 10,
      slidesPerView: 1,
      scrollbar: {
        el: $('.js-life-scrollbar'),
      },
    }
  
    var lifeSwiperActive = false;
    var lifeSwiper;
  
    if ($(window).innerWidth() < 768) {
      lifeSwiper = new Swiper('.js-life-swiper', lifeSwiperOption);
      lifeSwiperActive = true;
    }
  
    $(window).on('resize', function() {
      
      if ($(this).innerWidth() >767 && lifeSwiperActive) {
        lifeSwiper.destroy(1, 1);
        lifeSwiperActive = false;
      } else if($(this).innerWidth() < 768 && lifeSwiperActive === false) {
        lifeSwiper = new Swiper('.js-life-swiper', lifeSwiperOption);
        lifeSwiperActive = true;
      } 
  
    })
  }
  
  


})


$(function () {
    function loadBarInit() {

        var loadBar = document.querySelector('.ldBar');

        if (loadBar === null) return;

        loadBar.ldBar;

    }

    loadBarInit();


})


/*

 
 var bar1 = new ldBar("#myItem1");

 var bar2 = document.getElementById('myItem1').ldBar;
 bar1.set(60);


*/

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {

  options = options || {
    path: '/',
    'max-age': 60*60*24*30
  }

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  var updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (var optionKey in options) {
    updatedCookie += "; " + optionKey;
    var optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}

$(document).ready(function() {
  if(!getCookie('hideCookie')) {
    $('.js-cookie').show();
  }
  $(document).on('click', '.js-cookie-button', function() {
    $('.js-cookie').fadeOut();
    setCookie('hideCookie', 'true');
  });
});
function getScrollbarWidth() {
  return scrollBarWidth = window.innerWidth - $(window).width();
}

$(function() {

  var menuOpen = false;

  $('.js-burger').on('click', function() {
    
    navMenuToggle();
    
  })
  var sUsrAg = navigator.userAgent;

   if (sUsrAg.indexOf("Safari") > -1) {
    $('.nav-menu__menu').removeClass('menu-top--animate-delay')
  } else {
    $('.nav-menu__menu').addClass('menu-top--animate-delay')
  }
  $('.js-menu-anchor').each(function () {
    $(this).on('click', function(event) {
        var re =/#([\w+.-]+)/;
        var link = $(this).attr('href').match(re)[0];

        var scrolled = window.pageYOffset;
        var offsetTop = $(link).offset().top;

        if(scrolled > offsetTop) {
          offsetTop -= $('.header').innerHeight();
        }
      $('html, body').animate({scrollTop:offsetTop},600)
      closeMenu()
    });
  })
  var $menu = $('.js-nav-menu');

  function navMenuToggle() {

      var scrollbarWidth = getScrollbarWidth();

      if (!menuOpen) {
        $menu.addClass('is-active');
        $('.js-burger').addClass('is-active');
        $('.js-menu-tools').css({'padding-right' : scrollBarWidth});
        $('.js-header').css({'padding-right' : scrollBarWidth});
        $('body').css({'overflow' : 'hidden', 'padding-right' : scrollBarWidth});
        $('.nav-menu__menu').css({'padding-right' : scrollBarWidth});
        setTimeout(function(){
          $('.js-main-content').addClass('bg--blur');
          $('.js-header').addClass('is-blur');
          $('.js-cookie').css({
            'position': 'absolute',
            'bottom': (document.documentElement.offsetHeight - window.pageYOffset - window.innerHeight) + 'px'
          });
        },50);
        menuOpen = true;
      } else {
        closeMenu();
      }
  }

  $('.js-menu-inner').each(function(index, element) {
    var menupPs = new PerfectScrollbar(element, {
      wheelSpeed: 1,
      minScrollbarLength: 10,
      maxScrollbarLength : 70,
      suppressScrollX: true
    });
  
    menupPs.update();
    $(window).on('resize', function() {
      menupPs.update();
    });

    element.scrollTop += 1;
    element.scrollTop -= 1;
  });
  $('.js-main-content, .js-header').on('click', function() {
    closeMenu();
  })

  function closeMenu() {
    $menu.removeClass('is-active');
    $('.js-burger').removeClass('is-active');
    $('.js-header').css({'padding-right' : 0});
    $('.js-menu-tools').css({'padding-right' : 0});

    if($('.reviews-popup').hasClass('is-show')){
      $('body').attr('style' , 'overflow: hidden');
    }else{
      $('body').css({'overflow' : 'auto', 'padding-right' : 0});
    }
    
    $('.js-main-content').removeClass('bg--blur');
    $('.js-header').removeClass('is-blur');
    $('.js-cookie').css({
      'position': '',
      'bottom': ''
    });
    $('.nav-menu__menu').css({'padding-right' : ''});

    menuOpen = false;

  }

  $('.js-switch-theme').on('click', function() {
    $('html').toggleClass('is-dark');
    var currentMode =  $('html').attr('class');
    document.cookie = 'currentTheme=' + currentMode + ' max-age=2592000; path=/;';
  })

  function isDarkThemeSelected() {
    return document.cookie.match(/currentTheme=is-dark/i) != null
  }

  function setThemeFromCookie() {
    var root = $('html');

    if (isDarkThemeSelected()) {
      root.addClass('is-dark')
    } else {
      root.removeClass('is-dark')
    }

  }

  setThemeFromCookie()

  $('.js-link-contacts').on('click', function() {
    var $targetElement = $('#contacts');
    if ($targetElement.length) {
      closeMenu();
      event.preventDefault();
      var re =/#([\w+.-]+)/;
      var link = $(this).attr('href').match(re)[0];

      var scrolled = window.pageYOffset;
      var offsetTop = $(link).offset().top;

      if(scrolled > offsetTop) {
        offsetTop -= $('.header').innerHeight();
      }
      $('html, body').animate({scrollTop:offsetTop},600)
    }
  })
  $('.js-link-services').on('click', function() {
    var $targetElement = $('#services');
    if ($targetElement.length) {
      closeMenu();
      event.preventDefault();
      var re =/#([\w+.-]+)/;
      var link = $(this).attr('href').match(re)[0];
      
      var scrolled = window.pageYOffset;
      var offsetTop = $(link).offset().top;

      if(scrolled > offsetTop) {
        offsetTop -= $('.header').innerHeight();
      }

      $('html, body').animate({scrollTop:offsetTop},600)
    }
  })

  if(location.hash === '#services' || location.hash === '#contacts') {
    $(window).trigger('scroll', true);
  }
})
$(function() {
    var timeline = $('.corp-portals-steps__timeline');

    if (timeline.length) {
        var listSteps = $('.corp-portals-steps__list');
        var itemsSteps = $('.corp-portals-steps__item');

        function calcHeightTimeline() {
            var heightListSteps = listSteps.outerHeight();
            var halfHeightFirstStep = $(itemsSteps[0]).outerHeight() / 2;
            var halfHeightLastStep = $(itemsSteps[itemsSteps.length - 1]).outerHeight() / 2;
    
            $(timeline).css({
                top: halfHeightFirstStep,
                height: heightListSteps - (halfHeightFirstStep + halfHeightLastStep),
            });
        }

        function animateItems() {
            var scroll = $(window).scrollTop() + $(window).height();
            var isAnimatedItem = 0;
    
            $(itemsSteps).each(function() {
                isAnimatedItem = 0;
                var heightItem = $(this).outerHeight();
    
                if (scroll - heightItem - heightItem / 2 > $(this).offset().top && isAnimatedItem == 0) {
                    $(this).addClass('fade-in');
                    isAnimatedItem = 1;
                }
            });
        }

        var timelineFill = $('.corp-portals-steps__timeline-fill');
        var circlesSteps = $('.corp-portals-steps__circle');

        function animateTimeline() {
            var heightListSteps = $(listSteps).outerHeight();
            var controlPoint = $(window).scrollTop() + $(window).height() / 2;
            var topTimelineFill = timelineFill.offset().top;
            var heightTimelineFill = controlPoint - topTimelineFill;

            if ($(window).scrollTop() + $(window).height() > topTimelineFill && heightTimelineFill < heightListSteps) {
                $(timelineFill).css({height: heightTimelineFill});

                $(circlesSteps).each(function() {
                    if ($(this).offset().top <= controlPoint) {
                        $(this).addClass('is-active');
                    } else {
                        $(this).removeClass('is-active');
                    }
                });
            }
        }
        
        calcHeightTimeline();
        animateItems();
        animateTimeline();

        $(window).on('resize', function() {
            calcHeightTimeline();
            animateItems();
            animateTimeline();
        });

        $(window).on('scroll' , function() {
            animateItems();
            animateTimeline();
        })
    }
});
$(function () {
  var $consultationForm = $('.js-consultation-form'),
      $errorContainer = $consultationForm.find('.form-error-container'),
      $hiddenField = $consultationForm.find('.js-input-person');

  $consultationForm.validate({
    ignore: [],
    errorLabelContainer: $errorContainer,
    submitHandler: function () {
      if ($hiddenField.val() === '') {
        var data = new FormData();

        $consultationForm.find('.js-input').each(function () {
          var value = $(this).val();

          if (Array.isArray(value)) {
            value = value.join(', ')
          }

          data.append(this.name, value);
        });

        $.ajax({
          url: "/sendfeedback",
          type: "POST",
          data: data,
          processData: false,
          contentType: false,
          success: function () {
            $consultationForm.trigger('formSubmit');
          }
        });
      }

      return false;
    },
    invalidHandler: function (event, validator) {
      setTimeout(function () {
        $('.error').siblings('.contact-form__fill-line').addClass('error');
      }, 100)
    },
    onkeyup: function (element) {
      $(element).valid();
    },
  })
})
$(function() {
  
  if ($('.js-corp-portals-partners-swiper').length) {
    var partnersSwiperOption = {
      speed: 400,
      spaceBetween: 80,
      slidesPerView: 1.5,
      slidesPerColumn: 2,
      slidesPerColumnFill: 'row',
      // slidesPerGroup: 3,
      // centeredSlides: true,
      // centeredSlidesBounds: true,
      scrollbar: {
        el: '.swiper-scrollbar',
      },
    }
  
    var partnersSwiperActive = false;
    var partnersSwiper;
  
    if ($(window).innerWidth() < 768) {
      partnersSwiper = new Swiper('.js-corp-portals-partners-swiper', partnersSwiperOption);
      partnersSwiperActive = true;
    }
  
    $(window).on('resize', function() {
      
      if ($(this).innerWidth() >767 && partnersSwiperActive) {
        partnersSwiper.destroy(1, 1);
        partnersSwiperActive = false;
      } else if($(this).innerWidth() < 768 && partnersSwiperActive === false) {
        partnersSwiper = new Swiper('.js-corp-portals-partners-swiper', partnersSwiperOption);
        partnersSwiperActive = true;
      } 
  
    })

  }

  

})

$(document).ready(function() {

  var reviewsSwiper = new Swiper('.js-reviews-slider', {
    speed: 400,
    slidesPerView: 1 ,
    roundLengths: true,
    spaceBetween: 20,
    watchOverflow: true,
    navigation: {
      nextEl: '.js-reviews-slider~.swiper-button-next',
      prevEl: '.js-reviews-slider~.swiper-button-prev',
    },
    pagination: {
      el: '.js-reviews-slider .swiper-pagination',
      dynamicBullets: true,
      dynamicMainBullets: 3,
      clickable:true
    },
    breakpoints:{
      900: {
        slidesPerView: 2,
        effect: 'fade'
      }
    }
  });

  var popupSwiper = new Swiper('.js-reviews-popup-slider', {
    roundLengths: true,
    spaceBetween: 40,
    watchOverflow: true,
    effect: 'fade',
    navigation: {
      nextEl: '.js-reviews-popup-slider~.swiper-button-next',
      prevEl: '.js-reviews-popup-slider~.swiper-button-prev',
    },
    pagination: {
      el: '.js-reviews-popup-slider .swiper-pagination',
      dynamicBullets: true,
      dynamicMainBullets: 3
    },
    allowTouchMove: false,
    keyboard: {
      enabled: true
    }
  });


  popupSwiper.on('slideChange' , function(){
    updatePopupPadding();
    $('.reviews-popup .reviews-popup__text').scrollTop(0);
    $('.reviews-popup .reviews-popup__text').trigger('ps-y-reach-start');
  }); 


  $('.reviews-popup__text').each(function(index, element) {
    var popupPs = new PerfectScrollbar(element, {
      wheelSpeed: 0.5,
      wheelPropagation: true,
      minScrollbarLength: 10,
      maxScrollbarLength: 15
    });
  
    popupPs.update();
    $(window).on('resize', function() {
      popupPs.update();
    });
    $(element).on('ps:update', function() {
      popupPs.update();
    });

  });

  $(document).on('click' , '.js-show-popup-reviews' , function(){

    var sliderNumber = $(this).closest('.swiper-slide').attr('data-id');
    sliderNumber = parseInt(sliderNumber);

    $('.reviews-popup').addClass('is-show');
    $('body').css({'overflow':'hidden'})
    
    popupSwiper.slideTo(sliderNumber, 1000 , false );
    popupSwiper.update();
    updatePopupPadding();
    $('.reviews-popup .reviews-popup__text').trigger('ps:update');
    $('.reviews-popup .reviews-popup__text').scrollTop(0);
    $('.reviews-popup .reviews-popup__text').trigger('ps-y-reach-start');
  });

  $('.js-hide-popup-reviews').on('click' , function(){
    $('.reviews-popup').removeClass('is-show');
    $('.reviews-popup').css('position' , 'fixed');
    $('body').css({'overflow':'auto'})
  });

  $('.reviews-popup__text').on('ps-scroll-y' , function(){
    $(this).closest('.reviews-popup__text-wrapper').removeClass('is-top');
    $(this).closest('.reviews-popup__text-wrapper').removeClass('is-bottom');
  });

  $('.reviews-popup__text').on('ps-y-reach-start' , function(){
    $(this).closest('.reviews-popup__text-wrapper').addClass('is-top');
  });

  $('.reviews-popup__text').on('ps-y-reach-end' , function(){
    $(this).closest('.reviews-popup__text-wrapper').addClass('is-bottom');
  });

  $(document).on('click' , 'swiper-button-next' , 'swiper-button-prev' , function(){
    $('.reviews-popup').find('.ps__rail-y').css('opacity' , '0' , '!important');
  });

  function updatePopupPadding() {
    $('.reviews-popup__inner').each(function() {
      var $info = $(this).find('.reviews-popup__info'),
          $text = $(this).find('.reviews-popup__text-inner'),
          $wrapper = $(this).find('.reviews-popup__text-wrapper');
      $text.css('padding-top', $info.innerHeight() + 'px');
      $wrapper.css({
        'top': $info.innerHeight() + 'px',
        'margin-top': -$info.innerHeight()*2 + 'px'
      });
    });
  }
  updatePopupPadding();
  $(window).on('resize', function() {
    updatePopupPadding();
  });
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21tb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsid2luZG93Lm9ubG9hZCA9IGZ1bmN0aW9uICgpIHtcclxuICAgIGNsb3NlUHJlbG9hZHJlKCk7XHJcbn07XHJcblxyXG5mdW5jdGlvbiBjbG9zZVByZWxvYWRyZSgpIHtcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciAkcHJlbG9hZGVyID0gJCgnLmpzLXByZWxvYWRlcicpO1xyXG4gICAgICAgICRwcmVsb2FkZXIucmVtb3ZlQ2xhc3MoJ2lzLWFjdGl2ZScpO1xyXG4gICAgICAgICQoJ2JvZHknKS5yZW1vdmVDbGFzcygnbG9hZGluZycpO1xyXG4gICAgfSwgMTAwMClcclxufVxyXG5cclxuY29uc3QgcHJlbG9hZGVyUGFnZVRpdGxlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnByZWxvYWRlcl9fcGFnZS10aXRsZScpXHJcblxyXG5cclxuJChmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgLy8gSUUgZGV0ZWN0aW9uXHJcblxyXG4gICAgaWYgKCEhd2luZG93Lk1TSW5wdXRNZXRob2RDb250ZXh0ICYmICEhZG9jdW1lbnQuZG9jdW1lbnRNb2RlKSB7XHJcbiAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCdicm93c2VyLWllJyk7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIC8vIEludGVybmV0IEV4cGxvcmVyIDYtMTFcclxuICAgIHZhciBpc0lFID0gLypAY2Nfb24hQCovZmFsc2UgfHwgISFkb2N1bWVudC5kb2N1bWVudE1vZGU7XHJcblxyXG4gICAgdmFyIGlkRWRnZSA9ICFpc0lFICYmICEhd2luZG93LlN0eWxlTWVkaWFcclxuXHJcbiAgICBpZiAoIWlzSUUgJiYgISF3aW5kb3cuU3R5bGVNZWRpYSkge1xyXG4gICAgICAgICQoJ2JvZHknKS5hZGRDbGFzcygnYnJvd3Nlci1lZGdlJyk7XHJcbiAgICB9XHJcbiAgICAvLyBBbmRyb2lkIGRldGVjdGlvblxyXG5cclxuICAgIHZhciB1YSA9IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKTtcclxuICAgIHZhciBpc0FuZHJvaWQgPSB1YS5pbmRleE9mKFwiYW5kcm9pZFwiKSA+IC0xO1xyXG5cclxuICAgIGlmIChpc0FuZHJvaWQpIHtcclxuICAgICAgICAkKCdib2R5JykuYWRkQ2xhc3MoJ2lzLWFuZHJvaWQnKTtcclxuICAgIH1cclxuXHJcbiAgICAvL1RvdWNoIGRldGVjdGlvblxyXG4gICAgaWYgKCEoXCJvbnRvdWNoc3RhcnRcIiBpbiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpKSB7XHJcbiAgICAgICAgJCgnYm9keScpLmFkZENsYXNzKCduby10b3VjaCcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEhlYWRlciBzY3JvbGxcclxuXHJcbiAgICB2YXIgcHJldlNjcm9sbCA9IHdpbmRvdy5wYWdlWU9mZnNldDtcclxuICAgIHZhciAkaGVhZGVyID0gJCgnLmhlYWRlcicpO1xyXG5cclxuICAgIGlmIChwcmV2U2Nyb2xsID4gMCkge1xyXG4gICAgICAgICRoZWFkZXIuYWRkQ2xhc3MoJ2lzLWZpeGVkJyk7XHJcbiAgICAgICAgYWRkU2hhZG93KCk7XHJcbiAgICAgICAgJCh0aGlzKS50cmlnZ2VyKCdzY3JvbGwnKTtcclxuICAgIH1cclxuICAgIGlmICh3aW5kb3cubG9jYXRpb24uaGFzaC5tYXRjaCgvI1xcdy8pICYmIHByZXZTY3JvbGwgPiAwKSB7XHJcbiAgICAgICAgJGhlYWRlci5hZGRDbGFzcygnaXMtaGlkZScpXHJcbiAgICB9XHJcbiAgICBmdW5jdGlvbiBoZWFkZXJTY3JvbGwoZXZlbnQsIGlzQW5jaG9yKSB7XHJcbiAgICAgICAgdmFyIGN1cnJlbnRTY3JvbGwgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcbiAgICAgICAgdmFyIGRlbHRhID0gY3VycmVudFNjcm9sbCAtIHByZXZTY3JvbGw7XHJcbiAgICAgICAgaWYgKGRlbHRhIDwgMCkge1xyXG4gICAgICAgICAgICB2YXIgYmRyID0gd2luZG93LmlubmVyV2lkdGggPj0gNzY4ID8gMCA6IDRcclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRTY3JvbGwgPD0gYmRyKSB7XHJcbiAgICAgICAgICAgICAgICAkaGVhZGVyLmNzcygndHJhbnNpdGlvbicsICdub25lJykucmVtb3ZlQ2xhc3MoJ2lzLWZpeGVkIGlzLWhpZGUgaXMtc2hvdycpO1xyXG4gICAgICAgICAgICAgICAgcmVtb3ZlU2hhZG93KCk7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAkaGVhZGVyLmNzcygndHJhbnNpdGlvbicsICcnKTtcclxuICAgICAgICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICgkaGVhZGVyLmlzKCcuaXMtZml4ZWQnKSkge1xyXG4gICAgICAgICAgICAgICAgJGhlYWRlci5yZW1vdmVDbGFzcygnaXMtaGlkZScpLmFkZENsYXNzKCdpcy1zaG93Jyk7XHJcbiAgICAgICAgICAgICAgICBhZGRTaGFkb3coKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSBpZiAoZGVsdGEgPiAwIHx8IGlzQW5jaG9yKSB7XHJcbiAgICAgICAgICAgIGlmICgkaGVhZGVyLmlzKCcuaXMtZml4ZWQnKSkge1xyXG4gICAgICAgICAgICAgICAgJGhlYWRlci5yZW1vdmVDbGFzcygnaXMtc2hvdycpLmFkZENsYXNzKCdpcy1oaWRlJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFNjcm9sbCA+ICRoZWFkZXIub3V0ZXJIZWlnaHQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICRoZWFkZXIuY3NzKCd0cmFuc2l0aW9uJywgJ25vbmUnKS5hZGRDbGFzcygnaXMtZml4ZWQgaXMtaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkaGVhZGVyLmNzcygndHJhbnNpdGlvbicsICcnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcHJldlNjcm9sbCA9IGN1cnJlbnRTY3JvbGw7XHJcbiAgICB9XHJcblxyXG4gICAgJCh3aW5kb3cpLm9uKCdzY3JvbGwnLCBoZWFkZXJTY3JvbGwpO1xyXG5cclxuICAgIGZ1bmN0aW9uIGFkZFNoYWRvdygpIHtcclxuICAgICAgICAkKCcuanMtaGVhZGVyJykuYWRkQ2xhc3MoJ2hlYWRlci0tc2hhZG93JylcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiByZW1vdmVTaGFkb3coKSB7XHJcbiAgICAgICAgJCgnLmpzLWhlYWRlcicpLnJlbW92ZUNsYXNzKCdoZWFkZXItLXNoYWRvdycpXHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGNhcm91c2VsID0gJCgnLnRyaWFuZ2xlX19jYXJvdXNlbCcpWzBdO1xyXG4gICAgdmFyIGFuZ2xlID0gMDtcclxuICAgIHZhciBsYXN0ID0gMTtcclxuICAgIHZhciBpbnRlcnZhbEFuaW1hdGlvbiA9IDUgKiAxMDAwO1xyXG4gICAgdmFyIHRyaWFuZ2xlVGV4dCA9ICQoJy50cmlhbmdsZV9fdGV4dCcpO1xyXG4gICAgdmFyIHN0YXJ0QW5pbWF0ZUVsZW1lbnQgPSAwO1xyXG5cclxuICAgIGZ1bmN0aW9uIHJvdGF0ZUNhcm91c2VsKG5vdykge1xyXG4gICAgICAgIGlmICghbGFzdCB8fCBub3cgLSBsYXN0ID49IGludGVydmFsQW5pbWF0aW9uKSB7XHJcbiAgICAgICAgICAgIGxhc3QgPSBub3c7XHJcblxyXG4gICAgICAgICAgICBhbmdsZSArPSAtMTIwO1xyXG4gICAgICAgICAgICBjYXJvdXNlbC5zdHlsZS50cmFuc2Zvcm0gPSAndHJhbnNsYXRlWigtNDBweCkgcm90YXRlWCgnICsgYW5nbGUgKyAnZGVnKSc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShyb3RhdGVDYXJvdXNlbCk7XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gcm90YXRlQ2Fyb3VzZWxJRSgpIHtcclxuICAgICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQodHJpYW5nbGVUZXh0W3N0YXJ0QW5pbWF0ZUVsZW1lbnRdKS5hbmltYXRlKHtcIm9wYWNpdHlcIjogXCIwXCJ9LCAxMDAwKTtcclxuICAgICAgICAgICAgJCh0cmlhbmdsZVRleHRbc3RhcnRBbmltYXRlRWxlbWVudCArIDFdKS5hbmltYXRlKHtcIm9wYWNpdHlcIjogXCIxXCJ9LCAxMDAwKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChzdGFydEFuaW1hdGVFbGVtZW50ID09IDIpIHtcclxuICAgICAgICAgICAgICAgIHN0YXJ0QW5pbWF0ZUVsZW1lbnQgPSAwO1xyXG4gICAgICAgICAgICAgICAgJCh0cmlhbmdsZVRleHRbc3RhcnRBbmltYXRlRWxlbWVudF0pLmFuaW1hdGUoe1wib3BhY2l0eVwiOiBcIjFcIn0sIDEwMDApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgc3RhcnRBbmltYXRlRWxlbWVudCsrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgaW50ZXJ2YWxBbmltYXRpb24pXHJcbiAgICB9XHJcblxyXG4gICAgISFjYXJvdXNlbCAmJiAhaXNJRSAmJiB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHJvdGF0ZUNhcm91c2VsKTtcclxuICAgICEhY2Fyb3VzZWwgJiYgaXNJRSAmJiByb3RhdGVDYXJvdXNlbElFKCk7XHJcbn0pXHJcblxyXG4vLyBsYXp5IGltYWdlIGxvYWRcclxuXHJcbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnLmpzLWxhenknKS5sYXp5KHtcclxuICAgICAgICB0aHJlc2hvbGQ6IDEyMDBcclxuICAgIH0pO1xyXG59KVxyXG5cclxuJChmdW5jdGlvbigpIHtcclxuICAgICQoJy5qcy1hbmNob3InKS5vbignY2xpY2snLCBmdW5jdGlvbihldmVudCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIHZhciBsaW5rID0gJCh0aGlzKS5hdHRyKCdocmVmJyk7XHJcbiAgICAgICAgdmFyICR0YXJnZXRFbGVtZW50ID0gJChsaW5rKTtcclxuXHJcbiAgICAgICAgaWYgKCR0YXJnZXRFbGVtZW50Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICB2YXIgc2Nyb2xsZWQgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcbiAgICAgICAgICAgIHZhciBvZmZzZXRUb3AgPSAkKGxpbmspLm9mZnNldCgpLnRvcDtcclxuXHJcbiAgICAgICAgICAgIGlmKHNjcm9sbGVkID4gb2Zmc2V0VG9wKSB7XHJcbiAgICAgICAgICAgICAgICBvZmZzZXRUb3AgLT0gJCgnLmhlYWRlcicpLmlubmVySGVpZ2h0KCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtzY3JvbGxUb3A6b2Zmc2V0VG9wfSw2MDApXHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn0pO1xyXG4kKGZ1bmN0aW9uKCkge1xyXG4gIFxyXG4gIGlmICgkKHdpbmRvdykuaW5uZXJXaWR0aCgpIDwgNzY4KSB7XHJcbiAgICAkKGRvY3VtZW50KS5vbigndG91Y2hzdGFydCcsICcuanMtY2FzZS1jYXJkJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoJ3RvdWNoLWRvd24nKTtcclxuICAgIH0pXHJcbiAgICAkKGRvY3VtZW50KS5vbigndG91Y2hlbmQnLCBmdW5jdGlvbigpIHtcclxuICAgICAgJCgnLmpzLWNhc2UtY2FyZCcpLnJlbW92ZUNsYXNzKCd0b3VjaC1kb3duJyk7XHJcbiAgICB9KVxyXG5cclxuICB9XHJcblxyXG4vLyAgIHBvcHVwXHJcblxyXG4gIHZhciAgICRwcm9qZWN0UG9wdXAgPSAkKCcuanMtcHJvamVjdC1wb3B1cCcpLFxyXG4gICAgICAgICRjbG9zZVBvcHVwID0gJCgnLmpzLWNsb3NlLXBvcHVwJyksXHJcbiAgICAgICAgJGJvZHkgPSAkKCdib2R5JyksXHJcbiAgICAgICAgJG1lbnVUb29scyA9ICQoJy5qcy1tZW51LXRvb2xzJyksXHJcbiAgICAgICAgJGhlYWRlciA9ICQoJy5qcy1oZWFkZXInKSxcclxuICAgICAgICAkbWFpbkNvbnRlbnQgPSAkKCcuanMtbWFpbi1jb250ZW50JylcclxuXHJcbiAgdmFyIHBzXHJcblxyXG4gIGZ1bmN0aW9uIG9wZW5Qb3B1cChpZCkge1xyXG4gICAgdmFyIHNjcm9sbGJhcldpZHRoID0gZ2V0U2Nyb2xsYmFyV2lkdGgoKSxcclxuICAgICAgICAkcHJvamVjdFBvcHVwID0gJCgnW2RhdGEtcG9wdXA9JytpZCksXHJcbiAgICAgICAgJHByb2plY3RQb3B1cElubmVyID0gJHByb2plY3RQb3B1cC5maW5kKCcucG9wdXBfX2lubmVyJylcclxuXHJcbiAgICAkcHJvamVjdFBvcHVwLmFkZENsYXNzKCdpcy1hY3RpdmUnKVxyXG4gICAgJGJvZHkuY3NzKHsnb3ZlcmZsb3cnIDogJ2hpZGRlbicsICdwYWRkaW5nLXJpZ2h0JyA6IHNjcm9sbEJhcldpZHRofSlcclxuICAgICRtZW51VG9vbHMuY3NzKHsncGFkZGluZy1yaWdodCcgOiBzY3JvbGxCYXJXaWR0aH0pXHJcbiAgICAkaGVhZGVyLmNzcyh7J3BhZGRpbmctcmlnaHQnIDogc2Nyb2xsQmFyV2lkdGh9KVxyXG4gICAgJG1haW5Db250ZW50LmFkZENsYXNzKCdiZy0tYmx1cicpO1xyXG4gICAgJGhlYWRlci5hZGRDbGFzcygnaXMtYmx1cicpO1xyXG4gICAgJG1lbnVUb29scy5hZGRDbGFzcygnaXMtYmx1cicpO1xyXG4gICAgdmFyICRwb3B1cENvbnRlbnQgPSAkcHJvamVjdFBvcHVwLmZpbmQoJy5wcm9qZWN0LWRldGFpbHMnKVxyXG4gICAgcHMgPSBuZXcgUGVyZmVjdFNjcm9sbGJhcigkcHJvamVjdFBvcHVwSW5uZXJbMF0sIHtcclxuICAgICAgd2hlZWxTcGVlZDogMSxcclxuICAgICAgICBtaW5TY3JvbGxiYXJMZW5ndGg6IDEwLFxyXG4gICAgICAgIG1heFNjcm9sbGJhckxlbmd0aDogNzAsXHJcbiAgICAgICAgc3VwcHJlc3NTY3JvbGxYOiB0cnVlXHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gY2xvc2VQb3B1cCgpIHtcclxuICAgICRwcm9qZWN0UG9wdXAucmVtb3ZlQ2xhc3MoJ2lzLWFjdGl2ZScpXHJcbiAgICAkYm9keS5jc3MoeydvdmVyZmxvdycgOiAnYXV0bycsICdwYWRkaW5nLXJpZ2h0JyA6ICcwJ30pXHJcbiAgICAkbWVudVRvb2xzLmNzcyh7J3BhZGRpbmctcmlnaHQnIDogJzAnfSlcclxuICAgICRoZWFkZXIuY3NzKHsncGFkZGluZy1yaWdodCcgOiAnMCd9KVxyXG4gICAgJG1haW5Db250ZW50LnJlbW92ZUNsYXNzKCdiZy0tYmx1cicpO1xyXG4gICAgJGhlYWRlci5yZW1vdmVDbGFzcygnaXMtYmx1cicpO1xyXG4gICAgJG1lbnVUb29scy5yZW1vdmVDbGFzcygnaXMtYmx1cicpO1xyXG4gICAgcHMuZGVzdHJveSgpXHJcbiAgICBwcyA9IG51bGxcclxuICB9XHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcuanMtb3Blbi1wcm9qZWN0JywgZnVuY3Rpb24oZSkge1xyXG4gICAgdmFyIGhyZWYgPSAkKHRoaXMpLmF0dHIoJ2hyZWYnKVxyXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICBlLnN0b3BQcm9wYWdhdGlvbigpXHJcbiAgICBvcGVuUG9wdXAoaHJlZilcclxuICB9KVxyXG5cclxuICAkcHJvamVjdFBvcHVwLmZpbmQoJy5wb3B1cF9faW5uZXInKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHsgICAgXHJcbiAgICBjbG9zZVBvcHVwKClcclxuICB9KVxyXG5cclxuICAkY2xvc2VQb3B1cC5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgIGNsb3NlUG9wdXAoKVxyXG4gIH0pXHJcblxyXG4gICRwcm9qZWN0UG9wdXAuZmluZCgnLnBvcHVwX193aW5kb3cnKS5vbignY2xpY2snLCBmdW5jdGlvbihlKSB7XHJcbiAgICBlLnN0b3BQcm9wYWdhdGlvbigpXHJcbiAgfSlcclxuXHJcblxyXG59KVxyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICBsZXQgYnV0dG9uID0gJCgnLmpzLXNob3ctYXJ0aWNsZScpO1xyXG5cclxuICAgIGZ1bmN0aW9uIGhhbmRsZUJ0bihwYWdlLCBwYWdlc0NvdW50KSB7XHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYgKHBhZ2UgPj0gcGFnZXNDb3VudCkge1xyXG4gICAgICAgICAgICBidXR0b24ucGFyZW50KCkuaGlkZSgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoYnV0dG9uLnBhcmVudCgpLmlzKCc6aGlkZGVuJykpIHtcclxuICAgICAgICAgICAgYnV0dG9uLnBhcmVudCgpLnNob3coKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBjb25zdCBhcnRpY2xlcyA9ICQoJ2JvZHknKS5maW5kKCcuanMtYXJ0aWNsZXMnKTtcclxuXHJcblxyXG4gICAgJCgnYm9keScpLm9uKCdjbGljaycsICcuanMtc2hvdy1hcnRpY2xlJywgZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICBpZiAoIWFydGljbGVzLmhhc0NsYXNzKFwibG9hZGluZ1wiKSkge1xyXG5cclxuICAgICAgICAgICAgbGV0IHBhZ2UgPSBhcnRpY2xlcy5hdHRyKCdkYXRhLXBhZ2UnKSA/IHBhcnNlSW50KGFydGljbGVzLmF0dHIoJ2RhdGEtcGFnZScpKSA6IDE7XHJcbiAgICAgICAgICAgIGxldCBjb3VudFBhZ2VzID0gYXJ0aWNsZXMuYXR0cignZGF0YS1wYWdlcycpID8gcGFyc2VJbnQoYXJ0aWNsZXMuYXR0cignZGF0YS1wYWdlcycpKSA6IDE7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBhcnRpY2xlcy5kYXRhKCdhY3Rpb24nKTtcclxuICAgICAgICAgICAgbGV0IGRhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBwYWdlOiBwYWdlICsgMVxyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgbGV0IGNhdGVnb3J5SWQgPSBnZXRDYXRlZ29yeUlEKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoY2F0ZWdvcnlJZCkge1xyXG4gICAgICAgICAgICAgICAgZGF0YS5jYXRlZ29yeSA9IGNhdGVnb3J5SWQ7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChwYWdlIDwgY291bnRQYWdlcykge1xyXG5cclxuICAgICAgICAgICAgICAgIGFydGljbGVzLmFkZENsYXNzKFwibG9hZGluZ1wiKTtcclxuICAgICAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcIkdFVFwiXHJcbiAgICAgICAgICAgICAgICB9KS5kb25lKGZ1bmN0aW9uIChodG1sKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCQoaHRtbCkuZmluZCgnLmpzLWFydGljbGVzJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJ0aWNsZXMuYXBwZW5kKCQoaHRtbCkuaHRtbCgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJ0aWNsZXMuYXR0cignZGF0YS1wYWdlJywgJChodG1sKS5kYXRhKCdwYWdlJykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVCdG4oJChodG1sKS5kYXRhKCdwYWdlJyksICQoaHRtbCkuZGF0YSgncGFnZXMnKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSkuYWx3YXlzKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBhcnRpY2xlcy5yZW1vdmVDbGFzcygnbG9hZGluZycpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIChlKSA9PiB7XHJcbiAgICAgICAgbGV0IHJhZGlvQnRuID0gZS50YXJnZXQuY2xvc2VzdCgnLmFydGljbGVzIC5qcy1maWx0ZXItaXRlbV9faW5wdXQnKTtcclxuICAgICAgICBsZXQgcmVtb3ZlRmlsdGVyUmFkaW9CdG4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuYXJ0aWNsZXMgLmpzLWZpbHRlci1pdGVtX19pbnB1dCcpO1xyXG4gICAgICAgIGlmICghIXJhZGlvQnRuKSB7XHJcbiAgICAgICAgICAgIHJlbW92ZUZpbHRlclJhZGlvQnRuLmZvckVhY2goZWwgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVsLmNsYXNzTGlzdC5jb250YWlucygnY2hlY2tlZCcpICYmIGVsICE9PSByYWRpb0J0bikge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsLmNsYXNzTGlzdC5yZW1vdmUoJ2NoZWNrZWQnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgaWYgKHJhZGlvQnRuLmNoZWNrZWQgJiYgcmFkaW9CdG4uY2xhc3NMaXN0LmNvbnRhaW5zKCdjaGVja2VkJykpIHtcclxuICAgICAgICAgICAgICAgIHJhZGlvQnRuLmNsYXNzTGlzdC5yZW1vdmUoJ2NoZWNrZWQnKTtcclxuICAgICAgICAgICAgICAgIHJhZGlvQnRuLmNoZWNrZWQgPSBmYWxzZVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmFkaW9CdG4uY2xhc3NMaXN0LmFkZCgnY2hlY2tlZCcpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcnRpY2xlcyA9ICQoJ2JvZHknKS5maW5kKCcuanMtYXJ0aWNsZXMnKTtcclxuICAgICAgICAgICAgaWYgKGFydGljbGVzKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgdXJsID0gJCgnLmFydGljbGVzIC5qcy1maWx0ZXInKS5kYXRhKCdhY3Rpb24nKTtcclxuICAgICAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICAgICAgdXJsOiB1cmwsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYWdlOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeTogZ2V0Q2F0ZWdvcnlJRCgpXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBcIkdFVFwiXHJcbiAgICAgICAgICAgICAgICB9KS5kb25lKGZ1bmN0aW9uIChodG1sKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCQoaHRtbCkuZmluZCgnLmpzLWFydGljbGVzJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYXJ0aWNsZXMuaHRtbCgkKGh0bWwpLmh0bWwoKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFydGljbGVzLmF0dHIoJ2RhdGEtcGFnZScsIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVCdG4oMSwgJChodG1sKS5kYXRhKCdwYWdlcycpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KVxyXG5cclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG5cclxuICAgIH0pXHJcblxyXG4gICAgZnVuY3Rpb24gZ2V0Q2F0ZWdvcnlJRCgpIHtcclxuICAgICAgICByZXR1cm4gJCgnLmpzLWZpbHRlci1pdGVtX19pbnB1dC5jaGVja2VkJykudmFsKCk7XHJcbiAgICB9XHJcblxyXG59KTtcclxudmFyIGluaXRTd2lwZXIgPSBmdW5jdGlvbihzd2lwZXJTZWxlY3Rvcikge1xyXG4gIHZhciBwYXJ0bmVyc1N3aXBlck9wdGlvbiA9IHtcclxuICAgIHVwZGF0ZU9uV2luZG93UmVzaXplOiB0cnVlLFxyXG4gICAgc3BlZWQ6IDQwMCxcclxuICAgIHNsaWRlc1BlclZpZXc6ICdhdXRvJyxcclxuICAgIHNsaWRlc1BlckNvbHVtbkZpbGw6ICdyb3cnLFxyXG4gICAgc2Nyb2xsYmFyOiB7XHJcbiAgICAgIGVsOiAnLnN3aXBlci1zY3JvbGxiYXInLFxyXG4gICAgfSxcclxuICAgIGFsbG93VG91Y2hNb3ZlOiBmYWxzZSxcclxuICB9XHJcbiAgaWYgKHdpbmRvdy5pbm5lcldpZHRoIDwgNzY4KSB7XHJcbiAgICBwYXJ0bmVyc1N3aXBlck9wdGlvbi5sb29wZWRTbGlkZXMgPSAzO1xyXG4gICAgcGFydG5lcnNTd2lwZXJPcHRpb24uYWxsb3dUb3VjaE1vdmUgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIG5ldyBTd2lwZXIoJy5qcy1wYXJ0bmVycy1zd2lwZXInLCBwYXJ0bmVyc1N3aXBlck9wdGlvbik7XHJcbn1cclxuXHJcbiQoZnVuY3Rpb24oKSB7XHJcbiAgdmFyIHN3aXBlclNlbGVjdG9yID0gJy5qcy1wYXJ0bmVycy1zd2lwZXInO1xyXG4gIHZhciBpbm5lclN3aXBlclNlbGVjdG9yID0gJy5wYXJ0bmVyc19faW5uZXIuc3dpcGVyLXdyYXBwZXInO1xyXG4gIHZhciBwYXJ0bmVyc1N3aXBlciA9IGluaXRTd2lwZXIoc3dpcGVyU2VsZWN0b3IpO1xyXG4gIFxyXG4gIHZhciBzd2lwZXJXaWR0aDtcclxuICBcclxuICBpZiAoISQoc3dpcGVyU2VsZWN0b3IpLmxlbmd0aCkgcmV0dXJuO1xyXG5cclxuICBmdW5jdGlvbiBnZXRTd2lwZXJXaWR0aCgpIHtcclxuICAgIGlmKHN3aXBlcldpZHRoKSB7XHJcbiAgICAgIHJldHVybiBzd2lwZXJXaWR0aDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHN3aXBlcldpZHRoID0gMDtcclxuICAgICAgZm9yKGtleSA9IDA7IGtleSA8IHBhcnRuZXJzU3dpcGVyLnNsaWRlc1NpemVzR3JpZC5sZW5ndGg7IGtleSsrKSB7XHJcbiAgICAgICAgc3dpcGVyV2lkdGggKz0gcGFydG5lcnNTd2lwZXIuc2xpZGVzU2l6ZXNHcmlkW2tleV07XHJcbiAgICAgIH1cclxuICAgICAgLy9kb2N1bWVudC5xdWVyeVNlbGVjdG9yKGlubmVyU3dpcGVyU2VsZWN0b3IpLnN0eWxlLnRyYW5zZm9ybSA9IFwidHJhbnNsYXRlWChcIiArICgtcGFyc2VJbnQoKHN3aXBlcldpZHRoIC0gd2luZG93LmlubmVyV2lkdGgpIC8gMikpICsgXCJweClcIjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gc3dpcGVyV2lkdGg7XHJcbiAgfVxyXG5cclxuICBmdW5jdGlvbiBzd2lwZXJNb3VzZVRyaWdnZXIoZXZlbnQpIHtcclxuICAgIHZhciB3aW5kb3dXaWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoIC0gMjA7XHJcbiAgICB2YXIgY2VudGVyWCA9IHBhcnNlSW50KHdpbmRvdy5pbm5lcldpZHRoLzIpO1xyXG4gIFxyXG4gICAgdmFyIHN3aXBlcldyYXBwZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGlubmVyU3dpcGVyU2VsZWN0b3IpO1xyXG4gICAgdmFyIHN3aXBlcldpZHRoID0gZ2V0U3dpcGVyV2lkdGgoKTtcclxuICBcclxuICAgIHZhciBtb3VzZVggPSBldmVudC5jbGllbnRYO1xyXG4gICAgdmFyIGRpZmZYID0gbW91c2VYLWNlbnRlclg7XHJcbiAgXHJcbiAgICB2YXIgbWFyZ2luc091dGVyT2ZXaW5kb3cgPSBzd2lwZXJXaWR0aC13aW5kb3dXaWR0aDtcclxuICAgIHZhciBtYXJnaW5MZWZ0ID0gbWFyZ2luc091dGVyT2ZXaW5kb3cvMiArIGRpZmZYL3dpbmRvd1dpZHRoKm1hcmdpbnNPdXRlck9mV2luZG93O1xyXG4gICAgc3dpcGVyV3JhcHBlci5zdHlsZS50cmFuc2Zvcm0gPSBcInRyYW5zbGF0ZVgoXCIgKyAoLXBhcnNlSW50KG1hcmdpbkxlZnQpKSArIFwicHgpXCI7XHJcbiAgfVxyXG4gIFxyXG4gICQod2luZG93KS5vbigncmVzaXplJywgZnVuY3Rpb24oKSB7XHJcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPiA3NjcpIHtcclxuICAgICAgcGFydG5lcnNTd2lwZXIucGFyYW1zLmxvb3BlZFNsaWRlcyA9IDc7XHJcbiAgICAgIHBhcnRuZXJzU3dpcGVyLmFsbG93VG91Y2hNb3ZlID0gZmFsc2U7XHJcblxyXG4gICAgICBwYXJ0bmVyc1N3aXBlci51cGRhdGUoKTtcclxuICAgICAgc3dpcGVyV2lkdGggPSBmYWxzZTtcclxuXHJcbiAgICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIHN3aXBlck1vdXNlVHJpZ2dlcik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBkb2N1bWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBzd2lwZXJNb3VzZVRyaWdnZXIpO1xyXG4gICAgICBwYXJ0bmVyc1N3aXBlci5wYXJhbXMubG9vcGVkU2xpZGVzID0gMztcclxuICAgICAgcGFydG5lcnNTd2lwZXIuYWxsb3dUb3VjaE1vdmUgPSB0cnVlO1xyXG5cclxuICAgICAgcGFydG5lcnNTd2lwZXIudXBkYXRlKCk7XHJcbiAgICAgIHN3aXBlcldpZHRoID0gZmFsc2U7XHJcblxyXG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGlubmVyU3dpcGVyU2VsZWN0b3IpLnN0eWxlLnRyYW5zZm9ybSA9IFwidHJhbnNsYXRlWCgtNTEwcHgpXCI7XHJcbiAgICB9XHJcbiAgfSlcclxuXHJcbiAgaWYod2luZG93LmlubmVyV2lkdGggPiA3NjcpIHtcclxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsIHN3aXBlck1vdXNlVHJpZ2dlcik7XHJcbiAgfSBlbHNlIHtcclxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5uZXJTd2lwZXJTZWxlY3Rvcikuc3R5bGUudHJhbnNmb3JtID0gXCJ0cmFuc2xhdGVYKC01MTBweClcIjtcclxuICB9XHJcblxyXG4gIGdldFN3aXBlcldpZHRoKCk7XHJcbn0pXHJcbiQoZnVuY3Rpb24gKCkge1xyXG5cclxuICAvLyBzZWxlY3QyIGluaXRcclxuXHJcbiAgJCgnLmpzLWNvbnRhY3Qtc2VsZWN0Jykuc2VsZWN0Mih7XHJcbiAgICBkcm9wZG93blBvc2l0aW9uOiAnYmVsb3cnLFxyXG4gICAgZHJvcGRvd25Dc3NDbGFzczogJ25vLXNlYXJjaCcsXHJcbiAgICBwbGFjZWhvbGRlcjoge1xyXG4gICAgICBpZDogJycsXHJcbiAgICAgIHRleHQ6ICcnXHJcbiAgICB9LFxyXG4gICAgZHJvcGRvd25QYXJlbnQ6ICQoJy5qcy1jb250YWN0LXNlbGVjdC1yb3cnKSxcclxuICAgIGNsb3NlT25TZWxlY3Q6IGZhbHNlLFxyXG4gICAgbXVsdGlwbGU6IHRydWVcclxuICB9KTtcclxuXHJcbiAgJCgnLmpzLWNvbnRhY3Qtc2VsZWN0Jykub24oJ3NlbGVjdDI6b3BlbicsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICB2YXIgZXZ0ID0gXCJzY3JvbGwuc2VsZWN0MlwiO1xyXG4gICAgJChlLnRhcmdldCkucGFyZW50cygpLm9mZihldnQpO1xyXG4gICAgJCh3aW5kb3cpLm9mZihldnQpO1xyXG4gIH0pO1xyXG5cclxuXHJcbiAgLy8gQ29udGFjdCBmb3JtIGxhYmVscyBlZmZlY3RzXHJcblxyXG4gICQoJy5qcy1pbnB1dCcpLm9uKCdmb2N1cycsIGZ1bmN0aW9uICgpIHtcclxuICAgICQodGhpcykuc2libGluZ3MoJy5qcy1mb3JtLWxhYmVsJykuYWRkQ2xhc3MoJ2lzLXVwJyk7XHJcbiAgfSlcclxuICBpZigkKCcuY29udGFjdC1mb3JtX19pbnB1dC0tdGV4dGFyZWEnKS52YWx1ZSl7XHJcbiAgICAkKHRoaXMpLmNsb3Nlc3QoJy5jb250YWN0LWZvcm1fX3Jvdy0tbWVzc2FnZScpLmNoaWxkcmVuKCcuanMtZm9ybS1sYWJlbCcpLmFkZENsYXNzKCdpcy11cCcpO1xyXG4gIH1cclxuICAkKCcuY29udGFjdC1mb3JtX19pbnB1dC0tdGV4dGFyZWEnKS5vbignZm9jdXMnLCBmdW5jdGlvbigpe1xyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCcuY29udGFjdC1mb3JtX19yb3ctLW1lc3NhZ2UnKS5jaGlsZHJlbignLmpzLWZvcm0tbGFiZWwnKS5hZGRDbGFzcygnaXMtdXAnKTtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLmNvbnRhY3QtZm9ybV9fcm93LS1tZXNzYWdlJykuY2hpbGRyZW4oJy5jb250YWN0LWZvcm1fX2ZpbGwtbGluZScpLmNzcygnd2lkdGgnLCAnMTAwJScpO1xyXG4gIH0pXHJcbiAgJCgnLmNvbnRhY3QtZm9ybV9faW5wdXQtLXRleHRhcmVhJykub24oJ2ZvY3Vzb3V0JywgZnVuY3Rpb24oKXtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLmNvbnRhY3QtZm9ybV9fcm93LS1tZXNzYWdlJykuY2hpbGRyZW4oJy5jb250YWN0LWZvcm1fX2ZpbGwtbGluZScpLmNzcygnd2lkdGgnLCAnMCcpO1xyXG4gIH0pXHJcblxyXG4gICQoJy5qcy1pbnB1dCcpLm9uKCdibHVyJywgZnVuY3Rpb24gKCkge1xyXG4gICAgaWYgKCEkKHRoaXMpLnZhbCgpLmxlbmd0aCkge1xyXG4gICAgICAkKHRoaXMpLnNpYmxpbmdzKCcuanMtZm9ybS1sYWJlbCcpLnJlbW92ZUNsYXNzKCdpcy11cCcpO1xyXG4gICAgICBpZigkKHRoaXMpLmhhc0NsYXNzKCdjb250YWN0LWZvcm1fX2lucHV0LS10ZXh0YXJlYScpKXtcclxuICAgICAgICAkKHRoaXMpLmNsb3Nlc3QoJy5jb250YWN0LWZvcm1fX3Jvdy0tbWVzc2FnZScpLmNoaWxkcmVuKCcuanMtZm9ybS1sYWJlbCcpLnJlbW92ZUNsYXNzKCdpcy11cCcpO1xyXG4gICAgICAgIFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSlcclxuXHJcbiAgJCgnLmpzLWNvbnRhY3Qtc2VsZWN0Jykub24oJ3NlbGVjdDI6b3BlbmluZyBzZWxlY3QyOmNoYW5nZScsIGZ1bmN0aW9uICgpIHtcclxuICAgICQodGhpcykuc2libGluZ3MoJy5qcy1mb3JtLWxhYmVsJykuYWRkQ2xhc3MoJ2lzLXVwJyk7XHJcbiAgICAkc2VsZWN0Q29udGFpbmVyLmZpbmQoJy5jb250YWN0LWZvcm1fX2ZpbGwtbGluZScpLnJlbW92ZUNsYXNzKCdzZWxlY3QtZXJyb3InKTtcclxuICB9KVxyXG5cclxuICAkKCcuanMtY29udGFjdC1zZWxlY3QnKS5vbignc2VsZWN0MjpjbG9zZScsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICB2YXIgJHNlbGVjdGVkT3B0aW9ucyA9ICQoJy5zZWxlY3QyLXNlbGVjdGlvbl9fY2hvaWNlJyk7XHJcblxyXG4gICAgaWYgKCEkc2VsZWN0ZWRPcHRpb25zLmxlbmd0aCkge1xyXG4gICAgICAkKHRoaXMpLnNpYmxpbmdzKCcuanMtZm9ybS1sYWJlbCcpLnJlbW92ZUNsYXNzKCdpcy11cCcpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgfSlcclxuXHJcbiAgdmFyIHBzOyAvLyBQZXJmZWN0IFNjcm9sbGJhciBjb250YWluZXJcclxuXHJcbiAgJCgnLmpzLWNvbnRhY3Qtc2VsZWN0Jykub24oJ3NlbGVjdDI6b3BlbicsIGZ1bmN0aW9uIChlKSB7XHJcblxyXG4gICAgdmFyICRzZWxlY3RSZXN1bHN0cyA9ICQoJy5zZWxlY3QyLXJlc3VsdHNfX29wdGlvbnMnKTtcclxuXHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgcHMgPSBuZXcgUGVyZmVjdFNjcm9sbGJhcigkc2VsZWN0UmVzdWxzdHNbMF0sIHtcclxuICAgICAgICB3aGVlbFNwZWVkOiAxLFxyXG4gICAgICAgIG1pblNjcm9sbGJhckxlbmd0aDogMTAsXHJcbiAgICAgICAgbWF4U2Nyb2xsYmFyTGVuZ3RoOiA3MCxcclxuICAgICAgICBzdXBwcmVzc1Njcm9sbFg6IHRydWVcclxuICAgICAgfSk7XHJcbiAgICB9LCAxMClcclxuXHJcbiAgfSlcclxuXHJcbiAgJCgnLmpzLWNvbnRhY3Qtc2VsZWN0Jykub24oJ3NlbGVjdDI6Y2xvc2luZycsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICBwcy5kZXN0cm95KCk7XHJcbiAgICBwcyA9IG51bGw7XHJcbiAgfSlcclxuXHJcbiAgLy8gQ29udGFjdCBmb3JtIHZhbGlkYXRpb25cclxuXHJcbiAgJCgnaHRtbFtsYW5nPVwicnVcIl0gLmpzLXBob25lJykuaW5wdXRtYXNrKHtcclxuICAgICdtYXNrJzogJys3ICg5OTkpIDk5OS05OS05OScsXHJcbiAgICBzaG93TWFza09uSG92ZXI6IGZhbHNlLFxyXG4gICAgb25CZWZvcmVQYXN0ZTogZnVuY3Rpb24gKHBhc3RlZFZhbHVlLCBvcHRzKSB7XHJcbiAgICAgIHZhciBuZXdWYWx1ZSA9IHBhc3RlZFZhbHVlLnJlcGxhY2UoL1teMC05XS9pZywgJycpO1xyXG4gICAgICBuZXdWYWx1ZSA9IG5ld1ZhbHVlLnN1YnN0cmluZyhuZXdWYWx1ZS5sZW5ndGggLSAxMCk7XHJcbiAgICAgIHJldHVybiBuZXdWYWx1ZTtcclxuICAgIH0sXHJcbiAgfSk7XHJcblxyXG4gICQoJ2h0bWxbbGFuZz1cImVuXCJdIC5qcy1waG9uZScpLm9uKCdpbnB1dCcsIGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIHZhbHVlID0gJCh0aGlzKS52YWwoKTtcclxuICAgIHZhbHVlID0gdmFsdWUucmVwbGFjZSgvW14wLTlcXCtcXC1cXChcXClcXHMpXS9pZywgJycpO1xyXG4gICAgJCh0aGlzKS52YWwodmFsdWUpO1xyXG4gIH0pO1xyXG5cclxuICB2YXIgJGNvbnRhY3RGb3JtID0gJCgnLmpzLWNvbnRhY3QtZm9ybScpLFxyXG4gICAgJGVycm9yQ29udGFpbmVyID0gJGNvbnRhY3RGb3JtLmZpbmQoJy5mb3JtLWVycm9yLWNvbnRhaW5lcicpLFxyXG4gICAgJHNlbGVjdENvbnRhaW5lciA9ICQoJy5qcy1jb250YWN0LXNlbGVjdC1yb3cnKSxcclxuICAgICRoaWRkZW5GaWVsZCA9ICRjb250YWN0Rm9ybS5maW5kKCcuanMtaW5wdXQtcGVyc29uJyk7XHJcblxyXG4gICQoJ2Zvcm0nKS5vbignZm9ybVJlc2V0JywgZnVuY3Rpb24gKCkge1xyXG4gICAgJCh0aGlzKVswXS5yZXNldCgpO1xyXG4gICAgJCh0aGlzKS5maW5kKCcuY29udGFjdC1mb3JtX19maWxsLWxpbmUsIGlucHV0JykucmVtb3ZlQ2xhc3MoJ2Vycm9yIHNlbGVjdC1lcnJvcicpO1xyXG4gICAgJCh0aGlzKS5maW5kKCcuanMtZmlsZS1uYW1lJykuaHRtbCgn0J/RgNC40LrRgNC10L/QuNGC0Ywg0YDQtdC30Y7QvNC1Jyk7XHJcbiAgICAkKHRoaXMpLmZpbmQoJy5qcy1jb250YWN0LXNlbGVjdCcpLnZhbCgnJyk7XHJcbiAgICAkKHRoaXMpLmZpbmQoJy5zZWxlY3QyLXNlbGVjdGlvbl9fY2hvaWNlJykucmVtb3ZlKCk7XHJcbiAgICAkKHRoaXMpLmZpbmQoJy5qcy1mb3JtLWxhYmVsJykucmVtb3ZlQ2xhc3MoJ2lzLXVwJyk7XHJcbiAgfSlcclxuXHJcbiAgJCgnZm9ybScpLm9uKCdmb3JtU3VibWl0JywgZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyICRmb3JtU3VjY2VzID0gJCh0aGlzKS5zaWJsaW5ncygnLmpzLWZvcm0tc3VjY2VzJyk7XHJcblxyXG4gICAgJCh0aGlzKS50cmlnZ2VyKCdmb3JtUmVzZXQnKTtcclxuICAgICRmb3JtU3VjY2VzLmNzcyh7ICdkaXNwbGF5JzogJ2Jsb2NrJyB9KTtcclxuICAgICQoJy5jb250YWN0LWZvcm1fX2lucHV0JykuYmx1cigpO1xyXG4gICAgJGZvcm1TdWNjZXMuc3RvcCgpLmFuaW1hdGUoe1xyXG4gICAgICBvcGFjaXR5OiAxLFxyXG4gICAgfSwgMjAwKVxyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICRmb3JtU3VjY2VzLnN0b3AoKS5hbmltYXRlKHtcclxuICAgICAgICBvcGFjaXR5OiAwLFxyXG4gICAgICB9LCAyMDApXHJcbiAgICAgICRmb3JtU3VjY2VzLmhpZGUoMjAwKTtcclxuICAgIH0sIDMwMDApXHJcbiAgfSlcclxuXHJcbiAgalF1ZXJ5LnZhbGlkYXRvci5hZGRNZXRob2QoXCJjdXN0b21FbWFpbFwiLCBmdW5jdGlvbiAodmFsdWUsIGVsZW1lbnQpIHtcclxuICAgIC8vIGFsbG93IGFueSBub24td2hpdGVzcGFjZSBjaGFyYWN0ZXJzIGFzIHRoZSBob3N0IHBhcnRcclxuICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8IC9eKChbXjw+KClcXFtcXF1cXFxcLiw7Olxcc0BcIl0rKFxcLltePD4oKVxcW1xcXVxcXFwuLDs6XFxzQFwiXSspKil8KFwiLitcIikpQCgoXFxbWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfV0pfCgoW2EtekEtWlxcLTAtOV0rXFwuKStbYS16QS1aXXsyLH0pKSQvLnRlc3QodmFsdWUpO1xyXG4gIH0sICcnKTtcclxuXHJcbiAgalF1ZXJ5LnZhbGlkYXRvci5hZGRNZXRob2QoXCJjdXN0b21QaG9uZVwiLCBmdW5jdGlvbiAodmFsdWUsIGVsZW1lbnQpIHtcclxuICAgIC8vIGFsbG93IGFueSBub24td2hpdGVzcGFjZSBjaGFyYWN0ZXJzIGFzIHRoZSBob3N0IHBhcnRcclxuICAgIHJldHVybiB0aGlzLm9wdGlvbmFsKGVsZW1lbnQpIHx8IChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQubGFuZyAhPT0gJ2VuJyA/IDEwID09PSAkKGVsZW1lbnQpLmlucHV0bWFzayhcInVubWFza2VkdmFsdWVcIikubGVuZ3RoIDogdHJ1ZSlcclxuICB9LCAnJyk7XHJcblxyXG4gICRjb250YWN0Rm9ybS52YWxpZGF0ZSh7XHJcbiAgICBpZ25vcmU6IFtdLFxyXG4gICAgZXJyb3JMYWJlbENvbnRhaW5lcjogJGVycm9yQ29udGFpbmVyLFxyXG4gICAgc3VibWl0SGFuZGxlcjogZnVuY3Rpb24gKCkge1xyXG4gICAgICBpZiAoJGhpZGRlbkZpZWxkLnZhbCgpID09PSAnJykge1xyXG4gICAgICAgIHZhciBkYXRhID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgICAgJGNvbnRhY3RGb3JtLmZpbmQoJy5qcy1pbnB1dCcpLmZpbHRlcignaW5wdXQsIHNlbGVjdCwgdGV4dGFyZWEnKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgIGlmKCQodGhpcykuaXMoJ2lucHV0W3R5cGU9XCJyYWRpb1wiXScpICYmICEkKHRoaXMpLnByb3AoJ2NoZWNrZWQnKSkgcmV0dXJuO1xyXG4gICAgICAgICAgdmFyIHZhbHVlID0gJCh0aGlzKS52YWwoKTtcclxuICAgICAgICAgIGRhdGEuYXBwZW5kKHRoaXMubmFtZSwgdmFsdWUpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgdXJsOiBcIi9zZW5kZmVlZGJhY2tcIixcclxuICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgIHByb2Nlc3NEYXRhOiBmYWxzZSxcclxuICAgICAgICAgIGNvbnRlbnRUeXBlOiBmYWxzZSxcclxuICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJGNvbnRhY3RGb3JtLnRyaWdnZXIoJ2Zvcm1TdWJtaXQnKTtcclxuICAgICAgICAgICAgJGNvbnRhY3RGb3JtLnZhbGlkYXRlKCkucmVzZXRGb3JtKClcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH0sXHJcbiAgICBpbnZhbGlkSGFuZGxlcjogZnVuY3Rpb24gKGV2ZW50LCB2YWxpZGF0b3IpIHtcclxuXHJcbiAgICAgIHZhciAkc2VsZWN0ZWRPcHRpb25zID0gJCgnLnNlbGVjdDItc2VsZWN0aW9uX19jaG9pY2UnKTtcclxuICAgICAgaWYgKCEkc2VsZWN0ZWRPcHRpb25zLmxlbmd0aCkge1xyXG4gICAgICAgICRzZWxlY3RDb250YWluZXIuY2hpbGRyZW4oJy5jb250YWN0LWZvcm1fX2ZpbGwtbGluZScpLmFkZENsYXNzKCdzZWxlY3QtZXJyb3InKTtcclxuICAgICAgfVxyXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCcuZXJyb3InKS5zaWJsaW5ncygnLmNvbnRhY3QtZm9ybV9fZmlsbC1saW5lJykuYWRkQ2xhc3MoJ2Vycm9yJyk7XHJcbiAgICAgIH0sIDEwMClcclxuICAgIH0sXHJcbiAgICBvbmtleXVwOiBmdW5jdGlvbiAoZWxlbWVudCkge1xyXG4gICAgICAkKGVsZW1lbnQpLnZhbGlkKCk7XHJcbiAgICB9LFxyXG5cclxuICB9KVxyXG5cclxuICAkKCcuanMtaW5wdXQtbmFtZScpLm9uKCdpbnB1dCcsIGZ1bmN0aW9uICgpIHtcclxuICAgIHZhciB2YWx1ZSA9ICQodGhpcykudmFsKCk7XHJcbiAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UoL1teYS16QS1a0LAt0Y/QkC3Qr1xcc1xcLV0vaWcsICcnKTtcclxuICAgICQodGhpcykudmFsKHZhbHVlKTtcclxuICB9KTtcclxuXHJcbiAgJCgnLmpzLW9wZW4tbWFwJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnLmpzLW1hcCcpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTtcclxuICB9KVxyXG5cclxuICAkKCcuanMtY2xvc2UtbWFwJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnLmpzLW1hcCcpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuICB9KVxyXG5cclxuICAkKCcuanMtb3Blbi1jb250YWN0LWZvcm0nKS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcuanMtZm9ybS1ob2xkZXInKS5hZGRDbGFzcygnaXMtYWN0aXZlJyk7XHJcbiAgICAkKCdodG1sLCBib2R5JykuY3NzKHsgJ292ZXJmbG93JzogJ2hpZGRlbicgfSk7XHJcbiAgfSlcclxuXHJcbiAgJCgnLmpzLWNsb3NlLWNvbnRhY3QtZm9ybScpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICQoJy5qcy1mb3JtLWhvbGRlcicpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICQoJ2h0bWwsIGJvZHknKS5jc3MoJ292ZXJmbG93JywgJycpO1xyXG4gICAgJGNvbnRhY3RGb3JtLnRyaWdnZXIoJ2Zvcm1SZXNldCcpXHJcbiAgfSlcclxuXHJcblxyXG59KVxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKXtcclxuICAkKCcuY29udGFjdC1mb3JtX19pbnB1dC0tdGV4dGFyZWFfc2Nyb2xsJykuc2Nyb2xsYmFyKHtcclxuICAgIFwiZHVyYXRpb25cIjogXCIxMDAwXCIsXHJcbiAgICBcImF1dG9VcGRhdGVcIjogdHJ1ZVxyXG4gIH0pO1xyXG59KTtcclxuXHJcblxyXG4kKCBkb2N1bWVudCApLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gIHZhciBpc0lFID0gLypAY2Nfb24hQCovZmFsc2UgfHwgISFkb2N1bWVudC5kb2N1bWVudE1vZGU7XHJcbiAgaWYgKGlzSUUpIHtcclxuXHJcblxyXG4gICAgdmFyIGJvcmRlckJvdHRvbSA9IFwiPHNwYW4gY2xhc3M9J2xpbmstLXByaXZhY3lfYm9yZGVyLWInPjwvc3Bhbj5cIjtcclxuICAgIHZhciBsaW5rQiA9ICQoJy5saW5rLS1wcml2YWN5Jyk7XHJcblxyXG4gICAgJCgnLmxpbmstLXByaXZhY3knKS5hcHBlbmQoYm9yZGVyQm90dG9tKTtcclxuICAgICQoJy5qcy1jb250YWN0LWZvcm0gLmNvbnRhY3QtZm9ybV9fdGVybXMgJykuY3NzKCdkaXNwbGF5JywgJ2lubGluZS1ibG9jaycpXHJcbiAgICAkKCcuanMtY29udGFjdC1mb3JtIC5saW5rLS1wcml2YWN5JykuY3NzKCdkaXNwbGF5JywgJ2lubGluZS1ibG9jaycpO1xyXG4gIH1cclxuJCgnLmpzLWNvbnRhY3QtaW5mby1yYWRpby10ZXh0JykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICQoJy5qcy1jb250YWN0LWluZm8tcmFkaW8tdGV4dCcpLnJlbW92ZUNsYXNzKCdjb250YWN0LWluZm9fX3RleHQtLXJlZCcpXHJcbiAgJCh0aGlzKS5hZGRDbGFzcygnY29udGFjdC1pbmZvX190ZXh0LS1yZWQnKVxyXG59KVxyXG59KTtcclxuJChmdW5jdGlvbigpIHtcclxuXHJcbiAgJCgnLmpzLXVwLWJ1dHRvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKGUpIHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtzY3JvbGxUb3A6IDB9LCA3MDApO1xyXG4gIH0pO1xyXG5cclxufSlcclxuXHJcbiQoZnVuY3Rpb24oKSB7XHJcblxyXG4gIHZhciAkY2FyZWVyRm9ybSA9ICQoJy5qcy1jYXJlZXItZm9ybScpLCBcclxuICAgICAgJGVycm9yQ29udGFpbmVyID0gJCgnLmZvcm0tZXJyb3ItY29udGFpbmVyJyksXHJcbiAgICAgICRmaWxlSW5wdXQgPSAkKCcuanMtZmlsZS1pbnB1dCcpLFxyXG4gICAgICAkZmlsZUVycm9yID0gJCgnLmpzLWZpbGUtZXJyb3InKSxcclxuICAgICAgJGhpZGRlbkZpZWxkID0gJCgnLmpzLWlucHV0LXBlcnNvbicpO1xyXG4gICAgICBcclxuXHRcclxuICAkKCdmb3JtJykub24oJ2Zvcm1TZW5kJywgZnVuY3Rpb24oKSB7XHJcblx0ICBcclxuXHQgIFxyXG4gIH0pXHJcblxyXG4gICRjYXJlZXJGb3JtLnZhbGlkYXRlKHtcclxuICAgIGVycm9yTGFiZWxDb250YWluZXI6ICRlcnJvckNvbnRhaW5lcixcclxuICAgIHN1Ym1pdEhhbmRsZXI6IGZ1bmN0aW9uKCkge1xyXG4gICAgICBcclxuICAgICAgaWYgKCRoaWRkZW5GaWVsZC52YWwoKSA9PT0gJycpIHtcclxuXHRcdCRjYXJlZXJGb3JtLnRyaWdnZXIoJ2Zvcm1TZW5kJyk7XHJcbiAgICAgICAgLy8kY2FyZWVyRm9ybS50cmlnZ2VyKCdmb3JtU3VibWl0Jyk7XHJcbiAgICAgIH0gZWxzZSByZXR1cm47XHJcbiAgICAgXHJcbiAgICB9LFxyXG4gICAgaW52YWxpZEhhbmRsZXI6IGZ1bmN0aW9uKGV2ZW50LCB2YWxpZGF0b3IpIHtcclxuXHJcbiAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgICAkKCcuZXJyb3InKS5zaWJsaW5ncygnLmNvbnRhY3QtZm9ybV9fZmlsbC1saW5lJykuYWRkQ2xhc3MoJ2Vycm9yJyk7XHJcbiAgICAgIH0sMTAwKVxyXG4gICAgICBcclxuICAgIH0sXHJcbiAgICBvbmtleXVwOiBmdW5jdGlvbihlbGVtZW50KSB7XHJcbiAgICAgICQoZWxlbWVudCkudmFsaWQoKTsgXHJcbiAgICB9LFxyXG5cclxuICB9KSBcclxuXHJcbiAgJGZpbGVJbnB1dC5vbignY2hhbmdlJywgZnVuY3Rpb24oZSkge1xyXG4gICAgdmFyICRmaWxlTGFiZWwgPSAkKCcuanMtZmlsZS1uYW1lJyksXHJcbiAgICAgICAgbGFiZWxWYWwgPSAkZmlsZUxhYmVsLmh0bWwoKSxcclxuICAgICAgICBmaWxlTmFtZSA9ICcnO1xyXG4gICAgICAgIGVycm9yID0gZmFsc2U7XHJcbiAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdlcnJvcicpO1xyXG5cclxuICAgICQoJy5qcy1maWxlLWVycm9yJykuY3NzKCdkaXNwbGF5JywgJ25vbmUnKTtcclxuICAgIHZhciBmb3JtYXQgPSBbJ2RvYycsICdkb2N4JyAsJ3BkZicsJ2pwZycsJ3BuZycsJ3hscycsJ3hsc3gnLCd0eHQnXTtcclxuICAgIHZhciBwYXJ0cyA9IHRoaXMudmFsdWUuc3BsaXQoJy4nKTtcclxuICAgIHBhcnRzID0gcGFydHMucG9wKCk7XHJcblxyXG4gICAgZm9yKHZhciBpID0gMDsgaSA8IGZvcm1hdC5sZW5ndGg7IGkrKyl7XHJcbiAgICAgIGlmKHBhcnRzID09ICBmb3JtYXRbaV0pe1xyXG4gICAgICAgIGVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIH1cclxuICAgICAgZWxzZXtcclxuICAgICAgICBlcnJvciA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmKGVycm9yID09IHRydWUpe1xyXG4gICAgICB0aGlzLnZhbHVlID0gXCJcIjtcclxuICAgICAgJGZpbGVMYWJlbC5odG1sKCfQn9GA0LjQutGA0LXQv9C40YLRjCDRgNC10LfRjtC80LUnKTtcclxuICAgICAgJCgnLmpzLWZpbGUtZXJyb3InKS5jc3MoJ2Rpc3BsYXknLCAnYmxvY2snKTtcclxuICAgIH1cclxuICAgIGVsc2V7XHJcbiAgICBpZih0aGlzLmZpbGVzICYmIHRoaXMuZmlsZXMubGVuZ3RoID4gMSlcclxuXHRcdFx0ZmlsZU5hbWUgPSAodGhpcy5nZXRBdHRyaWJ1dGUoICdkYXRhLW11bHRpcGxlLWNhcHRpb24nICkgfHwgJycpLnJlcGxhY2UoJ3tjb3VudH0nLCB0aGlzLmZpbGVzLmxlbmd0aCk7XHJcblx0XHRlbHNlIGlmKCBlLnRhcmdldC52YWx1ZSApXHJcblx0XHRcdGZpbGVOYW1lID0gZS50YXJnZXQudmFsdWUuc3BsaXQoJ1xcXFwnKS5wb3AoKTtcclxuXHRcdGlmKGZpbGVOYW1lKVxyXG5cdFx0XHQkZmlsZUxhYmVsLmh0bWwoZmlsZU5hbWUpO1xyXG5cdFx0ZWxzZVxyXG5cdFx0XHQkZmlsZUxhYmVsLmh0bWwobGFiZWxWYWwpO1xyXG4gICAgfVxyXG4gIH0pXHJcblxyXG4gICQoJy5qcy1jYXJlZXItY29udGFjdCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgJCgnLmpzLWZvcm0taG9sZGVyJykuYWRkQ2xhc3MoJ2lzLWFjdGl2ZScpO1xyXG4gICAgICAkKCdodG1sLCBib2R5JykuY3NzKHsnb3ZlcmZsb3cnOiAnaGlkZGVuJ30pO1xyXG4gIH0pXHJcblxyXG4gIC8vIERPIE5PVCBERUxFVEUhXHJcblxyXG4gIC8vIFN0aWNrIGJsb2NrIHdpdGggSEggbGlua1xyXG4gICQoJy5qcy1jb250YWN0cy1zdGlja3knKS5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgLy8gJCgnLmpzLWZvcm0taG9sZGVyJykuYWRkQ2xhc3MoJ2lzLWFjdGl2ZScpO1xyXG4gICAgJCh0aGlzKS5zdGlja19pbl9wYXJlbnQoKTtcclxuICB9KTtcclxuXHJcbiAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBmdW5jdGlvbigpIHtcclxuICAgICQoZG9jdW1lbnQuYm9keSkudHJpZ2dlcihcInN0aWNreV9raXQ6cmVjYWxjXCIpO1xyXG4gIH0pXHJcbiAgXHJcbn0pXHJcblxyXG5cclxuJChmdW5jdGlvbigpIHtcclxuICBcclxuICAkKFwiLmpzLXNjaG9vbC1zd2lwZXJcIikuZWFjaChmdW5jdGlvbiAoaW5kZXgsIGVsZW0pIHtcclxuXHJcbiAgICB2YXIgc2Nob29sU3dpcGVyT3B0aW9uID0ge1xyXG4gICAgICBzcGVlZDogNDAwLFxyXG4gICAgICBzcGFjZUJldHdlZW46IDEwLFxyXG4gICAgICBzbGlkZXNQZXJWaWV3OiAxLFxyXG4gICAgICBzY3JvbGxiYXI6IHtcclxuICAgICAgICBlbDogJChlbGVtKS5wYXJlbnQoKS5uZXh0KFwiLmpzLXNjaG9vbC1zY3JvbGxiYXJcIiksXHJcbiAgICAgIH0sXHJcbiAgICB9XHJcbiAgXHJcbiAgICB2YXIgc2Nob29sU3dpcGVyQWN0aXZlID0gZmFsc2U7XHJcbiAgICB2YXIgc2Nob29sU3dpcGVyO1xyXG4gIFxyXG4gICAgaWYgKCQod2luZG93KS5pbm5lcldpZHRoKCkgPCA3NjgpIHtcclxuICAgICAgc2Nob29sU3dpcGVyID0gbmV3IFN3aXBlcihlbGVtLCBzY2hvb2xTd2lwZXJPcHRpb24pO1xyXG4gICAgICBzY2hvb2xTd2lwZXJBY3RpdmUgPSB0cnVlO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgXHJcbiAgICAgIGlmICgkKHRoaXMpLmlubmVyV2lkdGgoKSA+NzY3ICYmIHNjaG9vbFN3aXBlckFjdGl2ZSkge1xyXG4gICAgICAgIHNjaG9vbFN3aXBlci5kZXN0cm95KDEsIDEpO1xyXG4gICAgICAgIHNjaG9vbFN3aXBlckFjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICB9IGVsc2UgaWYoJCh0aGlzKS5pbm5lcldpZHRoKCkgPCA3NjggJiYgc2Nob29sU3dpcGVyQWN0aXZlID09PSBmYWxzZSkge1xyXG4gICAgICAgIHNjaG9vbFN3aXBlciA9IG5ldyBTd2lwZXIoZWxlbSwgc2Nob29sU3dpcGVyT3B0aW9uKTtcclxuICAgICAgICBzY2hvb2xTd2lwZXJBY3RpdmUgPSB0cnVlO1xyXG4gICAgICB9IFxyXG4gIFxyXG4gICAgfSlcclxuXHJcbiAgfSlcclxuXHJcbiAgXHJcblxyXG5cclxufSlcclxuXHJcblxyXG4kKGZ1bmN0aW9uKCkge1xyXG5cclxuICBpZiAoJCgnLmpzLWxpZmUtc3dpcGVyJykubGVuZ3RoKSB7XHJcbiAgICB2YXIgbGlmZVN3aXBlck9wdGlvbiA9IHtcclxuICAgICAgc3BlZWQ6IDQwMCxcclxuICAgICAgc3BhY2VCZXR3ZWVuOiAxMCxcclxuICAgICAgc2xpZGVzUGVyVmlldzogMSxcclxuICAgICAgc2Nyb2xsYmFyOiB7XHJcbiAgICAgICAgZWw6ICQoJy5qcy1saWZlLXNjcm9sbGJhcicpLFxyXG4gICAgICB9LFxyXG4gICAgfVxyXG4gIFxyXG4gICAgdmFyIGxpZmVTd2lwZXJBY3RpdmUgPSBmYWxzZTtcclxuICAgIHZhciBsaWZlU3dpcGVyO1xyXG4gIFxyXG4gICAgaWYgKCQod2luZG93KS5pbm5lcldpZHRoKCkgPCA3NjgpIHtcclxuICAgICAgbGlmZVN3aXBlciA9IG5ldyBTd2lwZXIoJy5qcy1saWZlLXN3aXBlcicsIGxpZmVTd2lwZXJPcHRpb24pO1xyXG4gICAgICBsaWZlU3dpcGVyQWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxuICBcclxuICAgICQod2luZG93KS5vbigncmVzaXplJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgIFxyXG4gICAgICBpZiAoJCh0aGlzKS5pbm5lcldpZHRoKCkgPjc2NyAmJiBsaWZlU3dpcGVyQWN0aXZlKSB7XHJcbiAgICAgICAgbGlmZVN3aXBlci5kZXN0cm95KDEsIDEpO1xyXG4gICAgICAgIGxpZmVTd2lwZXJBY3RpdmUgPSBmYWxzZTtcclxuICAgICAgfSBlbHNlIGlmKCQodGhpcykuaW5uZXJXaWR0aCgpIDwgNzY4ICYmIGxpZmVTd2lwZXJBY3RpdmUgPT09IGZhbHNlKSB7XHJcbiAgICAgICAgbGlmZVN3aXBlciA9IG5ldyBTd2lwZXIoJy5qcy1saWZlLXN3aXBlcicsIGxpZmVTd2lwZXJPcHRpb24pO1xyXG4gICAgICAgIGxpZmVTd2lwZXJBY3RpdmUgPSB0cnVlO1xyXG4gICAgICB9IFxyXG4gIFxyXG4gICAgfSlcclxuICB9XHJcbiAgXHJcbiAgXHJcblxyXG5cclxufSlcclxuXHJcblxyXG4kKGZ1bmN0aW9uICgpIHtcclxuICAgIGZ1bmN0aW9uIGxvYWRCYXJJbml0KCkge1xyXG5cclxuICAgICAgICB2YXIgbG9hZEJhciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5sZEJhcicpO1xyXG5cclxuICAgICAgICBpZiAobG9hZEJhciA9PT0gbnVsbCkgcmV0dXJuO1xyXG5cclxuICAgICAgICBsb2FkQmFyLmxkQmFyO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBsb2FkQmFySW5pdCgpO1xyXG5cclxuXHJcbn0pXHJcblxyXG5cclxuLypcclxuXHJcbiBcclxuIHZhciBiYXIxID0gbmV3IGxkQmFyKFwiI215SXRlbTFcIik7XHJcblxyXG4gdmFyIGJhcjIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbXlJdGVtMScpLmxkQmFyO1xyXG4gYmFyMS5zZXQoNjApO1xyXG5cclxuXHJcbiovXHJcblxyXG5mdW5jdGlvbiBnZXRDb29raWUobmFtZSkge1xyXG4gIHZhciBtYXRjaGVzID0gZG9jdW1lbnQuY29va2llLm1hdGNoKG5ldyBSZWdFeHAoXHJcbiAgICBcIig/Ol58OyApXCIgKyBuYW1lLnJlcGxhY2UoLyhbXFwuJD8qfHt9XFwoXFwpXFxbXFxdXFxcXFxcL1xcK15dKS9nLCAnXFxcXCQxJykgKyBcIj0oW147XSopXCJcclxuICApKTtcclxuICByZXR1cm4gbWF0Y2hlcyA/IGRlY29kZVVSSUNvbXBvbmVudChtYXRjaGVzWzFdKSA6IHVuZGVmaW5lZDtcclxufVxyXG5cclxuZnVuY3Rpb24gc2V0Q29va2llKG5hbWUsIHZhbHVlLCBvcHRpb25zKSB7XHJcblxyXG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHtcclxuICAgIHBhdGg6ICcvJyxcclxuICAgICdtYXgtYWdlJzogNjAqNjAqMjQqMzBcclxuICB9XHJcblxyXG4gIGlmIChvcHRpb25zLmV4cGlyZXMgaW5zdGFuY2VvZiBEYXRlKSB7XHJcbiAgICBvcHRpb25zLmV4cGlyZXMgPSBvcHRpb25zLmV4cGlyZXMudG9VVENTdHJpbmcoKTtcclxuICB9XHJcblxyXG4gIHZhciB1cGRhdGVkQ29va2llID0gZW5jb2RlVVJJQ29tcG9uZW50KG5hbWUpICsgXCI9XCIgKyBlbmNvZGVVUklDb21wb25lbnQodmFsdWUpO1xyXG5cclxuICBmb3IgKHZhciBvcHRpb25LZXkgaW4gb3B0aW9ucykge1xyXG4gICAgdXBkYXRlZENvb2tpZSArPSBcIjsgXCIgKyBvcHRpb25LZXk7XHJcbiAgICB2YXIgb3B0aW9uVmFsdWUgPSBvcHRpb25zW29wdGlvbktleV07XHJcbiAgICBpZiAob3B0aW9uVmFsdWUgIT09IHRydWUpIHtcclxuICAgICAgdXBkYXRlZENvb2tpZSArPSBcIj1cIiArIG9wdGlvblZhbHVlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZG9jdW1lbnQuY29va2llID0gdXBkYXRlZENvb2tpZTtcclxufVxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgaWYoIWdldENvb2tpZSgnaGlkZUNvb2tpZScpKSB7XHJcbiAgICAkKCcuanMtY29va2llJykuc2hvdygpO1xyXG4gIH1cclxuICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmpzLWNvb2tpZS1idXR0b24nLCBmdW5jdGlvbigpIHtcclxuICAgICQoJy5qcy1jb29raWUnKS5mYWRlT3V0KCk7XHJcbiAgICBzZXRDb29raWUoJ2hpZGVDb29raWUnLCAndHJ1ZScpO1xyXG4gIH0pO1xyXG59KTtcclxuZnVuY3Rpb24gZ2V0U2Nyb2xsYmFyV2lkdGgoKSB7XHJcbiAgcmV0dXJuIHNjcm9sbEJhcldpZHRoID0gd2luZG93LmlubmVyV2lkdGggLSAkKHdpbmRvdykud2lkdGgoKTtcclxufVxyXG5cclxuJChmdW5jdGlvbigpIHtcclxuXHJcbiAgdmFyIG1lbnVPcGVuID0gZmFsc2U7XHJcblxyXG4gICQoJy5qcy1idXJnZXInKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgIFxyXG4gICAgbmF2TWVudVRvZ2dsZSgpO1xyXG4gICAgXHJcbiAgfSlcclxuICB2YXIgc1VzckFnID0gbmF2aWdhdG9yLnVzZXJBZ2VudDtcclxuXHJcbiAgIGlmIChzVXNyQWcuaW5kZXhPZihcIlNhZmFyaVwiKSA+IC0xKSB7XHJcbiAgICAkKCcubmF2LW1lbnVfX21lbnUnKS5yZW1vdmVDbGFzcygnbWVudS10b3AtLWFuaW1hdGUtZGVsYXknKVxyXG4gIH0gZWxzZSB7XHJcbiAgICAkKCcubmF2LW1lbnVfX21lbnUnKS5hZGRDbGFzcygnbWVudS10b3AtLWFuaW1hdGUtZGVsYXknKVxyXG4gIH1cclxuICAkKCcuanMtbWVudS1hbmNob3InKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgICQodGhpcykub24oJ2NsaWNrJywgZnVuY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICB2YXIgcmUgPS8jKFtcXHcrLi1dKykvO1xyXG4gICAgICAgIHZhciBsaW5rID0gJCh0aGlzKS5hdHRyKCdocmVmJykubWF0Y2gocmUpWzBdO1xyXG5cclxuICAgICAgICB2YXIgc2Nyb2xsZWQgPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcbiAgICAgICAgdmFyIG9mZnNldFRvcCA9ICQobGluaykub2Zmc2V0KCkudG9wO1xyXG5cclxuICAgICAgICBpZihzY3JvbGxlZCA+IG9mZnNldFRvcCkge1xyXG4gICAgICAgICAgb2Zmc2V0VG9wIC09ICQoJy5oZWFkZXInKS5pbm5lckhlaWdodCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe3Njcm9sbFRvcDpvZmZzZXRUb3B9LDYwMClcclxuICAgICAgY2xvc2VNZW51KClcclxuICAgIH0pO1xyXG4gIH0pXHJcbiAgdmFyICRtZW51ID0gJCgnLmpzLW5hdi1tZW51Jyk7XHJcblxyXG4gIGZ1bmN0aW9uIG5hdk1lbnVUb2dnbGUoKSB7XHJcblxyXG4gICAgICB2YXIgc2Nyb2xsYmFyV2lkdGggPSBnZXRTY3JvbGxiYXJXaWR0aCgpO1xyXG5cclxuICAgICAgaWYgKCFtZW51T3Blbikge1xyXG4gICAgICAgICRtZW51LmFkZENsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICAgICAkKCcuanMtYnVyZ2VyJykuYWRkQ2xhc3MoJ2lzLWFjdGl2ZScpO1xyXG4gICAgICAgICQoJy5qcy1tZW51LXRvb2xzJykuY3NzKHsncGFkZGluZy1yaWdodCcgOiBzY3JvbGxCYXJXaWR0aH0pO1xyXG4gICAgICAgICQoJy5qcy1oZWFkZXInKS5jc3MoeydwYWRkaW5nLXJpZ2h0JyA6IHNjcm9sbEJhcldpZHRofSk7XHJcbiAgICAgICAgJCgnYm9keScpLmNzcyh7J292ZXJmbG93JyA6ICdoaWRkZW4nLCAncGFkZGluZy1yaWdodCcgOiBzY3JvbGxCYXJXaWR0aH0pO1xyXG4gICAgICAgICQoJy5uYXYtbWVudV9fbWVudScpLmNzcyh7J3BhZGRpbmctcmlnaHQnIDogc2Nyb2xsQmFyV2lkdGh9KTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAkKCcuanMtbWFpbi1jb250ZW50JykuYWRkQ2xhc3MoJ2JnLS1ibHVyJyk7XHJcbiAgICAgICAgICAkKCcuanMtaGVhZGVyJykuYWRkQ2xhc3MoJ2lzLWJsdXInKTtcclxuICAgICAgICAgICQoJy5qcy1jb29raWUnKS5jc3Moe1xyXG4gICAgICAgICAgICAncG9zaXRpb24nOiAnYWJzb2x1dGUnLFxyXG4gICAgICAgICAgICAnYm90dG9tJzogKGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5vZmZzZXRIZWlnaHQgLSB3aW5kb3cucGFnZVlPZmZzZXQgLSB3aW5kb3cuaW5uZXJIZWlnaHQpICsgJ3B4J1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSw1MCk7XHJcbiAgICAgICAgbWVudU9wZW4gPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNsb3NlTWVudSgpO1xyXG4gICAgICB9XHJcbiAgfVxyXG5cclxuICAkKCcuanMtbWVudS1pbm5lcicpLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcclxuICAgIHZhciBtZW51cFBzID0gbmV3IFBlcmZlY3RTY3JvbGxiYXIoZWxlbWVudCwge1xyXG4gICAgICB3aGVlbFNwZWVkOiAxLFxyXG4gICAgICBtaW5TY3JvbGxiYXJMZW5ndGg6IDEwLFxyXG4gICAgICBtYXhTY3JvbGxiYXJMZW5ndGggOiA3MCxcclxuICAgICAgc3VwcHJlc3NTY3JvbGxYOiB0cnVlXHJcbiAgICB9KTtcclxuICBcclxuICAgIG1lbnVwUHMudXBkYXRlKCk7XHJcbiAgICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIGZ1bmN0aW9uKCkge1xyXG4gICAgICBtZW51cFBzLnVwZGF0ZSgpO1xyXG4gICAgfSk7XHJcblxyXG4gICAgZWxlbWVudC5zY3JvbGxUb3AgKz0gMTtcclxuICAgIGVsZW1lbnQuc2Nyb2xsVG9wIC09IDE7XHJcbiAgfSk7XHJcbiAgJCgnLmpzLW1haW4tY29udGVudCwgLmpzLWhlYWRlcicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgY2xvc2VNZW51KCk7XHJcbiAgfSlcclxuXHJcbiAgZnVuY3Rpb24gY2xvc2VNZW51KCkge1xyXG4gICAgJG1lbnUucmVtb3ZlQ2xhc3MoJ2lzLWFjdGl2ZScpO1xyXG4gICAgJCgnLmpzLWJ1cmdlcicpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICQoJy5qcy1oZWFkZXInKS5jc3MoeydwYWRkaW5nLXJpZ2h0JyA6IDB9KTtcclxuICAgICQoJy5qcy1tZW51LXRvb2xzJykuY3NzKHsncGFkZGluZy1yaWdodCcgOiAwfSk7XHJcblxyXG4gICAgaWYoJCgnLnJldmlld3MtcG9wdXAnKS5oYXNDbGFzcygnaXMtc2hvdycpKXtcclxuICAgICAgJCgnYm9keScpLmF0dHIoJ3N0eWxlJyAsICdvdmVyZmxvdzogaGlkZGVuJyk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgJCgnYm9keScpLmNzcyh7J292ZXJmbG93JyA6ICdhdXRvJywgJ3BhZGRpbmctcmlnaHQnIDogMH0pO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAkKCcuanMtbWFpbi1jb250ZW50JykucmVtb3ZlQ2xhc3MoJ2JnLS1ibHVyJyk7XHJcbiAgICAkKCcuanMtaGVhZGVyJykucmVtb3ZlQ2xhc3MoJ2lzLWJsdXInKTtcclxuICAgICQoJy5qcy1jb29raWUnKS5jc3Moe1xyXG4gICAgICAncG9zaXRpb24nOiAnJyxcclxuICAgICAgJ2JvdHRvbSc6ICcnXHJcbiAgICB9KTtcclxuICAgICQoJy5uYXYtbWVudV9fbWVudScpLmNzcyh7J3BhZGRpbmctcmlnaHQnIDogJyd9KTtcclxuXHJcbiAgICBtZW51T3BlbiA9IGZhbHNlO1xyXG5cclxuICB9XHJcblxyXG4gICQoJy5qcy1zd2l0Y2gtdGhlbWUnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICQoJ2h0bWwnKS50b2dnbGVDbGFzcygnaXMtZGFyaycpO1xyXG4gICAgdmFyIGN1cnJlbnRNb2RlID0gICQoJ2h0bWwnKS5hdHRyKCdjbGFzcycpO1xyXG4gICAgZG9jdW1lbnQuY29va2llID0gJ2N1cnJlbnRUaGVtZT0nICsgY3VycmVudE1vZGUgKyAnIG1heC1hZ2U9MjU5MjAwMDsgcGF0aD0vOyc7XHJcbiAgfSlcclxuXHJcbiAgZnVuY3Rpb24gaXNEYXJrVGhlbWVTZWxlY3RlZCgpIHtcclxuICAgIHJldHVybiBkb2N1bWVudC5jb29raWUubWF0Y2goL2N1cnJlbnRUaGVtZT1pcy1kYXJrL2kpICE9IG51bGxcclxuICB9XHJcblxyXG4gIGZ1bmN0aW9uIHNldFRoZW1lRnJvbUNvb2tpZSgpIHtcclxuICAgIHZhciByb290ID0gJCgnaHRtbCcpO1xyXG5cclxuICAgIGlmIChpc0RhcmtUaGVtZVNlbGVjdGVkKCkpIHtcclxuICAgICAgcm9vdC5hZGRDbGFzcygnaXMtZGFyaycpXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByb290LnJlbW92ZUNsYXNzKCdpcy1kYXJrJylcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBzZXRUaGVtZUZyb21Db29raWUoKVxyXG5cclxuICAkKCcuanMtbGluay1jb250YWN0cycpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyICR0YXJnZXRFbGVtZW50ID0gJCgnI2NvbnRhY3RzJyk7XHJcbiAgICBpZiAoJHRhcmdldEVsZW1lbnQubGVuZ3RoKSB7XHJcbiAgICAgIGNsb3NlTWVudSgpO1xyXG4gICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICB2YXIgcmUgPS8jKFtcXHcrLi1dKykvO1xyXG4gICAgICB2YXIgbGluayA9ICQodGhpcykuYXR0cignaHJlZicpLm1hdGNoKHJlKVswXTtcclxuXHJcbiAgICAgIHZhciBzY3JvbGxlZCA9IHdpbmRvdy5wYWdlWU9mZnNldDtcclxuICAgICAgdmFyIG9mZnNldFRvcCA9ICQobGluaykub2Zmc2V0KCkudG9wO1xyXG5cclxuICAgICAgaWYoc2Nyb2xsZWQgPiBvZmZzZXRUb3ApIHtcclxuICAgICAgICBvZmZzZXRUb3AgLT0gJCgnLmhlYWRlcicpLmlubmVySGVpZ2h0KCk7XHJcbiAgICAgIH1cclxuICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe3Njcm9sbFRvcDpvZmZzZXRUb3B9LDYwMClcclxuICAgIH1cclxuICB9KVxyXG4gICQoJy5qcy1saW5rLXNlcnZpY2VzJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgJHRhcmdldEVsZW1lbnQgPSAkKCcjc2VydmljZXMnKTtcclxuICAgIGlmICgkdGFyZ2V0RWxlbWVudC5sZW5ndGgpIHtcclxuICAgICAgY2xvc2VNZW51KCk7XHJcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIHZhciByZSA9LyMoW1xcdysuLV0rKS87XHJcbiAgICAgIHZhciBsaW5rID0gJCh0aGlzKS5hdHRyKCdocmVmJykubWF0Y2gocmUpWzBdO1xyXG4gICAgICBcclxuICAgICAgdmFyIHNjcm9sbGVkID0gd2luZG93LnBhZ2VZT2Zmc2V0O1xyXG4gICAgICB2YXIgb2Zmc2V0VG9wID0gJChsaW5rKS5vZmZzZXQoKS50b3A7XHJcblxyXG4gICAgICBpZihzY3JvbGxlZCA+IG9mZnNldFRvcCkge1xyXG4gICAgICAgIG9mZnNldFRvcCAtPSAkKCcuaGVhZGVyJykuaW5uZXJIZWlnaHQoKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgJCgnaHRtbCwgYm9keScpLmFuaW1hdGUoe3Njcm9sbFRvcDpvZmZzZXRUb3B9LDYwMClcclxuICAgIH1cclxuICB9KVxyXG5cclxuICBpZihsb2NhdGlvbi5oYXNoID09PSAnI3NlcnZpY2VzJyB8fCBsb2NhdGlvbi5oYXNoID09PSAnI2NvbnRhY3RzJykge1xyXG4gICAgJCh3aW5kb3cpLnRyaWdnZXIoJ3Njcm9sbCcsIHRydWUpO1xyXG4gIH1cclxufSlcclxuJChmdW5jdGlvbigpIHtcclxuICAgIHZhciB0aW1lbGluZSA9ICQoJy5jb3JwLXBvcnRhbHMtc3RlcHNfX3RpbWVsaW5lJyk7XHJcblxyXG4gICAgaWYgKHRpbWVsaW5lLmxlbmd0aCkge1xyXG4gICAgICAgIHZhciBsaXN0U3RlcHMgPSAkKCcuY29ycC1wb3J0YWxzLXN0ZXBzX19saXN0Jyk7XHJcbiAgICAgICAgdmFyIGl0ZW1zU3RlcHMgPSAkKCcuY29ycC1wb3J0YWxzLXN0ZXBzX19pdGVtJyk7XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGNhbGNIZWlnaHRUaW1lbGluZSgpIHtcclxuICAgICAgICAgICAgdmFyIGhlaWdodExpc3RTdGVwcyA9IGxpc3RTdGVwcy5vdXRlckhlaWdodCgpO1xyXG4gICAgICAgICAgICB2YXIgaGFsZkhlaWdodEZpcnN0U3RlcCA9ICQoaXRlbXNTdGVwc1swXSkub3V0ZXJIZWlnaHQoKSAvIDI7XHJcbiAgICAgICAgICAgIHZhciBoYWxmSGVpZ2h0TGFzdFN0ZXAgPSAkKGl0ZW1zU3RlcHNbaXRlbXNTdGVwcy5sZW5ndGggLSAxXSkub3V0ZXJIZWlnaHQoKSAvIDI7XHJcbiAgICBcclxuICAgICAgICAgICAgJCh0aW1lbGluZSkuY3NzKHtcclxuICAgICAgICAgICAgICAgIHRvcDogaGFsZkhlaWdodEZpcnN0U3RlcCxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogaGVpZ2h0TGlzdFN0ZXBzIC0gKGhhbGZIZWlnaHRGaXJzdFN0ZXAgKyBoYWxmSGVpZ2h0TGFzdFN0ZXApLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIGFuaW1hdGVJdGVtcygpIHtcclxuICAgICAgICAgICAgdmFyIHNjcm9sbCA9ICQod2luZG93KS5zY3JvbGxUb3AoKSArICQod2luZG93KS5oZWlnaHQoKTtcclxuICAgICAgICAgICAgdmFyIGlzQW5pbWF0ZWRJdGVtID0gMDtcclxuICAgIFxyXG4gICAgICAgICAgICAkKGl0ZW1zU3RlcHMpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICBpc0FuaW1hdGVkSXRlbSA9IDA7XHJcbiAgICAgICAgICAgICAgICB2YXIgaGVpZ2h0SXRlbSA9ICQodGhpcykub3V0ZXJIZWlnaHQoKTtcclxuICAgIFxyXG4gICAgICAgICAgICAgICAgaWYgKHNjcm9sbCAtIGhlaWdodEl0ZW0gLSBoZWlnaHRJdGVtIC8gMiA+ICQodGhpcykub2Zmc2V0KCkudG9wICYmIGlzQW5pbWF0ZWRJdGVtID09IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdmYWRlLWluJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNBbmltYXRlZEl0ZW0gPSAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHZhciB0aW1lbGluZUZpbGwgPSAkKCcuY29ycC1wb3J0YWxzLXN0ZXBzX190aW1lbGluZS1maWxsJyk7XHJcbiAgICAgICAgdmFyIGNpcmNsZXNTdGVwcyA9ICQoJy5jb3JwLXBvcnRhbHMtc3RlcHNfX2NpcmNsZScpO1xyXG5cclxuICAgICAgICBmdW5jdGlvbiBhbmltYXRlVGltZWxpbmUoKSB7XHJcbiAgICAgICAgICAgIHZhciBoZWlnaHRMaXN0U3RlcHMgPSAkKGxpc3RTdGVwcykub3V0ZXJIZWlnaHQoKTtcclxuICAgICAgICAgICAgdmFyIGNvbnRyb2xQb2ludCA9ICQod2luZG93KS5zY3JvbGxUb3AoKSArICQod2luZG93KS5oZWlnaHQoKSAvIDI7XHJcbiAgICAgICAgICAgIHZhciB0b3BUaW1lbGluZUZpbGwgPSB0aW1lbGluZUZpbGwub2Zmc2V0KCkudG9wO1xyXG4gICAgICAgICAgICB2YXIgaGVpZ2h0VGltZWxpbmVGaWxsID0gY29udHJvbFBvaW50IC0gdG9wVGltZWxpbmVGaWxsO1xyXG5cclxuICAgICAgICAgICAgaWYgKCQod2luZG93KS5zY3JvbGxUb3AoKSArICQod2luZG93KS5oZWlnaHQoKSA+IHRvcFRpbWVsaW5lRmlsbCAmJiBoZWlnaHRUaW1lbGluZUZpbGwgPCBoZWlnaHRMaXN0U3RlcHMpIHtcclxuICAgICAgICAgICAgICAgICQodGltZWxpbmVGaWxsKS5jc3Moe2hlaWdodDogaGVpZ2h0VGltZWxpbmVGaWxsfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgJChjaXJjbGVzU3RlcHMpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKCQodGhpcykub2Zmc2V0KCkudG9wIDw9IGNvbnRyb2xQb2ludCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLmFkZENsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKHRoaXMpLnJlbW92ZUNsYXNzKCdpcy1hY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgICBjYWxjSGVpZ2h0VGltZWxpbmUoKTtcclxuICAgICAgICBhbmltYXRlSXRlbXMoKTtcclxuICAgICAgICBhbmltYXRlVGltZWxpbmUoKTtcclxuXHJcbiAgICAgICAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgY2FsY0hlaWdodFRpbWVsaW5lKCk7XHJcbiAgICAgICAgICAgIGFuaW1hdGVJdGVtcygpO1xyXG4gICAgICAgICAgICBhbmltYXRlVGltZWxpbmUoKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJCh3aW5kb3cpLm9uKCdzY3JvbGwnICwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGFuaW1hdGVJdGVtcygpO1xyXG4gICAgICAgICAgICBhbmltYXRlVGltZWxpbmUoKTtcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG59KTtcclxuJChmdW5jdGlvbiAoKSB7XHJcbiAgdmFyICRjb25zdWx0YXRpb25Gb3JtID0gJCgnLmpzLWNvbnN1bHRhdGlvbi1mb3JtJyksXHJcbiAgICAgICRlcnJvckNvbnRhaW5lciA9ICRjb25zdWx0YXRpb25Gb3JtLmZpbmQoJy5mb3JtLWVycm9yLWNvbnRhaW5lcicpLFxyXG4gICAgICAkaGlkZGVuRmllbGQgPSAkY29uc3VsdGF0aW9uRm9ybS5maW5kKCcuanMtaW5wdXQtcGVyc29uJyk7XHJcblxyXG4gICRjb25zdWx0YXRpb25Gb3JtLnZhbGlkYXRlKHtcclxuICAgIGlnbm9yZTogW10sXHJcbiAgICBlcnJvckxhYmVsQ29udGFpbmVyOiAkZXJyb3JDb250YWluZXIsXHJcbiAgICBzdWJtaXRIYW5kbGVyOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGlmICgkaGlkZGVuRmllbGQudmFsKCkgPT09ICcnKSB7XHJcbiAgICAgICAgdmFyIGRhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuXHJcbiAgICAgICAgJGNvbnN1bHRhdGlvbkZvcm0uZmluZCgnLmpzLWlucHV0JykuZWFjaChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICB2YXIgdmFsdWUgPSAkKHRoaXMpLnZhbCgpO1xyXG5cclxuICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KHZhbHVlKSkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLmpvaW4oJywgJylcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBkYXRhLmFwcGVuZCh0aGlzLm5hbWUsIHZhbHVlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgIHVybDogXCIvc2VuZGZlZWRiYWNrXCIsXHJcbiAgICAgICAgICB0eXBlOiBcIlBPU1RcIixcclxuICAgICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgICBwcm9jZXNzRGF0YTogZmFsc2UsXHJcbiAgICAgICAgICBjb250ZW50VHlwZTogZmFsc2UsXHJcbiAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICRjb25zdWx0YXRpb25Gb3JtLnRyaWdnZXIoJ2Zvcm1TdWJtaXQnKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfSxcclxuICAgIGludmFsaWRIYW5kbGVyOiBmdW5jdGlvbiAoZXZlbnQsIHZhbGlkYXRvcikge1xyXG4gICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKCcuZXJyb3InKS5zaWJsaW5ncygnLmNvbnRhY3QtZm9ybV9fZmlsbC1saW5lJykuYWRkQ2xhc3MoJ2Vycm9yJyk7XHJcbiAgICAgIH0sIDEwMClcclxuICAgIH0sXHJcbiAgICBvbmtleXVwOiBmdW5jdGlvbiAoZWxlbWVudCkge1xyXG4gICAgICAkKGVsZW1lbnQpLnZhbGlkKCk7XHJcbiAgICB9LFxyXG4gIH0pXHJcbn0pXHJcbiQoZnVuY3Rpb24oKSB7XHJcbiAgXHJcbiAgaWYgKCQoJy5qcy1jb3JwLXBvcnRhbHMtcGFydG5lcnMtc3dpcGVyJykubGVuZ3RoKSB7XHJcbiAgICB2YXIgcGFydG5lcnNTd2lwZXJPcHRpb24gPSB7XHJcbiAgICAgIHNwZWVkOiA0MDAsXHJcbiAgICAgIHNwYWNlQmV0d2VlbjogODAsXHJcbiAgICAgIHNsaWRlc1BlclZpZXc6IDEuNSxcclxuICAgICAgc2xpZGVzUGVyQ29sdW1uOiAyLFxyXG4gICAgICBzbGlkZXNQZXJDb2x1bW5GaWxsOiAncm93JyxcclxuICAgICAgLy8gc2xpZGVzUGVyR3JvdXA6IDMsXHJcbiAgICAgIC8vIGNlbnRlcmVkU2xpZGVzOiB0cnVlLFxyXG4gICAgICAvLyBjZW50ZXJlZFNsaWRlc0JvdW5kczogdHJ1ZSxcclxuICAgICAgc2Nyb2xsYmFyOiB7XHJcbiAgICAgICAgZWw6ICcuc3dpcGVyLXNjcm9sbGJhcicsXHJcbiAgICAgIH0sXHJcbiAgICB9XHJcbiAgXHJcbiAgICB2YXIgcGFydG5lcnNTd2lwZXJBY3RpdmUgPSBmYWxzZTtcclxuICAgIHZhciBwYXJ0bmVyc1N3aXBlcjtcclxuICBcclxuICAgIGlmICgkKHdpbmRvdykuaW5uZXJXaWR0aCgpIDwgNzY4KSB7XHJcbiAgICAgIHBhcnRuZXJzU3dpcGVyID0gbmV3IFN3aXBlcignLmpzLWNvcnAtcG9ydGFscy1wYXJ0bmVycy1zd2lwZXInLCBwYXJ0bmVyc1N3aXBlck9wdGlvbik7XHJcbiAgICAgIHBhcnRuZXJzU3dpcGVyQWN0aXZlID0gdHJ1ZTtcclxuICAgIH1cclxuICBcclxuICAgICQod2luZG93KS5vbigncmVzaXplJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgIFxyXG4gICAgICBpZiAoJCh0aGlzKS5pbm5lcldpZHRoKCkgPjc2NyAmJiBwYXJ0bmVyc1N3aXBlckFjdGl2ZSkge1xyXG4gICAgICAgIHBhcnRuZXJzU3dpcGVyLmRlc3Ryb3koMSwgMSk7XHJcbiAgICAgICAgcGFydG5lcnNTd2lwZXJBY3RpdmUgPSBmYWxzZTtcclxuICAgICAgfSBlbHNlIGlmKCQodGhpcykuaW5uZXJXaWR0aCgpIDwgNzY4ICYmIHBhcnRuZXJzU3dpcGVyQWN0aXZlID09PSBmYWxzZSkge1xyXG4gICAgICAgIHBhcnRuZXJzU3dpcGVyID0gbmV3IFN3aXBlcignLmpzLWNvcnAtcG9ydGFscy1wYXJ0bmVycy1zd2lwZXInLCBwYXJ0bmVyc1N3aXBlck9wdGlvbik7XHJcbiAgICAgICAgcGFydG5lcnNTd2lwZXJBY3RpdmUgPSB0cnVlO1xyXG4gICAgICB9IFxyXG4gIFxyXG4gICAgfSlcclxuXHJcbiAgfVxyXG5cclxuICBcclxuXHJcbn0pXHJcblxyXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcclxuXHJcbiAgdmFyIHJldmlld3NTd2lwZXIgPSBuZXcgU3dpcGVyKCcuanMtcmV2aWV3cy1zbGlkZXInLCB7XHJcbiAgICBzcGVlZDogNDAwLFxyXG4gICAgc2xpZGVzUGVyVmlldzogMSAsXHJcbiAgICByb3VuZExlbmd0aHM6IHRydWUsXHJcbiAgICBzcGFjZUJldHdlZW46IDIwLFxyXG4gICAgd2F0Y2hPdmVyZmxvdzogdHJ1ZSxcclxuICAgIG5hdmlnYXRpb246IHtcclxuICAgICAgbmV4dEVsOiAnLmpzLXJldmlld3Mtc2xpZGVyfi5zd2lwZXItYnV0dG9uLW5leHQnLFxyXG4gICAgICBwcmV2RWw6ICcuanMtcmV2aWV3cy1zbGlkZXJ+LnN3aXBlci1idXR0b24tcHJldicsXHJcbiAgICB9LFxyXG4gICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICBlbDogJy5qcy1yZXZpZXdzLXNsaWRlciAuc3dpcGVyLXBhZ2luYXRpb24nLFxyXG4gICAgICBkeW5hbWljQnVsbGV0czogdHJ1ZSxcclxuICAgICAgZHluYW1pY01haW5CdWxsZXRzOiAzLFxyXG4gICAgICBjbGlja2FibGU6dHJ1ZVxyXG4gICAgfSxcclxuICAgIGJyZWFrcG9pbnRzOntcclxuICAgICAgOTAwOiB7XHJcbiAgICAgICAgc2xpZGVzUGVyVmlldzogMixcclxuICAgICAgICBlZmZlY3Q6ICdmYWRlJ1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSk7XHJcblxyXG4gIHZhciBwb3B1cFN3aXBlciA9IG5ldyBTd2lwZXIoJy5qcy1yZXZpZXdzLXBvcHVwLXNsaWRlcicsIHtcclxuICAgIHJvdW5kTGVuZ3RoczogdHJ1ZSxcclxuICAgIHNwYWNlQmV0d2VlbjogNDAsXHJcbiAgICB3YXRjaE92ZXJmbG93OiB0cnVlLFxyXG4gICAgZWZmZWN0OiAnZmFkZScsXHJcbiAgICBuYXZpZ2F0aW9uOiB7XHJcbiAgICAgIG5leHRFbDogJy5qcy1yZXZpZXdzLXBvcHVwLXNsaWRlcn4uc3dpcGVyLWJ1dHRvbi1uZXh0JyxcclxuICAgICAgcHJldkVsOiAnLmpzLXJldmlld3MtcG9wdXAtc2xpZGVyfi5zd2lwZXItYnV0dG9uLXByZXYnLFxyXG4gICAgfSxcclxuICAgIHBhZ2luYXRpb246IHtcclxuICAgICAgZWw6ICcuanMtcmV2aWV3cy1wb3B1cC1zbGlkZXIgLnN3aXBlci1wYWdpbmF0aW9uJyxcclxuICAgICAgZHluYW1pY0J1bGxldHM6IHRydWUsXHJcbiAgICAgIGR5bmFtaWNNYWluQnVsbGV0czogM1xyXG4gICAgfSxcclxuICAgIGFsbG93VG91Y2hNb3ZlOiBmYWxzZSxcclxuICAgIGtleWJvYXJkOiB7XHJcbiAgICAgIGVuYWJsZWQ6IHRydWVcclxuICAgIH1cclxuICB9KTtcclxuXHJcblxyXG4gIHBvcHVwU3dpcGVyLm9uKCdzbGlkZUNoYW5nZScgLCBmdW5jdGlvbigpe1xyXG4gICAgdXBkYXRlUG9wdXBQYWRkaW5nKCk7XHJcbiAgICAkKCcucmV2aWV3cy1wb3B1cCAucmV2aWV3cy1wb3B1cF9fdGV4dCcpLnNjcm9sbFRvcCgwKTtcclxuICAgICQoJy5yZXZpZXdzLXBvcHVwIC5yZXZpZXdzLXBvcHVwX190ZXh0JykudHJpZ2dlcigncHMteS1yZWFjaC1zdGFydCcpO1xyXG4gIH0pOyBcclxuXHJcblxyXG4gICQoJy5yZXZpZXdzLXBvcHVwX190ZXh0JykuZWFjaChmdW5jdGlvbihpbmRleCwgZWxlbWVudCkge1xyXG4gICAgdmFyIHBvcHVwUHMgPSBuZXcgUGVyZmVjdFNjcm9sbGJhcihlbGVtZW50LCB7XHJcbiAgICAgIHdoZWVsU3BlZWQ6IDAuNSxcclxuICAgICAgd2hlZWxQcm9wYWdhdGlvbjogdHJ1ZSxcclxuICAgICAgbWluU2Nyb2xsYmFyTGVuZ3RoOiAxMCxcclxuICAgICAgbWF4U2Nyb2xsYmFyTGVuZ3RoOiAxNVxyXG4gICAgfSk7XHJcbiAgXHJcbiAgICBwb3B1cFBzLnVwZGF0ZSgpO1xyXG4gICAgJCh3aW5kb3cpLm9uKCdyZXNpemUnLCBmdW5jdGlvbigpIHtcclxuICAgICAgcG9wdXBQcy51cGRhdGUoKTtcclxuICAgIH0pO1xyXG4gICAgJChlbGVtZW50KS5vbigncHM6dXBkYXRlJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgIHBvcHVwUHMudXBkYXRlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgfSk7XHJcblxyXG4gICQoZG9jdW1lbnQpLm9uKCdjbGljaycgLCAnLmpzLXNob3ctcG9wdXAtcmV2aWV3cycgLCBmdW5jdGlvbigpe1xyXG5cclxuICAgIHZhciBzbGlkZXJOdW1iZXIgPSAkKHRoaXMpLmNsb3Nlc3QoJy5zd2lwZXItc2xpZGUnKS5hdHRyKCdkYXRhLWlkJyk7XHJcbiAgICBzbGlkZXJOdW1iZXIgPSBwYXJzZUludChzbGlkZXJOdW1iZXIpO1xyXG5cclxuICAgICQoJy5yZXZpZXdzLXBvcHVwJykuYWRkQ2xhc3MoJ2lzLXNob3cnKTtcclxuICAgICQoJ2JvZHknKS5jc3MoeydvdmVyZmxvdyc6J2hpZGRlbid9KVxyXG4gICAgXHJcbiAgICBwb3B1cFN3aXBlci5zbGlkZVRvKHNsaWRlck51bWJlciwgMTAwMCAsIGZhbHNlICk7XHJcbiAgICBwb3B1cFN3aXBlci51cGRhdGUoKTtcclxuICAgIHVwZGF0ZVBvcHVwUGFkZGluZygpO1xyXG4gICAgJCgnLnJldmlld3MtcG9wdXAgLnJldmlld3MtcG9wdXBfX3RleHQnKS50cmlnZ2VyKCdwczp1cGRhdGUnKTtcclxuICAgICQoJy5yZXZpZXdzLXBvcHVwIC5yZXZpZXdzLXBvcHVwX190ZXh0Jykuc2Nyb2xsVG9wKDApO1xyXG4gICAgJCgnLnJldmlld3MtcG9wdXAgLnJldmlld3MtcG9wdXBfX3RleHQnKS50cmlnZ2VyKCdwcy15LXJlYWNoLXN0YXJ0Jyk7XHJcbiAgfSk7XHJcblxyXG4gICQoJy5qcy1oaWRlLXBvcHVwLXJldmlld3MnKS5vbignY2xpY2snICwgZnVuY3Rpb24oKXtcclxuICAgICQoJy5yZXZpZXdzLXBvcHVwJykucmVtb3ZlQ2xhc3MoJ2lzLXNob3cnKTtcclxuICAgICQoJy5yZXZpZXdzLXBvcHVwJykuY3NzKCdwb3NpdGlvbicgLCAnZml4ZWQnKTtcclxuICAgICQoJ2JvZHknKS5jc3MoeydvdmVyZmxvdyc6J2F1dG8nfSlcclxuICB9KTtcclxuXHJcbiAgJCgnLnJldmlld3MtcG9wdXBfX3RleHQnKS5vbigncHMtc2Nyb2xsLXknICwgZnVuY3Rpb24oKXtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLnJldmlld3MtcG9wdXBfX3RleHQtd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdpcy10b3AnKTtcclxuICAgICQodGhpcykuY2xvc2VzdCgnLnJldmlld3MtcG9wdXBfX3RleHQtd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdpcy1ib3R0b20nKTtcclxuICB9KTtcclxuXHJcbiAgJCgnLnJldmlld3MtcG9wdXBfX3RleHQnKS5vbigncHMteS1yZWFjaC1zdGFydCcgLCBmdW5jdGlvbigpe1xyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCcucmV2aWV3cy1wb3B1cF9fdGV4dC13cmFwcGVyJykuYWRkQ2xhc3MoJ2lzLXRvcCcpO1xyXG4gIH0pO1xyXG5cclxuICAkKCcucmV2aWV3cy1wb3B1cF9fdGV4dCcpLm9uKCdwcy15LXJlYWNoLWVuZCcgLCBmdW5jdGlvbigpe1xyXG4gICAgJCh0aGlzKS5jbG9zZXN0KCcucmV2aWV3cy1wb3B1cF9fdGV4dC13cmFwcGVyJykuYWRkQ2xhc3MoJ2lzLWJvdHRvbScpO1xyXG4gIH0pO1xyXG5cclxuICAkKGRvY3VtZW50KS5vbignY2xpY2snICwgJ3N3aXBlci1idXR0b24tbmV4dCcgLCAnc3dpcGVyLWJ1dHRvbi1wcmV2JyAsIGZ1bmN0aW9uKCl7XHJcbiAgICAkKCcucmV2aWV3cy1wb3B1cCcpLmZpbmQoJy5wc19fcmFpbC15JykuY3NzKCdvcGFjaXR5JyAsICcwJyAsICchaW1wb3J0YW50Jyk7XHJcbiAgfSk7XHJcblxyXG4gIGZ1bmN0aW9uIHVwZGF0ZVBvcHVwUGFkZGluZygpIHtcclxuICAgICQoJy5yZXZpZXdzLXBvcHVwX19pbm5lcicpLmVhY2goZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciAkaW5mbyA9ICQodGhpcykuZmluZCgnLnJldmlld3MtcG9wdXBfX2luZm8nKSxcclxuICAgICAgICAgICR0ZXh0ID0gJCh0aGlzKS5maW5kKCcucmV2aWV3cy1wb3B1cF9fdGV4dC1pbm5lcicpLFxyXG4gICAgICAgICAgJHdyYXBwZXIgPSAkKHRoaXMpLmZpbmQoJy5yZXZpZXdzLXBvcHVwX190ZXh0LXdyYXBwZXInKTtcclxuICAgICAgJHRleHQuY3NzKCdwYWRkaW5nLXRvcCcsICRpbmZvLmlubmVySGVpZ2h0KCkgKyAncHgnKTtcclxuICAgICAgJHdyYXBwZXIuY3NzKHtcclxuICAgICAgICAndG9wJzogJGluZm8uaW5uZXJIZWlnaHQoKSArICdweCcsXHJcbiAgICAgICAgJ21hcmdpbi10b3AnOiAtJGluZm8uaW5uZXJIZWlnaHQoKSoyICsgJ3B4J1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxuICB1cGRhdGVQb3B1cFBhZGRpbmcoKTtcclxuICAkKHdpbmRvdykub24oJ3Jlc2l6ZScsIGZ1bmN0aW9uKCkge1xyXG4gICAgdXBkYXRlUG9wdXBQYWRkaW5nKCk7XHJcbiAgfSk7XHJcbn0pO1xyXG4iXSwiZmlsZSI6ImNvbW1vbi5qcyJ9
