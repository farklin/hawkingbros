<?php

namespace App\Models\Application;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Application extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'phone', 'email', 'message', 'service_id'];
}
