<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'status'];

    public function items()
    {
        return $this->hasMany(MenuItem::class)->where('menu_item_id', null)->orderBy('sorting');
    }
}
