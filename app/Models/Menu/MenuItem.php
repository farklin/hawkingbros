<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'link',
        'status',
        'sorting',
        'menu_id',
        'menu_item_id'
    ];

    protected $table = 'menu_items';

    public function children()
    {
        return $this->hasMany(MenuItem::class, 'menu_item_id')->orderBy('sorting');
    }

    public function getAll()
    {
        return $this->all();
    }
}
