<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'title',
        'content',
        'short_description',
        'image',
        'status',
    ];
}
