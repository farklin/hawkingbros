<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolutionStack extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'status'
    ];

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_solution_stack', 'solution_stack_id', 'project_id', 'id');
    }
}
