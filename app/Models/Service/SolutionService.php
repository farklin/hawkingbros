<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SolutionService extends Model
{
    use HasFactory;

    protected $table = 'service_solution';

    protected $fillable = ['value']; 
    

}
