<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Article;

class Tag extends Model
{
    use HasFactory;
    
    protected $fillable = ['title', 'status']; 

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }
}
