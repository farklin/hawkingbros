<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Parthner extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'link', 'image_id', 'status'
    ];

    public function getImageUrl()
    {
        return $this->image->path ?? '';
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
