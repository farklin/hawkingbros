<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Service\SolutionService;
use Illuminate\Support\Str;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'description', 'status', 'image_id'
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function getImageUrl()
    {
        return $this->image->path ?? '';
    }

    public function solutions()
    {
        return $this->hasMany(SolutionService::class);
    }

    public function getCode()
    {
        return Str::slug($this->title, '-');
    }
}
