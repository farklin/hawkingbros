<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Service\Team;
use Illuminate\Support\Str;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'title', 'volume', 'dedline', 'link', 'service_id', 'status', 'image_id',
    ];


    public function teams()
    {
        return $this->hasMany(Team::class, 'project_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function solution_stacks()
    {
        return $this->belongsToMany(SolutionStack::class, 'project_solution_stack', 'project_id', 'solution_stack_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'project_tag', 'project_id', 'tag_id', 'id');
    }

    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id');
    }

    public function getImageUrl()
    {
        return $this->image->path ?? '';
    }

    public function getCode()
    {
        return Str::slug($this->title, '-');
    }
}
