<?php

namespace App\Models\Service;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
    use HasFactory;

    protected $fillable = ['path', 'alt', 'status'];
    /**
     * Загрузка картинки
     *
     * @param Request $request 
     * @param string $key // Ключ массива Request файла каринки
     * @param string $store // имя хранилища Storage
     * @return void
     */
    public function downloadImage(Request $request, string $key, string $store)
    {
        if ($request->has($key)) {
            $this->attributes['path'] = $request->file($key)->store($store);
            $this->save();
        }
    }
    /**
     * Удаление картинки 
     *
     * @return void
     */
    public function destroyImage()
    {
        Storage::delete($this->path);
        $this->delete();
    }
}
