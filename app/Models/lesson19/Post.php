<?php

namespace App\Models\lesson19;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Post extends Model
{
    use HasFactory;
    
    use \App\Traits\Relation\BelongsTo\User;
}
