<?php

namespace App\Models\lesson18;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Lesson extends Model
{
    use HasFactory;
    /* 
    * объяснить отличия with & load.
    * Отличие заключается в том что with непосредственно сразу выполняет 2 запроса,
    * сначала получает подборку например User::all() а уже потом непосредственно связывает
    * с промежуточной связанной таблицей

    * а load сначала получает все записи и только на следующем шагу ищет связи, что позволяет более гибко вести отбор из записей связанной таблицы

       /**
     * Получение всех записей
     *
     * @return void
     */
    static public function getAll()
    {
        return self::all(); 
    }

    /**
     * Получение по названию
     * 
     * @param string $key 
     * @param string $title
     * @return void
     */
    static public function getValueByKey(string $key, string $name)
    {
        return self::where($key, $name)->get(); 
    }

    /**
     * получить записи, дата создания которых не больше 2 часов от текущего момента времени;
    */
    static public function getLessToHourse()
    {   
        return self::whereBetween('created_at', [Carbon::today()->now()->subHours(2), Carbon::today()->now() ] )->get();
    }

}
