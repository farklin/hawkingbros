<?php

namespace App\Admin\Controllers;

use App\Models\Service\Service;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class ServiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Service';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Service());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Title'));
        $grid->column('description', __('Description'));
        $grid->column('status', __('Status'));
        $grid->column('image_id', __('Image id'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Service::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('status', __('Status'));
        $show->field('image_id', __('Image id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Service());

        $form->text('title', __('Название'));
        $form->textarea('description', __('Описание'));
        $form->switch('status', __('Статус'));
        $form->image('image.path', __('Изображение'),  'Image')->move('service');

        $form->hasMany('solutions', 'Решения', function (Form\NestedForm $form) {
            $form->text('value', __('Название'));
        });

        return $form;
    }

    public function serviceCatalog(Request $request)
    {
        $q = $request->get('q');

        return Service::where('title', 'like', "%$q%")->paginate(null, ['id', 'title as text']);
    }

}
