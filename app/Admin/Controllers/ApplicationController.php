<?php

namespace App\Admin\Controllers;

use App\Models\Application\Application;
use App\Models\Service\Service;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ApplicationController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Заявки';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Application());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Название'));
        $grid->column('phone', __('Телефон'));
        $grid->column('email', __('Email'));
        $grid->column('message', __('Сообщение'));
        $grid->column('service_id', __('Услуга'));
        $grid->column('status', __('Статус'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Application::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Имя'));
        $show->field('phone', __('Телефон'));
        $show->field('email', __('Email'));
        $show->field('message', __('Сообщение'));
        $show->field('service_id', __('Service id'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Application());

        $form->text('name', __('Имя'));
        $form->mobile('phone', __('Телефон'));
        $form->email('email', __('Email'));
        $form->textarea('message', __('Сообщение'));
        
        $form->select('service_id')->options(function ($id) {
            $service = Service::find($id);

            if ($service) {
                return [$service->id => $service->title];
            }
        })->ajax('/admin/api/service');


        $form->switch('status', __('Status'));

        return $form;
    }
}
