<?php

namespace App\Admin\Controllers;

use App\Models\Menu\MenuItem;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MenuItemController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'MenuItem';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MenuItem());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('link', __('Link'));
        $grid->column('sorting', __('Sorting'));
        $grid->column('status', __('Status'));
        $grid->column('menu_id', __('Menu id'));
        $grid->column('menu_item_id', __('Menu item id'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MenuItem::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('link', __('Link'));
        $show->field('sorting', __('Sorting'));
        $show->field('status', __('Status'));
        $show->field('menu_id', __('Menu id'));
        $show->field('menu_item_id', __('Menu item id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MenuItem());

        $form->text('name', __('Name'));
        $form->url('link', __('Link'));
        $form->number('sorting', __('Sorting'));
        $form->switch('status', __('Status'));
        $form->number('menu_id', __('Menu id'));
        $form->number('menu_item_id', __('Menu item id'));

        return $form;
    }
}
