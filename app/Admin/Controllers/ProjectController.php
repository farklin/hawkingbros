<?php

namespace App\Admin\Controllers;

use App\Admin\Selectable\SolutionStack as SelectableSolutionStack;
use App\Models\Service\Project;
use App\Models\Service\Service;
use App\Models\Service\SolutionStack;
use App\Models\Service\Team;
use App\Models\Service\Tag;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;

class ProjectController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Project';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Project());

        $grid->column('id', __('Id'));
        $grid->column('title', __('Title'));
        $grid->column('volume', __('Volume'));
        $grid->column('dedline', __('Dedline'));
        $grid->column('link', __('Link'));
        $grid->column('status', __('Status'));
        $grid->column('image_id', __('Image id'));
        $grid->column('service_id', __('Service id'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
 
        
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Project::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('title', __('Title'));
        $show->field('volume', __('Volume'));
        $show->field('dedline', __('Dedline'));
        $show->field('link', __('Link'));
        $show->field('status', __('Status'));
        $show->field('image_id', __('Image id'));
        $show->field('service_id', __('Service id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Project());
        $form->column(12, function ($form) {
            $form->text('title', __('Название'));
            $form->text('volume', __('Обьем'));
            $form->text('dedline', __('Длительность'));
            $form->url('link', __('Ссылка на проект'));
            $form->switch('status', __('Статус'));
            $form->image('image.path', __('Изображение'), 'Image')->move('project');
        }); 

        $form->select('service_id', __('Услуга'))->options(function ($id) {
            $service = Service::find($id);

            if ($service) {
                return [$service->id => $service->title];
            }
        })->ajax('/admin/api/service');
        
        
    
        $form->column(1/2, function ($form) {
            $form->hasMany('teams', 'Команда', function (Form\NestedForm $form) {
                $form->text('name', __('Член команды'));
            });
        }); 

        $form->column(1/2, function ($form) {
           
            $form->belongsToMany('solution_stacks', SelectableSolutionStack::class, __('Стек технологий'));
            $form->multipleSelect('tags','Тэги')->options(Tag::all()->pluck('title','id'));
        }); 
        
        
        return $form;
    }


}
