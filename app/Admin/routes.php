<?php

use App\Models\Service\Service;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;


Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('/menu/menu', MenuController::class);
    $router->resource('/menu/menu-item', MenuItemController::class);
    $router->resource('/application', ApplicationController::class);
    $router->resource('/projects', ProjectController::class);
    $router->resource('/service', ServiceController::class);
    $router->resource('/parthner', ParthnerController::class);
    $router->resource('curses', CurseController::class);
    
    $router->get('/api/service', function(Request $request){
        $q = $request->get('q');
        return Service::where('title', 'like', "%$q%")->paginate(null, ['id', 'title as text']);
    });

});
