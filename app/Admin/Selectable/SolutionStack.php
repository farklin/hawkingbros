<?php

namespace App\Admin\Selectable;

use App\Models\Service\SolutionStack as ServiceSolutionStack;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Grid\Selectable;



class SolutionStack extends Selectable
{
    public $model = ServiceSolutionStack::class;

    public function make()
    {
        $this->column('id');
        $this->column('title');

        $this->filter(function (Filter $filter) {
            $filter->like('title');
        });
    }
    
}

