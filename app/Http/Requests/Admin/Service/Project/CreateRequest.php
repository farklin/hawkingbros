<?php

namespace App\Http\Requests\Admin\Service\Project;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'volume' => 'required',
            'dedline' => 'required',
            'link' => 'required',
            'image' => 'required',
            'teams' => 'array',
            'tags' => 'array',
            'solution_stacks' => 'array',
            'service_id' => 'required',
            'status' => 'nullable',
        ];
    }
}
