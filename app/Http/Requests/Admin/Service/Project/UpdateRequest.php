<?php

namespace App\Http\Requests\Admin\Service\Project;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'volume' => 'required',
            'dedline' => 'required',
            'link' => 'required',
            'tags' => 'array',
            'status' => 'nullable',
            'image' => 'nullable',
        ];
    }
}
