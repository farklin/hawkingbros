<?php

namespace App\Http\Requests\Admin\Service\Article;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => 'required',
            'title' => 'required',
            'content' => 'required',
            'short_description' => 'required',
            'image' => 'nullable',
            'status' => 'nullable',
        ];
    }
}
