<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UseCases\Service\ProjectService;


class ProjectController extends Controller
{
    protected $service;

    public function __construct(ProjectService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Проекты";
        $projects = $this->service->getProjects();
        $tags = $this->service->getTags();
        $services = $this->service->getServices();
        return view('templates.cases', compact('title', 'projects', 'tags', 'services'));
    }

    public function filter(Request $request)
    {
        $tags = $this->service->getTags();
        $services = $this->service->getServices();
        $title = "Проекты";
        $projects = $this->service->getFilter(
            $request->input('tags', []),
            $request->input('services', [])
        );

        return view('templates.cases', compact('title', 'projects', 'tags', 'services'));
    }
}
