<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Jobs\IncPointUserJob;
use App\Models\lesson19\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{   
    /**
     * В методе PostController->create с помощью хелпера вызывать view с формой создания поста. Сабмитить форму на метод PostController->store.
     *
     * @return void
     */
    public function create()
    {   
        return view('lessons/form/form-post'); 
    }

    /**
     * В методе PostController->store сохранять полученные из формы данные. Не использовать входной параметр Request для получения данных из запроса.
     *
     * @return void
     */
    public function store(PostRequest $request)
    {
        $validated = $request->validated();
    
        $post = new Post();
        $post->name = $request->name;
        $post->text = $request->text;
        $post->image = $request->image->store('images');

        if(Auth::check())
        {
            $post->user_id = Auth::user()->id;
            IncPointUserJob::dispatch(1, Auth::user()->id);
        }

        $post->save();
        return back()->with('success', 'Запись успешно добавлена');
    }

}
