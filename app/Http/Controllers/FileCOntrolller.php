<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage; 
use Illuminate\Http\Request;

class FileCOntrolller extends Controller
{
    /**
     * получить содержимое файла;
     */
    public function getFile()
    {
        $contents = Storage::disk('public')->get('files/file.jpg');
        return $contents; 
    }

    /**
     * определить существование;
    */
    public function existFile()
    {
        return  Storage::disk('public')->exists('files/file.jpg'); 
    }

    /**
     * скачать файл;
     */
    public function DownloadFile()
    {   
        
        return response()->download(Storage::disk('public')->path('files/file.jpg'), 'user_profile_photo.jpg'); 
    }


}
