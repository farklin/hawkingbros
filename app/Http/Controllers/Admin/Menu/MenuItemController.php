<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Menu\MenuItem\CreateRequest;
use App\Http\Requests\Admin\Menu\MenuItem\UpdateRequest;
use App\Models\Menu\MenuItem;
use App\UseCases\Admin\Menu\MenuItemManagerService;
use Illuminate\Http\Request;

class MenuItemController extends Controller
{

    protected $service;
    public function __construct(MenuItemManagerService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($menu)
    {
        $menus = $this->service->getMenuItems();
        return view('admin/templates/menu/menu-item/create', compact('menu', 'menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $item = $this->service->create($request);
        return redirect(route('admin.menu.show', $item->menu_id))->with('Запись успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menuItem = $this->service->getMenuItem($id);
        $menu = $menuItem->menu_id;
        $menus = $this->service->getMenuItemsChoise($id);
        return view('admin/templates/menu/menu-item/update', compact('menuItem', 'menu', 'menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $menuItem)
    {   
        $menuItem = $this->service->getMenuItem($menuItem);
        $this->service->update($request, $menuItem);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuItem $menuItem)
    {
        $this->service->remove($menuItem);
        return redirect(route('admin.menu.show'))->with('Запись успешно обновлена');
    }
}
