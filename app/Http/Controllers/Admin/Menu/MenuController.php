<?php

namespace App\Http\Controllers\Admin\Menu;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Menu\Menu\CreateRequest;
use App\Http\Requests\Admin\Menu\Menu\UpdateRequest;
use App\Models\Menu\Menu;
use App\UseCases\Admin\Menu\MenuManagerService;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    protected $service; 

    public function __construct( MenuManagerService $service )
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = $this->service->getMenus();
        return view('admin/templates/menu/list', compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/templates/menu/create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->service->create($request);
        return redirect(route('admin.menu.index'))->with('message', 'Запись успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($menu_id)
    {
        $menu = $this->service->getMenu($menu_id);
        return view('admin/templates/menu/menu-item', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($menu_id)
    {
        $menu = $this->service->getMenu($menu_id);
        return view('admin/templates/menu/update', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Menu $menu)
    {
        $this->service->update($request, $menu);
        return redirect(route('admin.menu.index'))->with('message', 'Запись успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $this->service->remove($menu);
        return redirect(route('admin.menu.index'))->with('message', 'Запись успешно удалена');
    }
}
