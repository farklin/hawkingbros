<?php

namespace App\Http\Controllers\Admin\Application;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Application\UpdateRequest;
use App\Models\Application\Application;
use App\Models\Service;
use App\UseCases\Admin\Application\ApplicationManagerService;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    protected $service;

    public function __construct(ApplicationManagerService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = $this->service->getApplications()->paginate(config('constants.options.option_pagination'));;
        return view('admin/templates/application/list', compact('applications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/templates/application/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect('admin/application');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($application_id)
    {
        $application = $this->service->getApplication($application_id);
        $services = $this->service->getServices();
        return view('admin/templates/application/update', compact('application', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Application $application)
    {
        $this->service->update($request, $application);
        return redirect(route('admin.application.index'))->with('message', 'Запись успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        $this->service->remove($application);
        return redirect(route('admin.application.index'))->with('message', 'Запись успешно удалена');
    }
}
