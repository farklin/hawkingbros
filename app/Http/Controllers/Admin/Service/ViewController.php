<?php

namespace App\Http\Controllers\Admin\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    // Домашная страница
    public function home()
    {
        return view('admin/templates/home/home');
    }

    // тестоывый метод удалимть 
    public function test()
    {
        return view('admin/templates/application/list');
    }
}
