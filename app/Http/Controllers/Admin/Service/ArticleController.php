<?php

namespace App\Http\Controllers\Admin\Service;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Service\Article\CreateRequest;
use App\Http\Requests\Admin\Service\Article\UpdateRequest;
use App\Models\Service\Article;
use App\UseCases\Admin\Service\ArticleManagerService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $service;
    public function __construct(ArticleManagerService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->service->getArticles()->paginate(config('constants.options.option_pagination'));
        return view('admin/templates/article/list', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/templates/article/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->service->create($request);
        return redirect(route('admin.article.index'))->with('message', 'Запись успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = $this->service->getArticle($id);
        return view('admin/templates/article/update', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Article $article)
    {
        $this->service->update($request, $article);
        return redirect(route('admin.article.index'))->with('message', 'Запись успешно изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($article)
    {
        $this->service->remove($article);
        return redirect(route('admin.article.index'))->with('message', 'Запись успешно удалена');
    }
}
