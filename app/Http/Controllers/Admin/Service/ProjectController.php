<?php

namespace App\Http\Controllers\Admin\Service;

use App\Helper\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Service\Project\CreateRequest;
use App\Http\Requests\Admin\Service\Project\UpdateRequest;
use App\Models\Service\Project;
use App\Query\Service\ServiceAllQuery;
use App\UseCases\Admin\Service\ProjectManagerService;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    private $service;

    public function __construct(ProjectManagerService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->service->getProjects()->paginate(config('constants.options.option_pagination'));
        return view('admin/templates/project/list', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services =  $this->service->getServices();
        return view('admin/templates/project/create', compact('services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->service->create($request);
        return redirect(route('admin.project.index'))->with('message', 'Запись успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($project)
    {
        $project = $this->service->getProject($project);
        $services =  $this->service->getServices();
        return view('admin/templates/project/update', compact('project', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Project $project)
    {
        $this->service->update($request, $project);
        return redirect(route('admin.project.index'))->with('message', 'Запись успешно обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
