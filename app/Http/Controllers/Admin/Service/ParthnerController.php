<?php

namespace App\Http\Controllers\Admin\Service;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Service\Parthner\CreateRequest;
use App\Http\Requests\Admin\Service\Parthner\UpdateRequest;
use App\Models\Service\Parthner;
use App\UseCases\Admin\Service\ParthnerManagerService;
use Illuminate\Http\Request;

class ParthnerController extends Controller
{

    private $service;
    public function __construct(ParthnerManagerService $service)
    {
        $this->service = $service;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parthners = $this->service->getParthners()->paginate(config('constants.options.option_pagination'));
        return view('admin/templates/parthner/list', compact('parthners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/templates/parthner/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $this->service->create($request);
        return redirect(route('admin.parthner.index'))->with('message', 'Запись успешно добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parthner = $this->service->getParthner($id);
        return view('admin/templates/parthner/update', compact('parthner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Parthner $parthner)
    {
        $this->service->update($request, $parthner);
        return redirect(route('admin.parthner.index'))->with('message', 'Запись успешно изменена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($parthner)
    {
        $this->service->remove($parthner);
        return redirect(route('admin.parthner.index'))->with('message', 'Запись успешно удалена');
    }
}
