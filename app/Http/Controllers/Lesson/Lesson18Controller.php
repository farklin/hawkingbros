<?php

namespace App\Http\Controllers\Lesson;

use App\Http\Controllers\Controller;
use App\Models\lesson18\Curse;
use Illuminate\Http\Request;

class Lesson18Controller extends Controller
{
    public function index()
    {
        return Curse::getCursByTitle('name', '');
    }
}
