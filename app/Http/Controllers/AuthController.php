<?php

namespace App\Http\Controllers;

use App\Events\RegisterUserEvent;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Вывод формы авторизации 
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
       return view('lessons/auth.login'); 
    }

    /**
     * Авторизация
     *
     * @param Request $request
     * @return void
     */
    public function auth(Request $request)
    {
        if(!Auth::attempt($request->only('email', 'password'), $request->has('remember')))
        {
            return back()->with('error', 'Неверный логин или пароль');
        }
        else{
            $user = Auth::user(); 
            return redirect(route('profiel', $user)); 
        }
    }

    /**
     * Страница профиля
     *
     * @param User $user
     * @return void
     */
    public function profiel(User $user)
    {
        return view('lessons/auth/profiel', ['email' => $user->email, 'user' => $user]); 
    }

    /**
     * Вывод формы регистрации
     *
     * @return void
     */
    public function register()
    {
        return view('lessons/auth/register'); 
    }

    /**
     * Регистрация пользователя 
     *
     * @param Request $request
     * @return void
     */
    public function registration(Request $request)
    {
        $user = User::create([
            'email'    => $request->input('email'),
            'name'     => $request->input('name'),
            'password' => bcrypt($request->input('password'))
        ]);
        // событие регистрации 
        event(new RegisterUserEvent($user));

        Auth::loginUsingId($user->id);

        return redirect()
                ->route('message')
                ->with('message', 'Вы успешно зарегистрировались');
    }
    
    /**
     * Вывод сообщений
     *
     * @return void
     */
    public function message()
    {
        return view('lessons/auth/message'); 
    }

}
