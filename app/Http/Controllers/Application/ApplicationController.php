<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Application\CreateRequest;
use App\Models\Application\Application;


class ApplicationController extends Controller
{

    /**
     * Создание заявки 
     *
     * @param CreateRequest $request
     * @return void
     */
    public function store(CreateRequest $request)
    {

        Application::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'message' =>  $request->message,
            'email' =>  $request->email,
            'service_id' => $request->service_id,
        ]);

        return back()->with('form_message', true);
    }
}
