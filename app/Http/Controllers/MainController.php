<?php

namespace App\Http\Controllers;

use App\Models\Service\Parthner;
use App\Models\Service\Project;
use App\Models\Service\Service;
use Illuminate\Http\Request;

class MainController extends Controller
{
    /**
     * Вывод главной страницы
     *
     * @return void
     */
    public function __invoke()
    {
        $title = 'Главная';
        $projects = Project::where('status', 1)->limit(2)->get();
        $services = Service::where('status', 1)->limit(2)->get();
        $parthners = Parthner::where('status', 1)->get();

        return view('templates.home', compact('title', 'projects', 'services', 'parthners'));
    }
}
