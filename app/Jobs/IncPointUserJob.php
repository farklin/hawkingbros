<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IncPointUserJob implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels; 

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $point;
    private $user_id;
    public $uniqueFor = 1800;

    public function __construct($point, $user_id)
    {
        $this->point = $point; 
        $this->user_id = $user_id;
    }

    public function uniqueId()
    {
        return $this->user_id;
    }

    /**
     * Execute the job.
     *sss
     * @return void
     */
    public function handle()
    {
        $user = User::find($this->user_id); 
        $user->point = $user->point + $this->point; 
        $user->save(); 
    }
}
