<?php

namespace App\UseCases\Service;

use App\Query\Project\FindProjectActiveQuery;
use App\Query\Project\FindProjectQuery;
use App\Query\Project\FindProjectTagsQuery;
use App\Query\Project\FindProjectServicesQuery;
use App\Query\Project\FindProjectFilterQuery;
use App\UseCases\Service;

class ProjectService extends Service
{
    /**
     * Получить все проекты
     *
     * @return $collection
     */

     // Вывод активных проектов

    public function getProjects()
    {
        return $this->queryBus->query(new FindProjectActiveQuery); 
    }

    /**
     * Вывод всех тегов у проектов
     *
     * @return $collection
     */
    public function getTags()
    {   
        return $this->queryBus->query(new FindProjectTagsQuery); 
    }

    /**
     * Вывод всех услуг у проектов
     *
     * @return $collection
     */
    public function getServices()
    {
        return $this->queryBus->query(new FindProjectServicesQuery); 
    }

    /**
     * Фильтрация списка проектов 
     *
     * @param [array] $tags
     * @param [array] $services
     * @return $collection 
     */
    public function getFilter($tags, $services)
    {
        return $this->queryBus->query(new FindProjectFilterQuery($tags, $services));  
    }
}
