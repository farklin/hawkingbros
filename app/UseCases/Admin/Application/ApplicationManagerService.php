<?php

namespace App\UseCases\Admin\Application;

use App\Command\Admin\Application\Remove\Command as RemoveApplicationCommand;
use App\Command\Admin\Application\Update\Command as UpdateApplicatonCommand;
use App\Http\Requests\Admin\Application\UpdateRequest;
use App\Models\Application\Application;
use App\Query\Application\ApplicationAllQuery;
use App\Query\Application\FindApplicationQuery;
use App\Query\Service\ServiceAllQuery;
use App\UseCases\Service;


class ApplicationManagerService extends Service
{
    //обновление заявки
    public function update(UpdateRequest $request, Application $application)
    {
        $this->commandBus->handle(new UpdateApplicatonCommand($request, $application));
    }

    //Удаление заявки
    public function remove(Application $application)
    {
        $this->commandBus->handle(new RemoveApplicationCommand($application));
    }

    //Вывод всех 
    public function getApplication($application_id)
    {
        return $this->queryBus->query(new FindApplicationQuery($application_id));
    }

    /**
     * Вывод всех услуг
     *
     * @return $collection 
     */
    public function getServices()
    {
        return $this->queryBus->query(new ServiceAllQuery());
    }
    
     /**
     * Вывод всех заявок 
     *
     * @return $collection 
     */
    public function getApplications()
    {
        return $this->queryBus->query(new ApplicationAllQuery());
    }
}
