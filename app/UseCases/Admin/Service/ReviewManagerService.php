<?php

namespace App\UseCases\Admin\Service;

use App\Http\Requests\Admin\Service\Review\CreateRequest;
use App\Http\Requests\Admin\Service\Review\UpdateRequest;
use App\Models\Service\Image;
use Illuminate\Support\Facades\Storage;
use App\Helper\Helper;
use App\Models\Service\Review;
use App\UseCases\Service;
use App\Command\Admin\Service\Review\Create\Command as ReviewCreateCommand;
use App\Command\Admin\Service\Review\Update\Command as ReviewUpdateCommand;
use App\Command\Admin\Service\Review\Remove\Command as ReviewRemoveCommand;
use App\Command\Admin\Service\Review\AddImage\Command as ReviewAddImageCommand;
use App\Command\Admin\Service\Image\Create\Command as ImageCreateCommand;
use App\Query\Parthner\ParthnerAllQuery;
use App\Query\Review\ReviewAllQuery;
use App\Query\Review\FindReviewQuery;

class ReviewManagerService extends Service
{
    public function create(CreateRequest $request)
    {
        $review = $this->commandBus->handle(new ReviewCreateCommand($request));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('review', $request->image));
            $this->commandBus->handle(new ReviewAddImageCommand($review, $image_id));
        }
    }

    public function update(UpdateRequest $request, Review $review)
    {
        $this->commandBus->handle(new ReviewUpdateCommand($request, $review));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('review', $request->image));
            $this->commandBus->handle(new ReviewAddImageCommand($review, $image_id));
        }
    }

    public function remove(Review $review)
    {
        $this->commandBus->handle(new ReviewRemoveCommand($review));
    }

    public function getReviews()
    {
        return $this->queryBus->query(new ReviewAllQuery());
    }

    public function getReview($review_id)
    {
        return $this->queryBus->query(new FindReviewQuery($review_id));
    }
}
