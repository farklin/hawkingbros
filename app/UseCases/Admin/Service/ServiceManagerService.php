<?php

namespace App\UseCases\Admin\Service;

use App\Command\Admin\Service\Service\Create\Command as ServiceCreateCommand;
use App\Command\Admin\Service\Service\Update\Command as ServiceUpdateCommand;
use App\Command\Admin\Service\Service\Remove\Command as ServiceRemoveCommand;
use App\Command\Admin\Service\Service\Solution\Create\Command as AddSolutionCommand;
use App\Command\Admin\Service\Service\Solution\Update\Command as UpdateSolutionCommnad;
use App\Command\Admin\Service\Service\AddImage\Command as ServciceAddImageCommand;
use App\Http\Requests\Admin\Service\Service\CreateRequest;
use App\Http\Requests\Admin\Service\Service\UpdateRequest;
use App\Models\Service\Service as ServiceM;
use App\Query\Service\ServiceAllQuery;
use App\Query\Service\FindServiceQuery;
use App\UseCases\Service;
use App\Command\Admin\Service\Image\Create\Command as ImageCreateCommand;



class ServiceManagerService extends Service
{
    public function create(CreateRequest $request)
    {
        $service = $this->commandBus->handle(new ServiceCreateCommand($request));
        $this->commandBus->handle(new AddSolutionCommand($request->solutions, $service));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('service', $request->image));
            $this->commandBus->handle(new ServciceAddImageCommand($service, $image_id));
        }
    }

    public function update(UpdateRequest $request, ServiceM $service)
    {
        $this->commandBus->handle(new ServiceUpdateCommand($request, $service));
        $this->commandBus->handle(new UpdateSolutionCommnad($request->solutions, $service));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('service', $request->image));
            $this->commandBus->handle(new ServciceAddImageCommand($service, $image_id));
        }
    }

    public function remove(ServiceM $service)
    {
        $this->commandBus->handle(new ServiceRemoveCommand($service));
    }

    public function getServices()
    {
        return $this->queryBus->query(new ServiceAllQuery());
    }

    public function getService($service_id)
    {
        return $this->queryBus->query(new FindServiceQuery($service_id));
    }
}
