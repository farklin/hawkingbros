<?php

namespace App\UseCases\Admin\Service;

use App\Http\Requests\Admin\Service\Project\CreateRequest;
use App\Http\Requests\Admin\Service\Project\UpdateRequest;
use App\Models\Service\Image;
use App\Models\Service\Project;
use Illuminate\Support\Facades\Storage;
use App\Helper\Helper;
use App\UseCases\Service;
use App\Command\Admin\Service\Project\Create\Command as ProjectCreateCommand;
use App\Command\Admin\Service\Project\Update\Command as ProjectUpdateCommand;
use App\Command\Admin\Service\Project\Remove\Command as ProjectRemoveCommand;
use App\Command\Admin\Service\Project\Team\Create\Command as AddTeamsCommand;
use App\Command\Admin\Service\Project\Team\Update\Command as UpdateTeamsCommand;
use App\Command\Admin\Service\Project\SolutionStack\Create\Command as AddSolutionStackCommand;
use App\Command\Admin\Service\Project\SolutionStack\Update\Command as UpdateSolutionStackCommand;
use App\Command\Admin\Service\Project\Tag\Create\Command as AddTagCommand;
use App\Command\Admin\Service\Project\Tag\Update\Command as UpdateTagCommand;
use App\Command\Admin\Service\Project\AddImage\Command as ProjectAddImageCommand;
use App\Command\Admin\Service\Image\Create\Command as ImageCreateCommand;
use App\Query\Project\FindProjectQuery;
use App\Query\Project\ProjectAllQuery;
use App\Query\Service\ServiceAllQuery;

class ProjectManagerService extends Service
{
    public function create(CreateRequest $request)
    {
        $project = $this->commandBus->handle(new ProjectCreateCommand($request));
        $this->commandBus->handle(new AddTeamsCommand($request->teams, $project));
        $this->commandBus->handle(new AddSolutionStackCommand($request->solution_stacks, $project));
        $this->commandBus->handle(new AddTagCommand($request->tags, $project));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('project', $request->image));
            $this->commandBus->handle(new ProjectAddImageCommand($project, $image_id));
        }
    }

    public function update(UpdateRequest $request, Project $project)
    {
        $this->commandBus->handle(new ProjectUpdateCommand($request, $project));
        $this->commandBus->handle(new UpdateTeamsCommand($request->teams, $project));
        $this->commandBus->handle(new UpdateSolutionStackCommand($request->solution_stacks, $project));
        $this->commandBus->handle(new UpdateTagCommand($request->tags, $project));
        
        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('project', $request->image));
            $this->commandBus->handle(new ProjectAddImageCommand($project, $image_id));
        }
    }

    public function remove(Project $project)
    {
        $this->commandBus->handle(new ProjectRemoveCommand($project));
    }

    public function getServices()
    {
        return $this->queryBus->query(new ServiceAllQuery());
    }

    public function getProjects()
    {
        return $this->queryBus->query(new ProjectAllQuery());
    }

    public function getProject($project_id)
    {
        return $this->queryBus->query(new FindProjectQuery($project_id));
    }
}
