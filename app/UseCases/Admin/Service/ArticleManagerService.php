<?php

namespace App\UseCases\Admin\Service;

use App\Http\Requests\Admin\Service\Article\CreateRequest;
use App\Http\Requests\Admin\Service\Article\UpdateRequest;
use App\Models\Service\Image;
use App\Models\Service\Article;
use Illuminate\Support\Facades\Storage;
use App\Helper\Helper;
use App\UseCases\Service;
use App\Command\Admin\Service\Article\Create\Command as ArticleCreateCommand;
use App\Command\Admin\Service\Article\Update\Command as ArticleUpdateCommand;
use App\Command\Admin\Service\Article\Remove\Command as ArticleRemoveCommand;
use App\Command\Admin\Service\Article\AddImage\Command as ArticleAddPhotoCommand;
use App\Command\Admin\Service\Image\Create\Command as ImageCreateCommand;
use App\Query\Article\ArticleAllQuery;
use App\Query\Article\FindArticleQuery;

class ArticleManagerService extends Service
{
    public function create(CreateRequest $request)
    {
        $parthner = $this->commandBus->handle(new ArticleCreateCommand($request));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('article', $request->image));
            $this->commandBus->handle(new ArticleAddPhotoCommand($parthner, $image_id));
        }
    }

    public function update(UpdateRequest $request, Article $parthner)
    {
        $this->commandBus->handle(new ArticleUpdateCommand($request, $parthner));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('article', $request->image));
            $this->commandBus->handle(new ArticleAddPhotoCommand($parthner, $image_id));
        }
    }

    public function remove(Article $parthner)
    {
        $this->commandBus->handle(new ArticleRemoveCommand($parthner));
    }

    public function getArticles()
    {
        return $this->queryBus->query(new ArticleAllQuery());
    }

    public function getArticle($parthner_id)
    {
        return $this->queryBus->query(new FindArticleQuery($parthner_id));
    }
}
