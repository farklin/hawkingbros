<?php

namespace App\UseCases\Admin\Service;

use App\Http\Requests\Admin\Service\Parthner\CreateRequest;
use App\Http\Requests\Admin\Service\Parthner\UpdateRequest;
use App\Models\Service\Image;
use App\Models\Service\Parthner;
use Illuminate\Support\Facades\Storage;
use App\Helper\Helper;
use App\UseCases\Service;
use App\Command\Admin\Service\Parthner\Create\Command as ParthnerCreateCommand;
use App\Command\Admin\Service\Parthner\Update\Command as ParthnerUpdateCommand;
use App\Command\Admin\Service\Parthner\Remove\Command as ParthnerRemoveCommand;
use App\Command\Admin\Service\Parthner\AddImage\Command as ParthnerAddPhotoCommand;
use App\Command\Admin\Service\Image\Create\Command as ImageCreateCommand;
use App\Query\Parthner\ParthnerAllQuery;
use App\Query\Parthner\FindParthnerQuery;

class ParthnerManagerService extends Service
{
    public function create(CreateRequest $request)
    {
        $parthner = $this->commandBus->handle(new ParthnerCreateCommand($request));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('parthner', $request->image));
            $this->commandBus->handle(new ParthnerAddPhotoCommand($parthner, $image_id));
        }
    }

    public function update(UpdateRequest $request, Parthner $parthner)
    {
        $this->commandBus->handle(new ParthnerUpdateCommand($request, $parthner));

        if ($request->image) {
            $image_id = $this->commandBus->handle(new ImageCreateCommand('parthner', $request->image));
            $this->commandBus->handle(new ParthnerAddPhotoCommand($parthner, $image_id));
        }
    }

    public function remove(Parthner $parthner)
    {
        $this->commandBus->handle(new ParthnerRemoveCommand($parthner));
    }

    public function getParthners()
    {
        return $this->queryBus->query(new ParthnerAllQuery());
    }

    public function getParthner($parthner_id)
    {
        return $this->queryBus->query(new FindParthnerQuery($parthner_id));
    }
}
