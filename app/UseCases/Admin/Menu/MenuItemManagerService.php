<?php

namespace App\UseCases\Admin\Menu;


use App\Http\Requests\Admin\Menu\MenuItem\CreateRequest;
use App\Http\Requests\Admin\Menu\MenuItem\UpdateRequest;
use App\UseCases\Service;
use App\Models\Menu\MenuItem;
use App\Command\Admin\Menu\MenuItem\Create\Command as MenuItemCreateCommand;
use App\Command\Admin\Menu\MenuItem\Update\Command as MenuItemUpdateCommand;
use App\Command\Admin\Menu\MenuItem\Remove\Command as MenuItemRemoveCommand;
use App\Query\Menu\MenuItem\FindMenuItemChoiseQuery;
use App\Query\Menu\MenuItem\FindMenuItemQuery;
use App\Query\Menu\MenuItem\MenuItemAllQuery;

class MenuItemManagerService extends Service
{
    /**
     * Создание элемента меню
     *
     * @param CreateRequest $request
     * @return void
     */
    public function create(CreateRequest $request)
    {
        return $this->commandBus->handle(new MenuItemCreateCommand($request));
    }

    /**
     * Обновление элемента меню 
     *
     * @param UpdateRequest $request
     * @param [MenuItem] $menu
     * @return void
     */
    public function update(UpdateRequest $request, MenuItem $menu)
    {
        $this->commandBus->handle(new MenuItemUpdateCommand($request, $menu));
    }

    /**
     * Удаление элемента меню 
     *
     * @param MenuItem $menu
     * @return void
     */
    public function remove(MenuItem $menu)
    {
        $this->commandBus->handle(new MenuItemRemoveCommand($menu));
    }

    /**
     * Вывод всех элементов меню 
     *
     * @return  $collection
     */
    public function getMenuItems()
    {
        return $this->queryBus->query(new MenuItemAllQuery());
    }

    /**
     * Вывод одного элемента меню кроме
     *
     * @param [int] $menu_item_id
     * @return $collection 
     */
    public function getMenuItem($menu_item_id)
    {
        return $this->queryBus->query(new FindMenuItemQuery($menu_item_id));
    }

     /**
     * Вывод элементов меню без вывода данного элемента 
     *
     * @param [int] $menu_item_id
     * @return $collection 
     */
    public function getMenuItemsChoise($menu_item_id)
    {
        return $this->queryBus->query(new FindMenuItemChoiseQuery($menu_item_id));
    }
}
