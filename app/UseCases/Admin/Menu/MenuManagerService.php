<?php

namespace App\UseCases\Admin\Menu;


use App\Http\Requests\Admin\Menu\Menu\CreateRequest;
use App\Http\Requests\Admin\Menu\Menu\UpdateRequest;
use App\UseCases\Service;
use App\Models\Menu\Menu; 
use App\Command\Admin\Menu\Menu\Create\Command as MenuCreateCommand; 
use App\Command\Admin\Menu\Menu\Update\Command as MenuUpdateCommand; 
use App\Command\Admin\Menu\Menu\Remove\Command as MenuRemoveCommand;
use App\Query\Menu\Menu\FindMenuQuery;
use App\Query\Menu\Menu\MenuAllQuery;

class MenuManagerService extends Service
{
    public function create(CreateRequest $request)
    {
       $this->commandBus->handle(new MenuCreateCommand($request));
    }

    public function update(UpdateRequest $request, Menu $menu)
    {
        $this->commandBus->handle(new MenuUpdateCommand($request, $menu));
    }

    public function remove(Menu $menu)
    {
        $this->commandBus->handle(new MenuRemoveCommand($menu));
    }

    public function getMenus()
    {
        return $this->queryBus->query(new MenuAllQuery());
    }

    public function getMenu($menu_id)
    {
        return $this->queryBus->query(new FindMenuQuery($menu_id));
    }
}
