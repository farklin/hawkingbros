<?php

namespace App\UseCases;

use App\Command\CommandBus;
use App\Query\QueryBus;
use App\UseCases\ServiceInterface;

class Service implements ServiceInterface
{
    protected $commandBus;
    protected $queryBus;

    public function __construct(CommandBus $commandBus, QueryBus $queryBus)
    {
        $this->commandBus = $commandBus;
        $this->queryBus = $queryBus;
    }
}
