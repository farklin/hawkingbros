<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Menu\Menu;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        $menu = Menu::all();
        foreach ($menu as $element) {
        view()->share($element->code, $element);   
        }
    }
}
