<?php 

namespace App\Traits\Relation\BelongsTo;

use Illuminate\Foundation\Auth\User as AuthUser;

trait User
{
    public function user()
    {
        return $this->belongsTo(AuthUser::class);
    }
}

