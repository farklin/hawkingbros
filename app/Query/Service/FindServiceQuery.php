<?php

namespace App\Query\Service;

use App\Models\Service\Service;

/**
 * Вывод одной услуги
 */
class FindServiceQuery
{
    protected $service;
    protected $service_id;

    public function __construct($service_id)
    {
        $this->service = new Service();
        $this->service_id = $service_id;
    }

    public function __invoke()
    {
        return $this->service::find($this->service_id);
    }
}
