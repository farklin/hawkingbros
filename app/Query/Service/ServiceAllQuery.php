<?php

namespace App\Query\Service;

use App\Models\Service\Service;

/**
 * Вывод всех услуг 
 */
class ServiceAllQuery
{
    protected $service;

    public function __construct()
    {
        $this->service = new Service();
    }

    public function __invoke()
    {
        return $this->service::orderBy('id')->get();
    }
}
