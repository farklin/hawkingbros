<?php

namespace App\Query\Parthner;

use App\Models\Service\Parthner;


/**
 * Вывод одного партнера 
 */
class FindParthnerQuery
{
    protected $parthner;
    protected $parthner_id;

    public function __construct($parthner_id)
    {
        $this->parthner = new Parthner;
        $this->parthner_id = $parthner_id;
    }

    public function __invoke()
    {
        return $this->parthner::find($this->parthner_id);
    }
}
