<?php

namespace App\Query\Parthner;

use App\Models\Service\Parthner;


/**
 * Вывод всех партнеров 
 */
class ParthnerAllQuery
{
    protected $parthners;

    public function __construct()
    {
        $this->parthners = new Parthner;
    }

    public function __invoke()
    {
        return $this->parthners::orderBy('id');
    }
}
