<?php

namespace App\Query\Project;

use App\Models\Service\Project;

/**
 * Нахождение активных проектов
 */
class FindProjectActiveQuery
{
    protected $project;

    public function __construct()
    {
        $this->project = new Project;
    }

    public function __invoke()
    {
        return $this->project::where('status', 1)->get(); 
    }
}
