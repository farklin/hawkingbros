<?php

namespace App\Query\Project;

use App\Models\Service\Project;

/**
 * Фильтрация проектов 
 */
class FindProjectFilterQuery
{
    protected $project;
    protected $tags;
    protected $services;

    /**
     * Конструктор класса фильтра
     *
     * @param [array] $tags (принимает id тегов [1,2 ...])
     * @param [array] $services (принимает id услуг [1,2 ...])
     */
    public function __construct($tags, $services)
    {
        $this->project = new Project;
        $this->tags = $tags;
        $this->services = $services;
    }

    /**
     * Фильтрация проектов по  тегам и предоставляемым услугам 
     *
     * @return Project $project 
     */
    public function __invoke()
    {
        $projects = $this->project::WhereHas('tags', function ($query) {
            if ($this->tags != []) {
                $query->whereIn('tags.id', $this->tags);
            }
            if ($this->services != []) {
                $query->whereIn('service_id', $this->services);
            }
        })
            ->get();
        return $projects;
    }
}
