<?php

namespace App\Query\Project;

use App\Models\Service\Project;


/**
 * Вывод связанных услуг с проектом 
 */
class FindProjectServicesQuery
{
    protected $project;

    public function __construct()
    {
        $this->project = new Project;
    }

    public function __invoke()
    {
        $services = $this->project::select('services.title', 'services.id')
            ->join('services', 'services.id', '=', 'projects.service_id')
            ->where('projects.status', 1)
            ->groupBy('services.title')
            ->get();

        return $services;
    }
}
