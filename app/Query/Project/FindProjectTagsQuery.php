<?php

namespace App\Query\Project;

use App\Models\Service\Project;


/**
 * Вывод всех тегов связаных с проектом 
 */
class FindProjectTagsQuery
{
    protected $project;

    public function __construct()
    {
        $this->project = new Project;
    }

    public function __invoke()
    {
        $tags = $this->project::select('tags.title', 'tags.id')
            ->join('project_tag', 'project_tag.project_id', '=', 'projects.id')
            ->join('tags', 'project_tag.tag_id', '=', 'tags.id')
            ->where('projects.status', 1)
            ->groupBy('tags.title')
            ->get();

        return $tags;
    }
}
