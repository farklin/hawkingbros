<?php

namespace App\Query\Project;

use App\Models\Service\Project;

/**
 * Вывод всех проектов
 */
class ProjectAllQuery
{
    protected $projects;

    public function __construct()
    {
        $this->projects = new Project;
    }

    public function __invoke()
    {
        return $this->projects::orderBy('id');
    }
}
