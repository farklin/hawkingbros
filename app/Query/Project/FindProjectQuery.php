<?php

namespace App\Query\Project;

use App\Models\Service\Project;

/**
 * Вывод одного проекта по id
 */
class FindProjectQuery
{
    protected $project;
    protected $project_id;

    public function __construct($project_id)
    {
        $this->project = new Project;
        $this->project_id = $project_id;
    }

    public function __invoke()
    {
        return $this->project::find($this->project_id);
    }
}
