<?php

namespace App\Query;

class QueryBus
{
    public function query($query)
    {
        $result = $query();
        return $result;
    }
}
