<?php

namespace App\Query\Review;

use App\Models\Service\Review;

/**
 * Вывод одного отзыва
 */
class FindReviewQuery
{
    protected $review;
    protected $review_id;

    public function __construct($review_id)
    {
        $this->review = new Review;
        $this->review_id = $review_id;
    }

    public function __invoke()
    {
        return $this->review::find($this->review_id);
    }
}
