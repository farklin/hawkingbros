<?php

namespace App\Query\Review;

use App\Models\Service\Review;

/**
 * Вывод всех отзывов
 */
class ReviewAllQuery
{
    protected $review;

    public function __construct()
    {
        $this->review = new Review;
    }

    public function __invoke()
    {
        return $this->review::orderBy('id')->get();
    }
}
