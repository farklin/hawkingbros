<?php

namespace App\Query\Article;

use App\Models\Service\Article;

/**
 * Вывод всех статей
 */
class ArticleAllQuery
{
    protected $article;

    public function __construct()
    {
        $this->article = new Article;
    }

    public function __invoke()
    {
        return $this->article::orderBy('id');
    }
}
