<?php

namespace App\Query\Article;

use App\Models\Service\Article;

/**
 * Вывод одной статьи 
 */
class FindArticleQuery
{
    protected $article;
    protected $article_id;

    public function __construct($article_id)
    {
        $this->article = new Article;
        $this->article_id = $article_id;
    }

    public function __invoke()
    {
        return $this->article::find($this->article_id);
    }
}
