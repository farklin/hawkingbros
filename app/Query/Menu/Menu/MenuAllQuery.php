<?php

namespace App\Query\Menu\Menu;

use App\Models\Menu\Menu;

/**
 * Вывод всех меню 
 */
class MenuAllQuery
{
    protected $menu;

    public function __construct()
    {
        $this->menu = new Menu;
    }

    public function __invoke()
    {
        return $this->menu::orderBy('id')->get();
    }
}
