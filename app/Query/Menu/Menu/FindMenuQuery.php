<?php

namespace App\Query\Menu\Menu;

use App\Models\Menu\Menu;


/**
 * Вывод одного меню 
 */
class FindMenuQuery
{
    protected $menu;
    protected $menu_id;

    public function __construct($menu_id)
    {
        $this->menu = new Menu;
        $this->menu_id = $menu_id;
    }

    public function __invoke()
    {
        return $this->menu::find($this->menu_id);
    }
}
