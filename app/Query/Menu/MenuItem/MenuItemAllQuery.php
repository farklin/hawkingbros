<?php

namespace App\Query\Menu\MenuItem;

use App\Models\Menu\MenuItem;

/**
 * Вывод всех элементов меню 
 */
class MenuItemAllQuery
{
    protected $menu_item;

    public function __construct()
    {
        $this->menu_item = new MenuItem;
    }

    public function __invoke()
    {
        return $this->menu_item::where('menu_item_id', null)->orderBy('sorting')->get();
    }
}
