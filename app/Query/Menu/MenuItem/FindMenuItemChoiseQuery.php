<?php

namespace App\Query\Menu\MenuItem;

use App\Models\Menu\MenuItem;

/**
 * Вывод всех элементов меню кроме самого обьекта
 */
class FindMenuItemChoiseQuery
{
    protected $menu_item;
    protected $menu_item_id;

    public function __construct($menu_item_id)
    {
        $this->menu_item = new MenuItem;
        $this->menu_item_id = $menu_item_id;
    }

    public function __invoke()
    {
        return $this->menu_item::where('menu_item_id', null)
                                ->where('id', '<>', $this->menu_item_id)
                                ->get();
    }
}
