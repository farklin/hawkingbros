<?php

namespace App\Query\Menu\MenuItem;

use App\Models\Menu\MenuItem;

/**
 * Вывод одного элемента меню 
 */
class FindMenuItemQuery
{
    protected $menu_item;
    protected $menu_item_id;

    public function __construct($menu_item_id)
    {
        $this->menu_item = new MenuItem;
        $this->menu_item_id = $menu_item_id;
    }

    public function __invoke()
    {
        return $this->menu_item::find($this->menu_item_id);
    }
}
