<?php

namespace App\Query\Application;

use App\Models\Application\Application;

/**
 * Вывод всех заявок
 */
class ApplicationAllQuery
{
    protected $application;

    public function __construct()
    {
        $this->application = new Application;
    }

    public function __invoke()
    {
        return $this->application::orderBy('id');
    }
}
