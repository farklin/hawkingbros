<?php

namespace App\Query\Application;

use App\Models\Application\Application;

/**
 * Вывод одной заявки
 */
class FindApplicationQuery
{
    protected $application;
    protected $id;

    public function __construct($id)
    {
        $this->application = new Application;
        $this->id = $id;
    }

    public function __invoke()
    {
        return $this->application::find($this->id);
    }
}
