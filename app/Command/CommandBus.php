<?php

namespace App\Command;


class CommandBus
{
    public function handle($command)
    {
        $value = $command();
        return $value;
    }
}
