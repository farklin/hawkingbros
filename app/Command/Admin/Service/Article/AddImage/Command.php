<?php

namespace App\Command\Admin\Service\Article\AddImage;

use App\Models\Service\Article;

class Command
{
    protected $article;
    protected $image_id;

    public function __construct(Article $article, $image_id)
    {
        $this->article = $article;
        $this->image_id = $image_id;
    }

    public function __invoke()
    {
        $this->article->update([
            'image_id' => $this->image_id
        ]);
    }
}
