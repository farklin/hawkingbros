<?php

namespace App\Command\Admin\Service\Article\Create;

use App\Http\Requests\Admin\Service\Article\CreateRequest;
use App\Models\Service\Article;
use App\Helper\Helper;

class Command
{
    private $article;

    public function __construct(CreateRequest $request)
    {
        $this->article = new Article();
        $this->request = $request;
    }

    public function __invoke()
    {
        $article = $this->article::create([
            'title' => $this->request->title,
            'content' => $this->request->content,
            'short_description' => $this->request->short_description,
            'url' => $this->request->url,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $article;
    }
}
