<?php

namespace App\Command\Admin\Service\Article\Remove;

use App\Models\Service\Article;

class Command
{

    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function __invoke()
    {
        $this->article->delete();
    }
}
