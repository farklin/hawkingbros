<?php

namespace App\Command\Admin\Service\Article\Update;

use App\Http\Requests\Admin\Service\Article\UpdateRequest;
use App\Models\Service\Article;
use App\Helper\Helper;

class Command
{
    private $article;

    public function __construct(UpdateRequest $request, Article $article)
    {
        $this->article = $article;
        $this->request = $request;
    }

    public function __invoke()
    {
        $article = $this->article->update([
            'title' => $this->request->title,
            'content' => $this->request->content,
            'short_description' => $this->request->short_description,
            'url' => $this->request->url,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $article;
    }
}
