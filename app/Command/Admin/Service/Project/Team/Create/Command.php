<?php

namespace App\Command\Admin\Service\Project\Team\Create;

use App\Models\Service\Project;


class Command
{

    protected $teams;
    protected $project;

    public function __construct($teams, $project)
    {
        $this->teams = $teams;
        $this->project = $project;
    }

    public function __invoke()
    {
        if ($this->teams) {
            foreach ($this->teams as $team) {
                if ($team != '') {
                    $this->project->teams()->create([
                        'name' => $team
                    ]);
                }
            }
        }
    }
}
