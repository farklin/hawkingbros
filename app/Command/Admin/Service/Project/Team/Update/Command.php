<?php

namespace App\Command\Admin\Service\Project\Team\Update;

use App\Models\Service\Project;


class Command
{

    protected $teams;
    protected $project;

    public function __construct($teams, $project)
    {
        $this->teams = $teams;
        $this->project = $project;
    }

    public function __invoke()
    {
        // отвязка существующих отношений 
       foreach($this->project->teams as $team)
       {
            $team->delete();
       }
       // Привязываем новые отношения 
       foreach ($this->teams as $team) {
        if ($team != '') {
            $this->project->teams()->create([
                'name' => $team
            ]);
        }
    }



    }
}
