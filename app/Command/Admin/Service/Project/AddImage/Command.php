<?php

namespace App\Command\Admin\Service\Project\AddImage;

use App\Models\Service\Project;

class Command
{
    protected $project;
    protected $image_id;

    public function __construct(Project $project, $image_id)
    {
        $this->project = $project;
        $this->image_id = $image_id;
    }

    public function __invoke()
    {
        $this->project->update([
            'image_id' => $this->image_id
        ]);
    }
}
