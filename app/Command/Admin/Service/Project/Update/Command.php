<?php

namespace App\Command\Admin\Service\Project\Update;

use App\Http\Requests\Admin\Service\Project\UpdateRequest;
use App\Models\Service\Project;
use App\Helper\Helper;

class Command
{
    private $project;

    public function __construct(UpdateRequest $request, Project $project)
    {
        $this->project = $project;
        $this->request = $request;
    }

    public function __invoke()
    {
        $project = $this->project->update([
            'title' => $this->request->title,
            'volume' => $this->request->volume,
            'dedline' => $this->request->dedline,
            'link' => $this->request->link,
            'service_id' => $this->request->service_id,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $project;
    }
}
