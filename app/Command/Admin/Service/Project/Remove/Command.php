<?php

namespace App\Command\Admin\Service\Project\Remove;

use App\Models\Service\Project;

class Command
{

    protected $project;

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    public function __invoke()
    {
        $this->project->delete();
    }
}
