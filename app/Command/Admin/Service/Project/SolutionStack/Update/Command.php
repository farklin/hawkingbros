<?php

namespace App\Command\Admin\Service\Project\SolutionStack\Update;

class Command
{

    protected $solution_stacks;
    protected $project;

    public function __construct($solution_stacks, $project)
    {
        $this->solution_stacks = $solution_stacks;
        $this->project = $project;
    }

    public function __invoke()
    {
         // отвязка существующих отношений 
       foreach($this->project->solution_stacks as $solution_stack)
       {
           $this->project->solution_stacks()->detach($solution_stack->id); 
           $solution_stack->delete(); 
       }
       
        if ($this->solution_stacks) {
            foreach ($this->solution_stacks as $solution_stack) {
                if ($solution_stack != '') {
                    $this->project->solution_stacks()->create([
                        'title' => $solution_stack,
                        'status' => true,
                    ]);
                }
            }
        }
    }
}
