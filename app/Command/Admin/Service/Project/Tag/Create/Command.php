<?php

namespace App\Command\Admin\Service\Project\Tag\Create;

use App\Models\Service\Tag;

class Command
{

    protected $tags;
    protected $project;

    public function __construct($tags, $project)
    {
        $this->tags = $tags;
        $this->project = $project;
    }

    public function __invoke()
    {
        if ($this->tags) {
            foreach ($this->tags as $tag) {
                if ($tag != '') {
                    if (Tag::where('title', $tag)->exists()) {
                        $this->project->tags()->attach(Tag::where('title', $tag)
                            ->first());
                    } else {
                        $this->project->tags()->create([
                            'title' => $tag,
                            'status' => true,
                        ]);
                    }
                }
            }
        }
    }
}
