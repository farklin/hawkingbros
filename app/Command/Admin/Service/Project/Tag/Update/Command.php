<?php

namespace App\Command\Admin\Service\Project\Tag\Update;

use App\Command\Admin\Service\Project\Tag\Create\Command as CreateTagsCommand;

class Command
{

    protected $tags;
    protected $project;

    public function __construct($tags, $project)
    {
        $this->tags = $tags;
        $this->project = $project;
    }

    public function __invoke()
    {
        // отвязка существующих отношений 
        foreach ($this->project->tags as $tag) {
            $this->project->tags()->detach($tag->id);
        }

        $create = new CreateTagsCommand($this->tags, $this->project);
        $create->__invoke();
    }
}
