<?php

namespace App\Command\Admin\Service\Tag\Create;

use App\Http\Requests\Admin\Service\Tag\CreateRequest;
use App\Models\Service\Tag;
use App\Helper\Helper;

class Command
{
    private $tag;

    public function __construct(CreateRequest $request)
    {
        $this->tag = new Tag();
        $this->request = $request;
    }

    public function __invoke()
    {
        $tag = $this->tag::create([]);

        return $tag;
    }
}
