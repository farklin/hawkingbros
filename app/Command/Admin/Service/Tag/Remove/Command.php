<?php

namespace App\Command\Admin\Service\Tag\Remove;

use App\Models\Service\Tag;

class Command
{

    protected $tag;

    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    public function __invoke()
    {
        $this->tag->delete();
    }
}
