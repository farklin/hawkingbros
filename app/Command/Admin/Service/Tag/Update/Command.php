<?php

namespace App\Command\Admin\Service\Tag\Update;

use App\Http\Requests\Admin\Service\Tag\UpdateRequest;
use App\Models\Service\Tag;
use App\Helper\Helper;

class Command
{
    private $tag;

    public function __construct(UpdateRequest $request, Tag $tag)
    {
        $this->tag = $tag;
        $this->request = $request;
    }

    public function __invoke()
    {
        $tag = $this->tag->update([]);

        return $tag;
    }
}
