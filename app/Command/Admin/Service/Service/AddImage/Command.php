<?php

namespace App\Command\Admin\Service\Service\AddImage;

use App\Models\Service\Service;

class Command
{
    protected $service;
    protected $image_id;

    public function __construct(Service $service, $image_id)
    {
        $this->service = $service;
        $this->image_id = $image_id;
    }

    public function __invoke()
    {
        $this->service->update([
            'image_id' => $this->image_id
        ]);
    }
}
