<?php

namespace App\Command\Admin\Service\Service\Update;

use App\Http\Requests\Admin\Service\Service\UpdateRequest;
use App\Models\Service\Service;
use App\Helper\Helper;

class Command
{
    private $service;

    public function __construct(UpdateRequest $request, Service $service)
    {
        $this->service = $service;
        $this->request = $request;
    }

    public function __invoke()
    {
        $service = $this->service->update([
            'title' => $this->request->title,
            'description' => $this->request->description,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $service;
    }
}
