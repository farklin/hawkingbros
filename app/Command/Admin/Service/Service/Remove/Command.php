<?php

namespace App\Command\Admin\Service\Service\Remove;

use App\Models\Service\Service;

class Command
{

    protected $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    public function __invoke()
    {
        $this->service->delete();
    }
}
