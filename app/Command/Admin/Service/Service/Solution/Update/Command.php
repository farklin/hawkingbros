<?php

namespace App\Command\Admin\Service\Service\Solution\Update;

use App\Models\Service\Service;


class Command
{

    protected $solutions;
    protected $service;

    public function __construct($solutions, $service)
    {
        $this->solutions = $solutions;
        $this->service = $service;
    }

    public function __invoke()
    {
        // отвязка существующих отношений 
       if( isset($this->service->solutions)) {
            foreach($this->service->solutions as $solution)
            {
                    $solution->delete();
            }
        }
       // Привязываем новые отношения 
       foreach ($this->solutions as $solution) {
        if ($solution != '') {
            $this->service->solutions()->create([
                'value' => $solution
            ]);
        }
    }
    }
}
