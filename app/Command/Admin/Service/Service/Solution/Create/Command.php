<?php

namespace App\Command\Admin\Service\Service\Solution\Create;

use App\Models\Service\Service;


class Command
{

    protected $solutions;
    protected $service;

    public function __construct($solutions, $service)
    {
        $this->solutions = $solutions;
        $this->service = $service;
    }

    public function __invoke()
    {
       // Привязываем новые отношения 
       foreach ($this->solutions as $solution) {
        if ($solution != '') {
            $this->service->solutions()->create([
                'value' => $solution
            ]);
        }
    }
    }
}
