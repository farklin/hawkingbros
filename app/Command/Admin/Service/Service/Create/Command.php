<?php

namespace App\Command\Admin\Service\Service\Create;

use App\Http\Requests\Admin\Service\Service\CreateRequest;
use App\Models\Service\Service;
use App\Helper\Helper;

class Command
{
    private $service;

    public function __construct(CreateRequest $request)
    {
        $this->service = new Service();
        $this->request = $request;
    }

    public function __invoke()
    {
        $service = $this->service::create([
            'title' => $this->request->title,
            'description' => $this->request->description,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $service;
    }
}
