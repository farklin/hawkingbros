<?php

namespace App\Command\Admin\Service\SolutionStack\Remove;

use App\Models\Service\SolutionStack;

class Command
{

    protected $solution_stack;

    public function __construct(SolutionStack $solution_stack)
    {
        $this->solution_stack = $solution_stack;
    }

    public function __invoke()
    {
        $this->solution_stack->delete();
    }
}
