<?php

namespace App\Command\Admin\Service\SolutionStack\Create;

use App\Http\Requests\Admin\Service\SolutionStack\CreateRequest;
use App\Models\Service\SolutionStack;
use App\Helper\Helper;

class Command
{
    private $solution_stack;

    public function __construct(CreateRequest $request)
    {
        $this->solution_stack = new SolutionStack();
        $this->request = $request;
    }

    public function __invoke()
    {
        $solution_stack = $this->solution_stack::create([]);

        return $solution_stack;
    }
}
