<?php

namespace App\Command\Admin\Service\SolutionStack\Update;

use App\Http\Requests\Admin\Service\SolutionStack\UpdateRequest;
use App\Models\Service\SolutionStack;
use App\Helper\Helper;

class Command
{
    private $solution_stack;

    public function __construct(UpdateRequest $request, SolutionStack $solution_stack)
    {
        $this->solution_stack = $solution_stack;
        $this->request = $request;
    }

    public function __invoke()
    {
        $solution_stack = $this->solution_stack->update([]);

        return $solution_stack;
    }
}
