<?php

namespace App\Command\Admin\Service\Review\Remove;

use App\Models\Service\Review;

class Command
{

    protected $review;

    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    public function __invoke()
    {
        $this->review->delete();
    }
}
