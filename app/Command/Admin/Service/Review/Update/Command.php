<?php

namespace App\Command\Admin\Service\Review\Update;

use App\Http\Requests\Admin\Service\Review\UpdateRequest;
use App\Models\Service\Review;
use App\Helper\Helper;

class Command
{
    private $review;

    public function __construct(UpdateRequest $request, Review $review)
    {
        $this->review = $review;
        $this->request = $request;
    }

    public function __invoke()
    {
        $review = $this->review->update([
            'name' => $this->request->name,
            'post' => $this->request->post,
            'comment' => $this->request->comment,
            'company' => $this->request->company,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $review;
    }
}
