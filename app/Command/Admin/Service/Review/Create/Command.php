<?php

namespace App\Command\Admin\Service\Review\Create;

use App\Http\Requests\Admin\Service\Review\CreateRequest;
use App\Models\Service\Review;
use App\Helper\Helper;

class Command
{
    private $review;

    public function __construct(CreateRequest $request)
    {
        $this->review = new Review();
        $this->request = $request;
    }

    public function __invoke()
    {
        $review = $this->review::create([
            'name' => $this->request->name,
            'post' => $this->request->post,
            'comment' => $this->request->comment,
            'company' => $this->request->company,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $review;
    }
}
