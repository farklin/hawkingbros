<?php

namespace App\Command\Admin\Service\Review\AddImage;

use App\Models\Service\Review;

class Command
{
    protected $review;
    protected $image_id;

    public function __construct(Review $review, $image_id)
    {
        $this->review = $review;
        $this->image_id = $image_id;
    }

    public function __invoke()
    {
        $this->review->update([
            'image_id' => $this->image_id
        ]);
    }
}
