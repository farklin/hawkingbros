<?php

namespace App\Command\Admin\Service\Parthner\Update;

use App\Http\Requests\Admin\Service\Parthner\UpdateRequest;
use App\Models\Service\Parthner;
use App\Helper\Helper;

class Command
{
    private $parthner;

    public function __construct(UpdateRequest $request, Parthner $parthner)
    {
        $this->parthner = $parthner;
        $this->request = $request;
    }

    public function __invoke()
    {
        $parthner = $this->parthner->update([
            'title' => $this->request->title,
            'link' => $this->request->link,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $parthner;
    }
}
