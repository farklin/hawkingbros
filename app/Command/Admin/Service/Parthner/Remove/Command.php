<?php

namespace App\Command\Admin\Service\Parthner\Remove;

use App\Models\Service\Parthner;

class Command
{

    protected $parthner;

    public function __construct(Parthner $parthner)
    {
        $this->parthner = $parthner;
    }

    public function __invoke()
    {
        $this->parthner->delete();
    }
}
