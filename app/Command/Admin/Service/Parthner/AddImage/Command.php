<?php

namespace App\Command\Admin\Service\Parthner\AddImage;

use App\Models\Service\Parthner;

class Command
{
    protected $parthner;
    protected $image_id;

    public function __construct(Parthner $parthner, $image_id)
    {
        $this->parthner = $parthner;
        $this->image_id = $image_id;
    }

    public function __invoke()
    {
        $this->parthner->update([
            'image_id' => $this->image_id
        ]);
    }
}
