<?php

namespace App\Command\Admin\Service\Parthner\Create;

use App\Http\Requests\Admin\Service\Parthner\CreateRequest;
use App\Models\Service\Parthner;
use App\Helper\Helper;

class Command
{
    private $parthner;

    public function __construct(CreateRequest $request)
    {
        $this->parthner = new Parthner();
        $this->request = $request;
    }

    public function __invoke()
    {
        $parthner = $this->parthner::create([
            'title' => $this->request->title,
            'link' =>  $this->request->link,
            'image_id' => null,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $parthner;
    }
}
