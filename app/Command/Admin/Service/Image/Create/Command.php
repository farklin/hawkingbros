<?php

namespace App\Command\Admin\Service\Image\Create;

use App\Models\Service\Image;

class Command
{
    private $image;
    private $path;

    public function __construct($path, $image)
    {
        $this->imageExistence = new Image();
        $this->path = $path;
        $this->image = $image;
    }

    public function __invoke()
    {
        $path = $this->image->store($this->path, 'public');

        $image = $this->imageExistence::create([
            'path' => $path,
            'alt' => '',
            'status' => true,
        ]);

        return $image->id;
    }
}
