<?php

namespace App\Command\Admin\Application\Remove;

use App\Models\Application\Application;
use App\Models\Service\Article;

class Command
{

    protected $application;

    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    public function __invoke()
    {
        $this->application->delete();
    }
}
