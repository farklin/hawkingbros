<?php

namespace App\Command\Admin\Application\Update;

use App\Http\Requests\Admin\Application\UpdateRequest;
use App\Models\Application\Application;
use App\Helper\Helper;

class Command
{
    private $application;

    public function __construct(UpdateRequest $request, Application $application)
    {
        $this->application = $application;
        $this->request = $request;
    }

    public function __invoke()
    {
        $this->application->update([
            'name' => $this->request->name,
            'phone' => $this->request->phone,
            'message' => $this->request->message,
            'service_id' => $this->request->service_id,
            'status' => Helper::statusExist($this->request->status),
        ]);
    }
}
