<?php

namespace App\Command\Admin\Menu\Menu\Remove;

use App\Http\Requests\Admin\Menu\Menu\CreateRequest;
use App\Models\Menu\Menu;
use App\Helper\Helper;

class Command
{
    private $menu;

    public function __construct(Menu $menu)
    {
        $this->menu = $menu; 
    }

    public function __invoke()
    {
       $this->menu->delete();  
    }
}
