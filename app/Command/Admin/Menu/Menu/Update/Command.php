<?php

namespace App\Command\Admin\Menu\Menu\Update;


use App\Models\Menu\Menu;
use App\Helper\Helper;
use App\Http\Requests\Admin\Menu\Menu\UpdateRequest;

class Command
{
    private $menu;

    public function __construct(UpdateRequest $request, Menu $menu)
    {
        $this->menu = $menu;
        $this->request = $request;
    }

    public function __invoke()
    {
        $this->menu->update([
            'name' => $this->request->name,
            'code' => $this->request->code,
            'status' => Helper::statusExist($this->request->status),
        ]);
    }
}
