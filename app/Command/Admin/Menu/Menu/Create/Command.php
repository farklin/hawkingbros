<?php

namespace App\Command\Admin\Menu\Menu\Create;

use App\Http\Requests\Admin\Menu\Menu\CreateRequest;
use App\Models\Menu\Menu;
use App\Helper\Helper;

class Command
{
    private $menu;

    public function __construct(CreateRequest $request)
    {
        $this->menu = new Menu();
        $this->request = $request;
    }

    public function __invoke()
    {
        $menu = $this->menu::create([
            'name' => $this->request->name,
            'code' => $this->request->code,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $menu;
    }
}
