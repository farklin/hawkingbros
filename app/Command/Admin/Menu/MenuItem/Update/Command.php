<?php

namespace App\Command\Admin\Menu\MenuItem\Update;


use App\Models\Menu\MenuItem;
use App\Helper\Helper;
use App\Http\Requests\Admin\Menu\MenuItem\UpdateRequest;

class Command
{
    private $menuItem;
    private $request;

    public function __construct(UpdateRequest $request, $menuItem)
    {
        $this->menuItem = $menuItem;
        $this->request = $request;
    }

    public function __invoke()
    {
        $item = $this->menuItem->update([
            'name' => $this->request->name,
            'link' => $this->request->link,
            'sorting' => $this->request->sorting,
            'menu_id' => $this->request->menu_id,
            'menu_item_id' => $this->request->menu_item_id,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $item;
    }
}
