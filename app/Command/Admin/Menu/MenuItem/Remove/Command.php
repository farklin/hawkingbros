<?php

namespace App\Command\Admin\Menu\MenuItem\Remove;

use App\Http\Requests\Admin\Menu\MenuItem\CreateRequest;
use App\Models\Menu\MenuItem;
use App\Helper\Helper;

class Command
{
    private $menu;

    public function __construct(MenuItem $menu)
    {
        $this->menu = $menu; 
    }

    public function __invoke()
    {
       $this->menu->delete();  
    }
}
