<?php

namespace App\Command\Admin\Menu\MenuItem\Create;

use App\Http\Requests\Admin\Menu\MenuItem\CreateRequest;
use App\Models\Menu\MenuItem;
use App\Helper\Helper;

class Command
{
    private $menu;

    public function __construct(CreateRequest $request)
    {
        $this->menu = new MenuItem();
        $this->request = $request;
    }

    public function __invoke()
    {
        $menu = $this->menu::create([
            'name' => $this->request->name,
            'link' => $this->request->link,
            'sorting' => $this->request->sorting,
            'menu_item_id' => $this->request->menu_item_id,
            'menu_id' => $this->request->menu_id,
            'status' => Helper::statusExist($this->request->status),
        ]);

        return $menu;
    }
}
