<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Tag;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(50),
            'content' => $this->faker->text(600),
            'short_description' => $this->faker->text(250),
            'url' => Str::slug($this->faker->unique()->text(50)), 
            'tag_id' => Tag::get()->random()->id,
        ];
    }
}
