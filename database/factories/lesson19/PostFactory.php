<?php

namespace Database\Factories\lesson19;

use App\Models\lesson19\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{

    protected $model =  Post::class; 
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'text' => $this->faker->text(),
        ];
    }
}
